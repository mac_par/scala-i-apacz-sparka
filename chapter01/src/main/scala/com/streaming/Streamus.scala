package com.streaming

import com.streams.LogUtils
import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Seconds, StreamingContext}

object Streamus {
  def main(args:Array[String]):Unit = {
    val conf = new SparkConf().setMaster("local[*]").setAppName("streamCosTam")

    val sc = new StreamingContext(conf, Seconds(2))
    val lines = sc.socketTextStream("127.0.0.1", 7779)

    lines.map{line => LogUtils.parseFromLogLine(line)}.map(entry => (entry.getIpAddress, 1))
        .reduceByKey((x,y)=> x+y).print()

    sc.start()
    sc.awaitTermination()
  }
}
