package com.streaming

import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Seconds, StreamingContext}

object Str01 extends App {
  val conf = new SparkConf().setMaster("local[2]").setAppName("Stream01")
  val scc = new StreamingContext(conf, Seconds(2))
  scc.sparkContext.setLogLevel("WARN")

  val lines = scc.socketTextStream("127.0.0.1", 9999)
  val words = lines.flatMap { line => line.split("\\s+") }
  val wordCount = words.map { word => (word, 1) }.reduceByKey((x, y) => x + y)

  wordCount.print()
  scc.start()
  scc.awaitTermination()
}
