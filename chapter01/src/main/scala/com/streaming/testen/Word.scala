package com.streaming.testen
import java.util.Objects

import org.mongodb.scala.bson.ObjectId
import org.mongodb.scala.bson.codecs.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.bson.codecs.configuration.CodecRegistry


case class Word(_id: ObjectId, word: String, occurrence: Int)

object Word {
  def apply(arg: (String, Int)): Word = new Word(new ObjectId, arg._1, arg._2)

  def unapply(arg: Word): Option[(ObjectId, String, Int)] = if (Objects.isNull(arg)) None else Some(arg._id, arg.word, arg.occurrence)

  val wordCoder: CodecRegistry = fromRegistries(fromProviders(classOf[Word]),DEFAULT_CODEC_REGISTRY)
}
