package com.streaming.testen

import java.time.LocalTime

import com.mongodb.spark.MongoSpark
import org.apache.spark.sql.SparkSession
import org.mongodb.scala.Document

object SparkMitMongo {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .master("local[*]")
      .appName("cosTam")
      .config("spark.mongodb.output.uri", "mongodb://127.0.0.1:27017/testowo.test")
      .getOrCreate()
    val sc = spark.sparkContext
    val docs = sc.parallelize(1 to 10).mapPartitions { part =>
      part.map { item => Document("item" -> item, "at" -> LocalTime.now().toString) }
    }

    MongoSpark.save(docs)
    sc.stop()
  }
}
