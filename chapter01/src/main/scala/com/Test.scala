package com

import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Seconds, StreamingContext}

object Test extends App {
  val sparkConf = new SparkConf().setMaster("local[2]").setAppName("SteamingCounter")
  val scc = new StreamingContext(sparkConf, Seconds(1))

  val lines = scc.socketTextStream("localhost.localdomain", 7777)
  val errorLines = lines.filter { line => line.contains("error") }

  errorLines.print()

  scc.start()
  scc.awaitTermination()
}
