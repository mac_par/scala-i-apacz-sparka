package com

import org.apache.spark.{SparkConf, SparkContext}

object DiePractiken {
  def main(args:Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local[*]")
    val sc = new SparkContext(conf)
    val products = sc.parallelize(List((1,"mobile",50000), (2,"Gucci",9845612), (3,"TV",852022)))
    sc.stop()
  }
}
