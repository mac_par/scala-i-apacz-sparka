package com.chpt01

import org.apache.spark.{SparkConf, SparkContext}

object ReadingREADME extends App {
  val conf = new SparkConf().setMaster("local").setAppName("Mein App")
  val sc = new SparkContext(conf)
  val lines = sc.textFile("/home/maciek/spark/spark-2.4.4-bin-hadoop2.7/README.md")

  println(lines.count())
  println(lines.first())
  println(lines.filter { line => line.contains("Python") }.count())

  val words = lines.flatMap { line => line.split(" ") }
  val counts = words.map { word => (word, 1) }.reduceByKey { case (x, y) => x + y }
  println(s"Words $counts")
  sc.stop()
}
