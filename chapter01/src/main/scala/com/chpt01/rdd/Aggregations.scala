package com.chpt01.rdd
import org.apache.spark.{SparkContext,SparkConf}

object Aggregations extends App {
  val config = new SparkConf().setMaster("local").setAppName("Aggregations")
  val sc = new SparkContext(config)

  val numbers = sc.parallelize(List(1,2,3,4,5,6,7,8,9,10))

  val sum1 = numbers.reduce((x,y) => x+y)
  val sum2 = numbers.fold(0)((x,y)=> x+y)
  val sum3 = numbers.aggregate(0)((acc,value) => acc+ value, (acc1,acc2) => acc1 + acc2)
  println(s"Sums - 1. $sum1, 2. $sum2, 3. $sum3")

  val avg = numbers.aggregate((0,0))((acc,value) => (acc._1 + value, acc._2 +1), (acc1, acc2) => (acc1._1 + acc2._1, acc1._2 + acc2._2))
  val avgResult = avg._1 / avg._2.toDouble
  println(s"avg: $avgResult")

  val top = numbers.top(5)
  println(s"top ${top.mkString(", ")}")

  val map = numbers.countByValue()
  println(s"mapka $map")

}
