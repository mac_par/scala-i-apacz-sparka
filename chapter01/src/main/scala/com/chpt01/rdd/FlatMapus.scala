package com.chpt01.rdd
import org.apache.spark.{SparkContext,SparkConf}

object FlatMapus extends App {
  val config = new SparkConf().setMaster("local").setAppName("flaten")
  val sc = new SparkContext(config)

  val strings = sc.parallelize(List("Hello Word", "Hi", "Make Them Supper!"))

  val words = strings.flatMap{word => word.split(" ") }
  println(s"first: ${words.first()}")
  println(words.collect().mkString(", "))
}
