package com.chpt01.rdd
import org.apache.spark.{SparkContext,SparkConf}


object Collecting extends App {
  val config = new SparkConf().setMaster("local").setAppName("costam")
  val sc = new SparkContext(config)

  val numbers = sc.parallelize(List(1,2,3,4))
  val result1 = numbers.map {x => x * x}
  val result2 = numbers.filter{ line => line != 1}

  println(result1.collect().mkString(", "))
  println(result2.collect().mkString(", "))
  sc.stop()
}
