package com.chpt01.rdd
import org.apache.spark.{SparkContext,SparkConf}

object Sections extends App {
  val config = new SparkConf().setMaster("local").setAppName("Sections")
  val sc = new SparkContext(config)

  val rdd1 = sc.parallelize(List("Marko", "Polo", "Barbra", "Streisand", "Zenek","Zbigniew", "Zenek"))
  val rdd2 = sc.parallelize(List("Disco", "Polo", "Soul", "Streisand", "Zenek","Piniadz", "Soul"))

  val union = rdd1.union(rdd2)
  println(s"Union: ${union.collect().mkString(" : ")}")
  println(s"Union - distince: ${union.distinct().collect().mkString(" : ")}")

  val distinct1 = rdd1.distinct()
  val distinct2 = rdd2.distinct()

  println(s"Distinct 1: ${distinct1.collect().mkString(" : ")}")
  println(s"Distinct 2: ${distinct2.collect().mkString(" : ")}")

  val intersection = rdd1.intersection(rdd2)

  println(s"Intersection: ${intersection.collect().mkString(" : ")}")

  val subtract = rdd1.subtract(rdd2)
  println(s"Subtract: ${subtract.collect().mkString(" : ")}")
}
