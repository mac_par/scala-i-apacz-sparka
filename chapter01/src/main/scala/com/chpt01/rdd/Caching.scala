package com.chpt01.rdd
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkConf, SparkContext}

object Caching extends App {
  val config = new SparkConf().setMaster("local").setAppName("Caching")
  val sc = new SparkContext(config)

  val numbers = sc.parallelize(List(1,2,3,4,5))
  println("Ready?")
  val result = numbers.map(x => x*x)
  result.persist(StorageLevel.DISK_ONLY)
  println(s"count: ${result.count}")
  println(s"collect: ${result.collect().mkString(", ")}")

  sc.stop()
}
