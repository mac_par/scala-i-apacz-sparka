package com.mongostream
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.streaming.{Minutes, Seconds, StreamingContext}
import com.mongodb.spark._
import org.apache.spark.sql.SparkSession
import org.bson.Document

import scala.util.{Failure, Success, Try}

object MongoSparkTry {
  def extractWords(input: String): Option[(String,Int)] = {
    Try {
      val array = input.split("\\s+")
      (array(0),1)
    } match {
      case Success(value) => Some(value)
      case Failure(exception) => None
    }
  }

  def getSession(database:String, collection: String): SparkSession = {
    SparkSession.builder()
      .master("local[*]")
      .appName("stream-saver")
      .config("spark.mongodb.output.uri", s"mongodb://127.0.0.1:27017/$database.$collection")
      .config("spark.mongodb.input.uri", s"mongodb://127.0.0.1:27017/$database.$collection")
      .getOrCreate()
  }

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local[*]").setAppName("Main runner")
    val scc = new StreamingContext(conf, Seconds(5))
    scc.sparkContext.setLogLevel("ERROR")

    val lines = scc.socketTextStream("127.0.0.1", 9999)

    val occurrences = lines.mapPartitions{partition=>
      partition.flatMap(extractWords)
    }

    val wordCountIntermediate = occurrences.reduceByKey((x,y)=> x+y)

    val wordCount = wordCountIntermediate.map{item => Word(item._1,item._2)}

    wordCount.foreachRDD{rdd=>
      val session = getSession("personas","person")
      import session.implicits._

      val df = rdd.toDF()
      df.show()
    }
    scc.start()
    scc.awaitTermination()
  }
}
