package com.mongostream

import java.util.UUID

import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.bson.codecs.DEFAULT_CODEC_REGISTRY
import org.bson.codecs.configuration.CodecRegistry
import org.mongodb.scala.bson.codecs.Macros._

case class Word(_id: UUID, word: String, occurrences: Int)

object Word {
  def apply(_id: UUID, word: String, occurences: Int): Word = new Word(_id, word, occurences)

  def apply(word: String, occurences: Int): Word = new Word(UUID.randomUUID(), word, occurences)

  implicit val codecRegistry: CodecRegistry = fromRegistries(fromProviders(classOf[Word]), DEFAULT_CODEC_REGISTRY)
}
