package com.chpt04
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkConf, SparkContext}

object ReadWords extends App {
//  val conf = new SparkConf().setMaster("local").setAppName("Appek")
  val sc = new SparkContext("local", "Appek")
  val lines = sc.textFile("/home/maciek/spark/spark-2.4.4-bin-hadoop2.7/README.md")
  val words = lines.flatMap(line => line.split("\\s+")).map{word => (word, 1)}. reduceByKey((x,y) => x +y)
    .persist(StorageLevel.MEMORY_ONLY)

  words.foreach(println)
  println(s"Total: ${words.count()}")
  val more1 = words.filter{y => y._2 > 1}.persist(StorageLevel.MEMORY_ONLY)
//  val more1 = words.filter{case (x,y) => y > 1}.persist(StorageLevel.MEMORY_ONLY)
  println(s"Total: ${more1.count()}")
  println(s"Words: ${more1.keys.collect().mkString(", ")}")
  println(s"Words: ${more1.sortByKey().keys.collect().mkString(", ")}")

  val diff = words.subtractByKey(more1).persist(StorageLevel.MEMORY_ONLY)
  diff foreach println

  println(s"diff total ${diff.count()}")

  val join = diff.join(words).persist(StorageLevel.MEMORY_ONLY)
  join.collect() foreach println

  println("Other - left join")
  words.leftOuterJoin(diff).foreach(println)
}
