package com.chpt04

import org.apache.spark.SparkContext

object ReadWords4 extends App {
  //  val conf = new SparkConf().setMaster("local").setAppName("Appek")
  val sc = new SparkContext("local", "Appek")
  val lines = sc.textFile("/home/maciek/spark/spark-2.4.4-bin-hadoop2.7/README.md")
//  val words = lines.flatMap(line => line.split("\\s+")).map { word => (word, 1) }.reduceByKey((x, y) => x + y)
  val words = lines.flatMap(line => line.split("\\s+")).countByValue()

  val result = lines.flatMap(line => line.split("\\s+")).map{item => (item, 1)}.repartition(4)
    .aggregateByKey((0,0))((acc:(Int,Int), value) => (acc._1+value, acc._2+1), (acc1:(Int,Int), acc2:(Int,Int))=> (acc1._1+ acc2._1, acc1._2 + acc2._2))
    .mapValues(v => v._1/ v._2.toFloat)
  result foreach println
  println(result.partitions.size)
}
