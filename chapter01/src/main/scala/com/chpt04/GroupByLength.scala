package com.chpt04

import org.apache.spark.SparkContext
import org.apache.spark.storage.StorageLevel

object GroupByLength extends App {
  //  val conf = new SparkConf().setMaster("local").setAppName("Appek")
  val sc = new SparkContext("local", "Appek")
  val lines = sc.textFile("/home/maciek/spark/spark-2.4.4-bin-hadoop2.7/README.md")
//  val words = lines.flatMap(line => line.split("\\s+")).map { word => (word, 1) }.reduceByKey((x, y) => x + y)
  val words = lines.flatMap(line => line.split("\\s+"))

  words.groupBy(_.length).foreach(println)
}
