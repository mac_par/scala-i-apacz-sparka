package com.chpt04

import org.apache.spark.{HashPartitioner, SparkContext}
import org.apache.spark.storage.StorageLevel

object WordSort extends App {
  val sc = new SparkContext("local", "Appek")
  val lines = sc.textFile("/home/maciek/spark/spark-2.4.4-bin-hadoop2.7/README.md")
    .flatMap{line => line.split("\\s+")}.map(word => (word, 1)).persist(StorageLevel.MEMORY_ONLY)
  lines.sortByKey(ascending = true, numPartitions = 2).foreach(println)
  //no partitioner
  println(lines.partitioner)
  //setting partitioning to 15 partitions
  val newLines = lines.partitionBy(new HashPartitioner(15))
  println(lines.partitioner)
  println(newLines.partitioner.get.numPartitions)


  val words = sc.textFile("/home/maciek/spark/spark-2.4.4-bin-hadoop2.7/README.md").flatMap{line =>
    line.split("\\s+")
  }.persist(StorageLevel.MEMORY_ONLY)
}
