package com.chapter06.accumulators

import java.nio.file.Paths

import org.apache.spark.storage.StorageLevel
import org.apache.spark.util.LongAccumulator
import org.apache.spark.{SparkConf, SparkContext}

object CountBlankLines {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local").setAppName("countBlanks")
    val sc = new SparkContext(conf)

    val path = Paths.get("output/blankLines.txt")
    if (path.toFile.exists()) {
      path.toFile.listFiles().foreach{_.delete()}
      path.toFile.delete()
    }
    val blankLines: LongAccumulator = sc.longAccumulator

    val input = sc.textFile("chapter01/src/main/resources/files/blanklines.txt").persist(StorageLevel.MEMORY_ONLY)
    println(s"Lines: ${input.count()}")

    val result = input.mapPartitions { part =>

      part.flatMap { line =>
        if ("".equals(line)) {
          blankLines add 1;
          None
        } else {
          line.split("\\s+")
        }
      }
    }.saveAsTextFile("output/blankLines.txt")

    println(s"Blank Lines: ${blankLines.value}")
    sc.stop()
  }
}
