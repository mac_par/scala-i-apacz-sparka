package com.chapter06.accumulators.hamradio

import java.nio.file.Paths
import java.util.Objects

import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper}
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkConf, SparkContext}
import org.eclipse.jetty.client.{ContentExchange, HttpClient}

import scala.util.{Failure, Success, Try}

object CheckHamRadioStations {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local").setAppName("HamRadios")
    val sc = new SparkContext(conf)

    clearStations

    val radioStationsAmount = sc.longAccumulator("radioStations")

    val radioStations = sc.textFile("chapter01/src/main/resources/files/callsigns").map { station =>
      radioStationsAmount add 1
      station.trim
    }

    val result = radioStations.distinct().mapPartitions { signs =>
      val mapper = createMapper
      val httpClient = new HttpClient()
      httpClient.start()
      signs.map { sign =>
        createExchangeForSign(sign, httpClient)
      }.map { content =>
        (content._1, parseContent(content._2, mapper))
      }.filter{ item =>
        item._2.isDefined
      }.map{station =>
        (station._1, station._2.get)
      } .filterNot{item => Objects.isNull(item._2)}
    }.persist(StorageLevel.MEMORY_ONLY)
    println(s"Total radio stations: ${radioStationsAmount.value}")

    result foreach println
    result.mapPartitions{partition=>
      val mapper = createMapper

      partition.map{station =>
        Try {
          mapper.writeValueAsString(station._2)
        } match {
          case Success(value) => value
          case Failure(ex) => None
        }
      }
    }.saveAsTextFile("output/radio-stations.txt")
    sc.stop()
  }

  def createMapper: ObjectMapper = {
    val mapper = new ObjectMapper()
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    mapper.registerModule(DefaultScalaModule)
    mapper
  }

  def createExchangeForSign(sign: String, client: HttpClient): (String, ContentExchange) = {
    val contentExchange = new ContentExchange()
    contentExchange.setURL(s"http://new73s.herokuapp.com/qsos/$sign.json")
    client.send(contentExchange)
    (sign, contentExchange)
  }

  def parseContent(input: ContentExchange, mapper: ObjectMapper): Option[Array[HamRadioLog]] = {
    Try {
      val result = input
      result.waitForDone()
      val response = result.getResponseContent
      mapper.readValue(response, classOf[Array[HamRadioLog]])
    } match {
      case Success(value) => Some(value)
      case Failure(exception) => None
    }
  }

  def clearStations: Unit = {
    val path = Paths.get("output/radio-stations.txt").toFile
    if (path.exists()) {
      path.listFiles().foreach{_.delete()}
      path.delete()
    }
  }
}
