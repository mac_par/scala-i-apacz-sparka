package com.chapter06.accumulators.hamradio
import com.fasterxml.jackson.annotation.JsonAlias

class HamRadioLog(val address: String, val age: Int, band: String, @JsonAlias(Array("callsign")) val callSign:String,
                  val city: String, val comment: String, @JsonAlias(Array("confirmcode")) val confirmCode: String,
                  @JsonAlias(Array("contactgrid")) val contactGrid: String, @JsonAlias(Array("contactlat")) val contactLat: Double,
                  @JsonAlias(Array("contactlong")) val contactLong: Double, @JsonAlias(Array("contacttime")) val contactTime: String,
                  val continent: String, val country: String, val county: String, @JsonAlias(Array("created_at")) val createdAt: String){
  override def toString = s"HamRadioLog(address=$address, age=$age, callSign=$callSign, city=$city, comment=$comment, confirmCode=$confirmCode, contactGrid=$contactGrid, contactLat=$contactLat, contactLong=$contactLong, contactTime=$contactTime, continent=$continent, country=$country, county=$county, createdAt=$createdAt)"
}
