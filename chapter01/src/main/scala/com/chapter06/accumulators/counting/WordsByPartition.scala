package com.chapter06.accumulators.counting
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkConf, SparkContext}
object WordsByPartition {
  type Jakis = (String,Int)
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local").setAppName("WordCount")
    val sc = new SparkContext(conf)

    //read file
    val lines = sc.textFile("chapter01/src/main/resources/files/blanklines.txt")
    val result = lines.mapPartitions(mapToPairs).reduceByKey((x,y)=> x+y)

    result foreach println
    sc.stop()
  }

  def mapToPairs(batch: Iterator[String]): Iterator[Jakis] = {
    batch.flatMap { line =>
        line.split("\\s+")
    }.map{(_,1)}
  }

  def sumOccurences(arg1:Int, arg2:Int): Int = {
    arg1 + arg1
  }
}
