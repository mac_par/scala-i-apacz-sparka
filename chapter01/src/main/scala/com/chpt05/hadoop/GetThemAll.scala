package com.chpt05.hadoop

import java.nio.file.Paths

import org.apache.hadoop.io.Text
import org.apache.spark.{SparkConf, SparkContext}

object GetThemAll {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local").setAppName("app")
    val sc = new SparkContext(conf)

    val outputFile = Paths.get("output/sequence")
    if (outputFile.toFile.exists()) {
      outputFile.toFile.listFiles() foreach {
        _.delete()
      }
      println("Deleting")
      outputFile.toFile.delete()
    }


    val lines = sc.textFile("chapter01/src/main/resources/files/callsign_tbl").map { line =>
      val arr = line.split(", ")
      (new Text(arr(0)), new Text(arr(1)))
    }.saveAsSequenceFile("output/sequence")
    val input = sc.sequenceFile("output/sequence",
      classOf[Text], classOf[Text], 3).map { pair => (pair._1.toString, pair._2.toString) }

    println("Reading file")
    input foreach println
    println("Reading file2")
    sc.sequenceFile[Text, Text]("output/sequence") foreach println

    sc.stop()
  }
}
