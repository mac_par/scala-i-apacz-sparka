package com.chpt05.json

import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper}
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkConf, SparkContext}

import scala.util.{Failure, Success, Try}

object ParseJson {
  def main(args: Array[String]): Unit = {
    val config = new SparkConf().setMaster("local").setAppName("Person JSON")
    val sc = new SparkContext(config)

    val input = sc.textFile("chapter01/src/main/resources/files/pandainfo.json")

    //proces using objectmapper - serialization
    //for efficiency - create object mapper per partition
    val result = input.mapPartitions{records =>
      val mapper = new ObjectMapper() with ScalaObjectMapper
      mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
      mapper.registerModule(DefaultScalaModule)

      records.flatMap { record =>
        Try{
          mapper.readValue(record, classOf[Person])
        } match {
          case Success(value) => Some(value)
          case Failure(_) => None
        }
      }
    }.persist(StorageLevel.MEMORY_ONLY)
    //display
    result foreach println

    //save to file only those who love pandas
    val toFile = result.filter{_.lovesPandas}.mapPartitions { partition =>
      val mapper = new ObjectMapper() with ScalaObjectMapper
      mapper.registerModule(DefaultScalaModule)

      partition.map(mapper.writeValueAsString(_))
    }

    toFile.saveAsTextFile("output/jsonBrother.json")
    sc.stop()
  }
}
