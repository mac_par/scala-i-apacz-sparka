package com.chpt05.json.hive
import org.apache.spark.sql.SparkSession
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkConf, SparkContext}

object ReadJson {
  def main(args:Array[String]):Unit = {
    val conf = new SparkConf().setMaster("local").setAppName("readJsonWithHive")
    val ss = SparkSession.builder().enableHiveSupport().config(conf).getOrCreate()
    val tweets = ss.read.json("chapter01/src/main/resources/files/simpleTweet.json")
    tweets.createOrReplaceTempView("tweets")
    val results = tweets.sqlContext.sql("select user.name, user.location, text from tweets").persist(StorageLevel.MEMORY_ONLY)

    println(results.count())
    results.foreach{item => println(s"${item(0)}, ${item(1)}, ${item(2)}")}
    ss.close()
  }
}
