package com.chpt05.csv

import java.io.{StringReader, StringWriter}

import au.com.bytecode.opencsv.{CSVReader, CSVWriter}
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkConf, SparkContext}

object CSVAll {
  def main(args: Array[String]): Unit = {
    val config = new SparkConf().setMaster("local").setAppName("CSVAll")
    val sc = new SparkContext(config)

    val input = sc.wholeTextFiles("chapter01/src/main/resources/files/*.csv")
    val result = input.mapPartitions{ partition =>
           partition.map{case(_,txt) =>
           val csvReader = new CSVReader(new StringReader(txt))

               csvReader.readAll()
           }
    }.persist(StorageLevel.MEMORY_ONLY)

    result foreach println

    result.mapPartitions{partition =>
      val stringWriter = new StringWriter()
      partition.map{array =>
        val csvWriter = new CSVWriter(stringWriter)
        csvWriter.writeAll(array)
        val string = stringWriter.toString
        stringWriter.flush()
        string
      }
    }.saveAsTextFile("output/csvTotal.csv")
    sc.stop()
  }
}
