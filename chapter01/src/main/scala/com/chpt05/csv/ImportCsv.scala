package com.chpt05.csv
import java.io.{StringReader, StringWriter}

import au.com.bytecode.opencsv.{CSVParser, CSVReader, CSVWriter}
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkConf, SparkContext}

object ImportCsv {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local").setAppName("CSVDivider")
    val sc = new SparkContext(conf)
    val input = sc.textFile("chapter01/src/main/resources/files/favourite_animals.csv")
      .map {line =>
        val csvReader = new CSVReader(new StringReader(line))
        csvReader.readNext()
      }.persist(StorageLevel.MEMORY_ONLY)

    input foreach{ line => println(line.mkString(" "))}

    val toFile = input.map{arr =>
      arr.mkString(":")
    }.persist(StorageLevel.MEMORY_ONLY)

    toFile foreach println

    toFile.saveAsTextFile("output/csvFile.csv")
    sc.stop()
  }
}
