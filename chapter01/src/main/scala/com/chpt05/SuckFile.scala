package com.chpt05

import org.apache.spark.{SparkConf, SparkContext}

object SuckFile extends App {
  val conf = new SparkConf().setMaster("local").setAppName("SuckFile")
  val sc = new SparkContext(conf)

  val lines = sc.wholeTextFiles("chapter01/src/main/resources/files/", 2)
  lines.foreach(println)

  val logOnly = sc.wholeTextFiles("chapter01/src/main/resources/files/*.log")
  val resultLogs = logOnly mapValues{item => item.split("\\s+")}

  resultLogs.mapValues{item => item.mkString(":")}.saveAsTextFile("output/log-result.txt")
  sc.stop()
}
