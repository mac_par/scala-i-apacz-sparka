package com.mongus.classen
import org.mongodb.scala.bson.ObjectId
import org.mongodb.scala.bson.codecs.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.bson.codecs.configuration.CodecRegistry

/**
 *
 * @param _id - id will be accessible
 * @param firstName -first name
 * @param lastName - last name
 */
case class Person(_id: ObjectId, firstName: String, lastName: String)

object Person {
  def apply(_id: ObjectId, firstName: String, lastName: String): Person = new Person(_id, firstName, lastName)
  def apply(firstName: String, lastName: String): Person = new Person(new ObjectId, firstName, lastName)

  //case classes can use default codec
  val codecRegistry: CodecRegistry = fromRegistries(fromProviders(classOf[Person]), DEFAULT_CODEC_REGISTRY)
}
