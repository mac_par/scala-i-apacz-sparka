package com.mongus.classen

import org.mongodb.scala.model.{Filters, Updates}
import org.mongodb.scala.result.UpdateResult
import org.mongodb.scala.{Completed, Document, MongoClient, MongoClientSettings, MongoCollection, ServerAddress}

import scala.collection.JavaConverters._

object ClazzCazen extends App {
  val settings = MongoClientSettings.builder().applyToClusterSettings{block => block.hosts(List(new ServerAddress("127.0.0.1")).asJava)}.build()

  val client = MongoClient(settings)
  val db = client.getDatabase("domek").withCodecRegistry(Person.codecRegistry)
  val personsCollection: MongoCollection[Person] = db.getCollection("domek.rozne.persons")
  val p1 = Person("Marko","Polo")

  personsCollection.insertOne(p1).subscribe((p:Completed) => println(s"Inserted: $p"), (error: Throwable)=> println(s"Error: ${error.getMessage}"), () => println("finished"))

  java.util.concurrent.TimeUnit.SECONDS.sleep(3)

  //insert multiple
  val persons = List(Person("Ada", "Wong"), Person("Leon","Kenedy"), Person("Zombie", "Trump"))
  personsCollection.insertMany(persons).subscribe((p:Completed) => println(s"Inserted: $p"), (error: Throwable)=> println(s"Error: ${error.getMessage}"), () => println("finished"))

  java.util.concurrent.TimeUnit.SECONDS.sleep(5)
  //finding elements
  personsCollection.find().first().subscribe((p:Person) => println(s"First: $p"))
  personsCollection.find(Filters.eq("firstName","Leon")).subscribe((p:Person) => println(s"EQ: $p"))
  personsCollection.find(Filters.regex("lastName","^T")).subscribe((p:Person) => println(s"Regex: $p"))
  personsCollection.find().subscribe((p:Person) => println(s"All: $p"))
  java.util.concurrent.TimeUnit.SECONDS.sleep(10)
  //update Donald Trump
  personsCollection.updateOne(Filters.eq("firstName","Zombie"), Updates.set("firstName","Donald")).subscribe((u:UpdateResult)=> println(s"Update: $u"))
  java.util.concurrent.TimeUnit.SECONDS.sleep(2)

  println("Displaying all")
  personsCollection.find().subscribe((p: Person) => println(p))

  java.util.concurrent.TimeUnit.SECONDS.sleep(5)
  println("Bye!")
  client close()
}
