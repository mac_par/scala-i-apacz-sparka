package com.mongus
import java.util.concurrent.TimeUnit

import org.mongodb.scala.{Document, FindObservable, MongoClient, MongoClientSettings, ServerAddress}
import scala.collection.JavaConverters._

object FindAll {
  def main(args: Array[String]): Unit = {
    val clientSettings = MongoClientSettings.builder().applyToClusterSettings{block => block.hosts(List(new ServerAddress("127.0.0.1")).asJava)}.build()

    val client = MongoClient(clientSettings)

    val db = client.getDatabase("domek")
    val collection = db.getCollection("domek.rozne")
    val result: FindObservable[Document] = collection.find()

    result.subscribe((item:Document)=> println(item), (error:Throwable) => println(s"Error: ${error.getMessage}"), () => println("Finished"))

    TimeUnit.SECONDS.sleep(5)
    println("Closing")
    client.close()
  }
}
