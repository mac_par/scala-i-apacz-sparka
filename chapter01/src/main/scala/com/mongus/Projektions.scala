package com.mongus

import java.util.concurrent.TimeUnit

import scala.collection.JavaConverters._
import org.mongodb.scala.{Document, MongoClient, MongoClientSettings, ServerAddress}
import org.mongodb.scala.model.Projections

object Projektions {
  def main(args:Array[String]): Unit = {
    val settings = MongoClientSettings.builder().applyToClusterSettings{block => block.hosts(List(new ServerAddress("127.0.0.1")).asJava)}.build()
    val client = MongoClient(settings)

    val db = client.getDatabase("domek")
    val collection = db.getCollection("domek.rozne")

    //bez id - 25
    val documents1 = collection.find().limit(25).projection(Projections.excludeId()).subscribe((item:Document)=> println(s"bezId: $item"))

    //od 16-25
    val documents2 = collection.find().projection(Projections.slice("i",5,10)).subscribe((item:Document)=> println(s"slice: $item"))

    val documents3 = collection.find().projection(Projections.exclude("i")).subscribe((item:Document)=> println(s"bez 'i': $item"))

    TimeUnit.SECONDS.sleep(15)
    println("Bye!")

    client.close()
  }
}
