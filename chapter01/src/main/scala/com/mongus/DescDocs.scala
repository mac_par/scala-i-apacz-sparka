package com.mongus

import java.util.concurrent.TimeUnit

import org.mongodb.scala.{Document, MongoClient, MongoClientSettings, MongoCollection, ServerAddress}

import scala.collection.JavaConverters._
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Sorts._

object DescDocs extends App {
  val settings = MongoClientSettings.builder().applyToClusterSettings{block => block.hosts(List(new ServerAddress("127.0.0.1")).asJava)}.build()

  val client = MongoClient(settings)
  val db = client.getDatabase("domek")
  val collection = db.getCollection("domek.rozne")
  //tylko 5 najwyzszych malejacych wartosci

  val findings = collection.find(gt("i",90)).sort(descending("i")).limit(5)
  findings.subscribe((doc: Document) => println(doc), (error: Throwable)=> println(s"Error: ${error.getMessage}", ()=>println("Koniec")))

  TimeUnit.SECONDS.sleep(5)
  println("Next")

  collection.find().batchSize(10).subscribe((doc: Document) => println(doc), (error: Throwable)=> println(s"Error: ${error.getMessage}", ()=>println("Koniec")))
  TimeUnit.SECONDS.sleep(5)
  println("Bye!")
  client.close()
}
