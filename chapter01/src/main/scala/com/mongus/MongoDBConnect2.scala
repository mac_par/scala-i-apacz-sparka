package com.mongus


import com.mongodb.ConnectionString
import org.mongodb.scala.{MongoClientSettings, MongoClient, MongoDatabase}

import scala.collection.JavaConverters._
//using scala objects
object MongoDBConnect2 {
  def main(args: Array[String]): Unit = {
    val settings = MongoClientSettings.builder().applyConnectionString(new ConnectionString("mongodb://127.0.0.1:27017"))
      .build()

    val client: MongoClient = MongoClient(settings)
    val db: MongoDatabase = client.getDatabase("domek")
    println(db.codecRegistry)
    println(db.name)
    for {
      name <- db.listCollections()
    } {
      println(name)
    }

    client.close()
  }
}