package com.mongus
import java.util.concurrent.TimeUnit

import org.mongodb.scala.{Document, MongoClient, MongoClientSettings, ServerAddress}
import org.mongodb.scala.model.Filters.gte

import scala.collection.JavaConverters._

object MongoGreaterThan50 extends App {
  val settings = MongoClientSettings.builder().applyToClusterSettings{block => block.hosts(List(new ServerAddress("127.0.0.1")).asJava)}.build()

  val client = MongoClient(settings)
  val db = client.getDatabase("domek")
  val collection = db.getCollection("domek.rozne")
  val result = collection.find(gte("i", 50))

  result.subscribe((item: Document)=> println(item), (error:Throwable) => println(s"Error: ${error.getMessage}"), () => println("Finished"))

  TimeUnit.SECONDS.sleep(5)
  println("closing")
  client.close()
}
