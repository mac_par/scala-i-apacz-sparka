package com.mongus

import com.mongodb.{MongoClient, ServerAddress}
import com.mongodb.client.MongoDatabase

import scala.collection.JavaConverters._

object MongoDBConnect1 {
  def main(args: Array[String]): Unit = {

    val client: MongoClient = new MongoClient(new ServerAddress("127.0.0.1"))

    val db: MongoDatabase = client.getDatabase("domek")

    println(db.getName)
    for {
      name <- db.listCollections().asScala
    } {
      println(name)
    }

    client.close()
  }
}