package com.mongus

import java.util.concurrent.TimeUnit

import org.mongodb.scala.{MongoClient, MongoClientSettings, ServerAddress}

import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

object QueryFirst extends App {
  val settings = MongoClientSettings.builder().applyToClusterSettings { block => block.hosts(List(new ServerAddress("127.0.0.1")).asJava) }.build()
  val client = MongoClient(settings)

  val db = client.getDatabase("domek")

  val result = db.getCollection("domek.rozne").find().first()
  result.head().onComplete {
    case Success(value) => println(value)
    case Failure(exception) => println(exception.getMessage)
  }(ExecutionContext.global)

  TimeUnit.SECONDS.sleep(5)

  client.close()
}
