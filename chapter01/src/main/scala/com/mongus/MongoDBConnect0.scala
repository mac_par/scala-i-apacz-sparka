package com.mongus

import com.mongodb.MongoClient
import com.mongodb.client.MongoDatabase

import scala.collection.JavaConverters._

object MongoDBConnect0 extends App {
  val client: MongoClient = new MongoClient()

  println(client.getConnectPoint)
  val db: MongoDatabase = client.getDatabase("domek")

  println(db.getName)
  for {
    item <- db.listCollections().asScala
    set <- item.entrySet().asScala
  } {
    println(set)
  }

  client.close()
}
