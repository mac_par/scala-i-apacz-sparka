package com.mongus

import java.util.concurrent.TimeUnit

import scala.collection.JavaConverters._
import org.mongodb.scala.{Completed, Document, MongoClient, MongoClientSettings, MongoCollection, Observable, ServerAddress, SingleObservable}

import scala.collection.immutable
import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success, Try}

object MongoDbConn3 {
  def main(args: Array[String]):Unit = {
    val settings = MongoClientSettings.builder().applyToClusterSettings{block =>
      block.hosts(List(new ServerAddress("127.0.0.1")).asJava)
    }.build

    val client = MongoClient(settings)
    val db = client.getDatabase("domek")

    val collection: MongoCollection[Document] = db.getCollection("rozne")
    val doc: Document = Document("_id"->"", "title" -> "Refi must die","director"-> "Erdogan's supportees", "year" -> "eternity")

    val row: SingleObservable[Completed] = collection.insertOne(doc)

    row.subscribe((result:Completed)=> println(s"Inserted: ${result.toString()}"),
      (error:Throwable) => println(s"Error occurred ${error.getMessage}"), () => println("completed") )

    val documents: immutable.Seq[Document] = (1 to 100).map{ item => Document("i"-> item)}
    val insertMany: SingleObservable[Completed] = collection.insertMany(documents)

    val counter: Observable[Long] = for {
      inserted <- insertMany
      counted <- collection.countDocuments()
    } yield counted

    counter.head().onComplete {
      case Success(value) => println(s"total # of documents after inserting 100 small ones (should be 101):  $value")
      case Failure(ex) => println(s"Exception: ${ex.getMessage}")
    }(ExecutionContext.global)


    TimeUnit.SECONDS.sleep(5)

    client.close()
  }

}
