package com.mongus

import java.util.concurrent.TimeUnit

import scala.collection.JavaConverters._
import org.mongodb.scala.{Completed, Document, MongoClient, MongoClientSettings, ServerAddress}
import org.mongodb.scala.model.{Filters, Updates}
import org.mongodb.scala.result.UpdateResult

object Updateing extends App {
  val settings = MongoClientSettings.builder().applyToClusterSettings{block => block.hosts(List(new ServerAddress("127.0.0.1")).asJava)}.build()

  val client = MongoClient(settings)

  val db = client.getDatabase("domek")
  val collection = db.getCollection("domek.rozne.marko")


  (1 until 25).foreach{i =>
    collection.insertOne(Document("name"->"Marko", "i"->i)).subscribe((item: Completed) => println(item.toString()))
  }

  TimeUnit.SECONDS.sleep(5)
  println("updating values: 5 set 110")

  collection.updateOne(Filters.mod("i",5,0), Updates.set("i", 110)).subscribe((item: UpdateResult)=> println(s"Single ${item.getUpsertedId}"))

  println("updating values: mod5 - chanigng i -> j")
  collection.updateMany(Filters.mod("i",5,0),Updates.rename("i", "j")).subscribe((item:UpdateResult)=> println(s"Multi: ${item.getUpsertedId}"))


  TimeUnit.SECONDS.sleep(5)

  println("increment all smaller that 100 for \"i\"")
  collection.updateMany(Filters.lt("i",100), Updates.inc("i",100)).subscribe((item: UpdateResult) => println(s"Inc: ${item.getModifiedCount}"))

  TimeUnit.SECONDS.sleep(5)


  println("Koniec")
  client.close()
}
