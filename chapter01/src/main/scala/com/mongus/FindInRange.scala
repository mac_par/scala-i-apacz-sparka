package com.mongus
import java.util.concurrent.TimeUnit

import scala.collection.JavaConverters._
import org.mongodb.scala.{Document, MongoClient, MongoClientSettings, MongoDatabase, ServerAddress}
import org.mongodb.scala.model.{Filters, Sorts}

object FindInRange {
  def main(args: Array[String]):Unit = {
    val settings = MongoClientSettings.builder().applyToClusterSettings{block => block.hosts(List(new ServerAddress("127.0.0.1")).asJava)}.build()

    val client = MongoClient(settings)
    val db: MongoDatabase = client.getDatabase("domek")
    val collection = db.getCollection("domek.rozne")

    val documents = collection.find(Filters.and(Filters.gt("i",43), Filters.lte("i",66), Filters.nin("i", 59)))
        .subscribe((item: Document)=> println(item), (error:Throwable)=> println(s"Error has occurred: ${error.getMessage}"), () => println("Finished"))

    TimeUnit.SECONDS.sleep(5)
    println("Koniec")
    client.close()
  }
}
