package com.mongus

import org.mongodb.scala.model.Filters
import org.mongodb.scala.result.DeleteResult
import org.mongodb.scala.{Completed, MongoClient, MongoClientSettings, ServerAddress}

import scala.collection.JavaConverters._

object Deleting {
  def main(args: Array[String]): Unit = {
    val settings = MongoClientSettings.builder().applyToClusterSettings { block => block.hosts(List(new ServerAddress("127.0.0.1")).asJava) }.build()

    val client = MongoClient(settings)
    val db = client.getDatabase("domek")
    val collection = db.getCollection("domek.rozne.marko")

    println("Deleting an element with \"j\"=15")
    collection.deleteOne(Filters.eq("j", 15)).subscribe((item: DeleteResult) => println(s"Item deleted: ${item}"))

    java.util.concurrent.TimeUnit.SECONDS.sleep(5)

    println("Remove all with value greater that 100")
    collection.deleteMany(Filters.gte("i",100)).subscribe((item: DeleteResult) => println(s"Deleting: $item"))

    java.util.concurrent.TimeUnit.SECONDS.sleep(5)
    println("droping collection domek.rozne.marko")
    collection.drop().subscribe((item: Completed) => println(s"Finito: $item"))

    java.util.concurrent.TimeUnit.SECONDS.sleep(3)

    println("Bye")
    client.close()
  }
}
