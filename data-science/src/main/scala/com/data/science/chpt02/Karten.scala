package com.data.science.chpt02

import org.apache.spark.SparkContext

object Karten extends App {
  val sc = new SparkContext("local[*]", "Karten")

  val a = sc.parallelize(1 to 5)
  val b = sc.parallelize(3 until 6)

  val karten = a.cartesian(b)

  karten.foreach(println)
  sc.stop()
}
