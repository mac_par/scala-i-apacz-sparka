package com.data.science.chpt02

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

object Zippen {
  def main(args: Array[String]) : Unit = {
    val spark = new SparkContext("local[*]", "zippen")
    spark.setLogLevel("INFO")

    val strRdd = spark.parallelize(List("human", "alien", "Convid-19", "death"), 3)
    val mappedRDD: RDD[Int] = strRdd.map(_.length)
    val zippen: RDD[(String, Int)] = strRdd.zip(mappedRDD)

    zippen.foreach(println)
    spark.stop()
  }
}
