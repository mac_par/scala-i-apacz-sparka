package com.data.science.chpt02

import org.apache.spark.SparkContext

object Grupen {
  def main(args: Array[String]): Unit = {
    val sc = new SparkContext("local[*]", "grupen")

    sc.setLogLevel("INFO")
    val data = sc.parallelize(List("human","post","Cron","jeel"))
    val keyed = data.keyBy(_.length)

    val cos = keyed.groupByKey(3).collect()
    cos.foreach(println)
    sc.stop()
  }
}
