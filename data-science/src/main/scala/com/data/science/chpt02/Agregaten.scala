package com.data.science.chpt02

import org.apache.spark.SparkContext

object Agregaten extends App {
  val sc = new SparkContext("local[*]", "agren")

  sc.setLogLevel("INFO")
  val listen = sc.parallelize(List("human","post","Cron","jeel", "szmbowus"), 3)

  val keyed = listen.keyBy(_.length)

  val reduced = keyed.reduceByKey(_ + _)

  reduced.foreach(println)

  sc.stop()
}
