package com.udemy.part1.example4

import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql._

import scala.util.{Failure, Success, Try}

object TotalAmountPerCustomer extends App {
  def extractData(arg:String): Option[(Int,Float)] = {
    Try {
      val array = arg.split(",")
      val customerId = array(0).toInt
      val mushtarayat = array(2).toFloat
      customerId -> mushtarayat
    } match {
      case Success(value) => Some(value)
      case Failure(exception) => None
    }
  }

  val conf = new SparkConf().setMaster("local[*]").setAppName("TotalMushtarayat per client")
  val sc = new SparkContext(conf)
  sc.setLogLevel("ERROR")

  val rawData = sc.textFile("data/SparkScala/customer-orders.csv")
  val intermediateValues = rawData.repartition(4).mapPartitions{partition =>
    partition.flatMap(extractData)
  }.persist(StorageLevel.MEMORY_ONLY)

  //general approach
  val totalResults = intermediateValues.reduceByKey((x,y)=> x+y)

  totalResults.sortBy(_._2,ascending = false, numPartitions = 1).foreach(println)

  //sql-based approach
  val sparkSession = SparkSession.builder().config(conf).getOrCreate()
  import sparkSession.implicits._
  val sqlRecords = intermediateValues.toDF("customer", "spent")
  sqlRecords.createOrReplaceTempView("ecommerce")
  val results = sparkSession.sql("select distinct max(customer) as customer_id, format_number(sum(spent),2) as total_mushtarayaat" +
    " from ecommerce group by customer order by total_mushtarayaat desc")
  results.show(200)

  sc.stop()
}
