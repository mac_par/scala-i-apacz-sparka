package com.udemy.part1.example2

import org.apache.spark.{SparkConf, SparkContext}

import scala.util.{Failure, Success, Try}

object MaxTempPerStation {
  private def extractTuple(arg: String): Option[(String, String, Float)] = {
    Try {
      val array = arg.split(",")
      val stationId = array(0)
      val fieldType = array(2)
      val value = array(3).toFloat * 0.1F
      (stationId, fieldType, value)
    } match {
      case Success(value) => Some(value)
      case Failure(exception) => None
    }
  }

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local[*]").setAppName("MaxTempByStation")
    val sc = new SparkContext(conf)
    val input = sc.textFile("data/SparkScala/1800.csv")

    val initialTuples = input.mapPartitions { partition =>
      partition.flatMap {
        extractTuple
      }.filter {
        _._2 == "TMAX"
      }.map { record => record._1 -> record._3 }
    }

    val maxTempGroupedByStation = initialTuples.groupByKey

    val maxTempByStationReducedSet = maxTempGroupedByStation.mapValues(_.max).collect()

    maxTempByStationReducedSet.foreach{ item=>
      println(f"Max temperature ${item._2}%.2f at station ${item._1}%s")
    }

    sc.stop()
  }
}
