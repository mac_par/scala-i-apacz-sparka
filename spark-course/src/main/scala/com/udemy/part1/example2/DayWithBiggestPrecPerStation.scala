package com.udemy.part1.example2

import org.apache.spark.{SparkConf,SparkContext}
import scala.util.{Try, Success, Failure}

object DayWithBiggestPrecPerStation extends App {
  def parseTuple(str: String): Option[(String,String,String,Float)] = {
    Try{
      val array = str.split(",")
      val stationId = array(0)
      val date = array(1)
      val field = array(2)
      val value = array(3).toFloat

      (stationId, date, field, value)
    } match {
      case Success(value) => Some(value)
      case Failure(exception) => None
    }
  }

  private def reduceDataByValue(arg1: (String,Float), arg2:(String,Float)): (String,Float) = if (arg1._2 > arg2._2) arg1 else arg2

  val conf = new SparkConf().setMaster("local[*]").setAppName("Highest Prec by date")
  val sc = new SparkContext(conf)

  val initialDataSet = sc.textFile("data/SparkScala/1800.csv")
  val precPerStationAndDate = initialDataSet.mapPartitions{partition =>
    partition.flatMap{parseTuple}.filter{_._3 == "PRCP"}.map{item => item._1 -> (item._2, item._4)}
  }

  val reducedPrecPerStation = precPerStationAndDate.reduceByKey(reduceDataByValue)

  reducedPrecPerStation.sortByKey().foreach{entry =>
    println(f"${entry._1}%s: the highest prec ${entry._2._2}%.2f was recorder at ${entry._2._1}")
  }
  sc.stop()
}
