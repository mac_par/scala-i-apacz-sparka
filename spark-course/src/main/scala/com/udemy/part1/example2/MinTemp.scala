package com.udemy.part1.example2

import scala.util.{Try, Success, Failure}
import org.apache.spark.{SparkContext,SparkConf}

object MinTemp extends App {
  def extractData(arg:String): Option[(String,String,Float)] = {
    Try {
      val array = arg.split(",")
      val stationId = array(0)
      val fieldType = array(2)
      //present value in Celcius
      val temp = array(3).toFloat * 0.1F
      (stationId, fieldType, temp)
    } match {
      case Success(value) => Some(value)
      case Failure(exception) => None
    }
  }

  val conf = new SparkConf().setMaster("local[*]").setAppName("MinimalTemp")
  val sc = new SparkContext(conf)
  sc.setLogLevel("ERROR")

  val input = sc.textFile("data/SparkScala/1800.csv")

  val stationTempTuple = input.mapPartitions{ partitions =>
    partitions.flatMap{extractData}.filter{record => record._2 == "TMAX"}
      .map{item => (item._1, item._3)}
  }

  val lowestTempByStation = stationTempTuple.groupByKey().mapValues(_.min)

  lowestTempByStation.sortByKey().collect().foreach{ record =>
    println(f"At ${record._1}%s ${record._2}%.2f was registered.")
  }


  sc.stop()
}
