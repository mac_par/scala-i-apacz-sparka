package com.udemy.part1

import org.apache.spark.{SparkConf, SparkContext}

import scala.util.{Failure, Success, Try}

object AverageFriendsByAge {
  def extractAgeByFriends(arg: String): Option[(Int, Int)] = {
    Try {
      val array = arg.split(",")
      val age = array(2).toInt
      val friendsCount = array(3).toInt
      age -> friendsCount
    } match {
      case Success(value) => Option(value)
      case Failure(exception) => None
    }
  }

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local[*]").setAppName("Average Friends by Age")
    val sc = new SparkContext(conf)
    sc.setLogLevel("ERROR")

    val initialRecords = sc.textFile("data/SparkScala/fakefriends.csv")

    val mapFriends = initialRecords.mapPartitions { partition =>
      partition.flatMap(extractAgeByFriends)
    }

    val collectedFriends = mapFriends.groupByKey(3)

    val averageFriendsByAgeResult = collectedFriends.mapValues { item => item.sum / item.size }

    averageFriendsByAgeResult.sortByKey().foreach(println)
    sc.stop()
  }
}
