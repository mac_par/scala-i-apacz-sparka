package com.udemy.part1

import org.apache.spark.{SparkConf, SparkContext}

object RatingsCounter {
  def main(args: Array[String]): Unit = {
    val config = new SparkConf().setMaster("local[*]").setAppName("RatingCount")

    val sc = new SparkContext(config)
    sc.setLogLevel("ERROR")

    val lines = sc.textFile("data/ml-100k/u.data")

    val ratings = lines.map { line => line.split("\t")(2) }
    val resultsByValue = ratings.countByValue()

    //przekształć na sekwencje
    val sortedSequence = resultsByValue.toSeq.sortBy(_._1)

    //display
    sortedSequence.foreach(println)

  }
}
