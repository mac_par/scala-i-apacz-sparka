package com.udemy.part1.example7.ml

import java.nio.charset.{CodingErrorAction, StandardCharsets}

import org.apache.spark.ml.recommendation.ALS
import org.apache.spark.sql.{Row, SparkSession}

import scala.collection.mutable
import scala.io.{Codec, Source}

object Recomendations {
  def movieTitles: Map[Int, String] = {
    val codec: Codec = Codec(StandardCharsets.UTF_8)
    codec.onUnmappableCharacter(CodingErrorAction.REPLACE)
    codec.onMalformedInput(CodingErrorAction.REPLACE)

    val map: scala.collection.mutable.Map[Int, String] = scala.collection.mutable.Map[Int, String]()
    val source: Source = Source.fromFile("data/ml-100k/u.item")(codec)

    for {line <- source.getLines()} {
      val fields = line.split("\\|")
      map += (fields(0).toInt -> fields(1))
    }

    source.close
    map.toMap
  }

  case class Rating(userId: Int, movieId: Int, rating: Float)

  def main(args: Array[String]): Unit = {
    val session = SparkSession.builder().master("local[*]").appName("Recommendations").getOrCreate()
    session.sparkContext.setLogLevel("ERROR")
    import session.implicits._
    val moviesMap = movieTitles

    val data = session.read.textFile("data/ml-100k/u.data2").map(_.split("\\s+")).map(item => Rating(item(0).toInt, item(1).toInt, item(2).toFloat))
    val ratings = data.toDF()

    println("Recommendation model...")
    val als = new ALS()
      .setMaxIter(5)
      .setRegParam(0.01)
        .setUserCol("userId")
        .setItemCol("movieId")
        .setRatingCol("rating")

    val model = als.fit(ratings)

    val users = Seq(0).toDF("userId")
    val recommendations = model.recommendForUserSubset(users,10)

    println("Recommendations for user 0")

    for {userRec <- recommendations.collect()} {
      val subset = userRec(1)
      val tmp = subset.asInstanceOf[mutable.WrappedArray[Row]]
      for {row <- tmp} {
        val movie = row(0).asInstanceOf[Int]
        val rating = row(1).asInstanceOf[Float]
        val movieTitle = moviesMap(movie)
        println(s"$movieTitle score: $rating")
      }
    }
    session.stop()
  }
}
