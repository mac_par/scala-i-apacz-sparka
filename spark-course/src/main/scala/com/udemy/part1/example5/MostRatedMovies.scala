package com.udemy.part1.example5

import java.nio.charset.{CodingErrorAction, StandardCharsets}

import org.apache.spark.{SparkConf, SparkContext}

import scala.io.Codec
import scala.util.{Failure, Success, Try}

object MostRatedMovies {
  def movieTitleList: Map[Int, String] = {
    val codec = Codec(StandardCharsets.UTF_8)
    codec.onUnmappableCharacter(CodingErrorAction.REPLACE)
    codec.onMalformedInput(CodingErrorAction.REPLACE)

    val source = scala.io.Source.fromFile("data/ml-100k/u.item")(codec)
    val lines = source.getLines()

    val map = scala.collection.mutable.Map[Int, String]()

    for {line <- lines
         array = line.split("\\|")
         if array.length > 1
         } {
      map.put(array(0).toInt, array(1))
    }
    source.close()

    map.toMap
  }

  def extractMovie(line: String): Option[(Int, Int)] = {
    Try {
      val fields = line.split("\t")

      (fields(1).toInt, 1)
    } match {
      case Success(value) => Some(value)
      case Failure(exception) => None
    }
  }

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local[*]").setAppName("MoviesRatingsWithNames")

    val sc = new SparkContext(conf)
    sc.setLogLevel("ERROR")

    val rawData = sc.textFile("data/ml-100k/u.data")
    val moviesList = rawData.mapPartitions{partition =>
      partition.flatMap{extractMovie}
    }

    val moviesRatedTimes = moviesList.groupByKey().mapValues(_.size)

    val movieTitles = sc.broadcast(movieTitleList)

    val movieTitlesWithTimesRated = moviesRatedTimes.mapPartitions{partiton =>
      partiton.map{record => (record._2, movieTitles.value(record._1))}
    }

    val sortedMoviesByRatedTimes = movieTitlesWithTimesRated.groupByKey()

    sortedMoviesByRatedTimes.sortByKey(ascending = false)foreach(println)
    sc.stop()
  }
}
