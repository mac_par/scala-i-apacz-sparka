package com.udemy.part1.example5


import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.util.LongAccumulator

import scala.collection.mutable.ArrayBuffer

object SuperHeroBFS {
  val startCharacterId = 5306 //Spider-man
  val lookedCharacter = 14 //ADAM

  var hitCounter: Option[LongAccumulator] = None

  type BFSData = (Array[Int], Int, Int)
  type BFSNode = (Int, BFSData)

  def convertLineIntoNode(line: String): BFSNode = {
    val array = line.split("\\s+")
    val hero = array(0).toInt
    val edges:ArrayBuffer[Int] = ArrayBuffer[Int]()
    var distance:Int = 9999
    var colour: Int = 0 //white

    if (startCharacterId == hero) {
      colour = 1 //gray
      distance = 0
    }

    for {edge <- 1 until array.length} {
      edges += array(edge).toInt
    }

    (hero, (edges.toArray, distance,colour))
  }

  def getHeroesData(sc: SparkContext): RDD[BFSNode] = {
    sc.textFile("data/SparkScala/Marvel-graph.txt")
      .map(convertLineIntoNode)
  }

  def iterateNodes(node: BFSNode): Array[BFSNode] = {
    val container: ArrayBuffer[BFSNode] = ArrayBuffer[BFSNode]()
    val heroId:Int = node._1
    val edges: Array[Int] = node._2._1
    val distance:Int = node._2._2
    var color:Int = node._2._3

    //gray node
    if (color == 1) {
      for {edge <- edges} {

        if (lookedCharacter == edge) {
          if (hitCounter.isDefined) {
            hitCounter.get.add(1)
          }
        }

        val childNode: BFSNode = (edge, (Array(), distance + 1, color))
        container += childNode
      }

      color += 1
    }

    val processedNode: BFSNode = (heroId, (edges, distance, color))
    container += processedNode
    container.toArray
  }

  def reduceBFSNodes(node1: BFSData, node2: BFSData): BFSData = {
    val edges:ArrayBuffer[Int] = ArrayBuffer()
    var distance: Int = 9999
    var colour: Int = 0

    val edges1: Array[Int] = node1._1
    val distance1: Int = node1._2
    val colour1: Int = node1._3
    val edges2: Array[Int] = node2._1
    val distance2: Int = node2._2
    val colour2: Int = node2._3

    //if edges exists merge them
    if (!edges1.isEmpty) {
      edges ++= edges1
    }

    if (!edges2.isEmpty) {
      edges ++= edges2
    }

    //take lesser distance
    distance = Math.min(distance, Math.min(distance1,distance2))

    //take darker color
    colour = Math.max(colour, Math.max(colour1, colour2))

    (edges.toArray, distance, colour)
  }

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local[*]").setAppName("FindAdam")
    val sc = new SparkContext(conf)
    sc.setLogLevel("ERROR")

    var herosDataSet = getHeroesData(sc)
    hitCounter = Some(sc.longAccumulator("HeroHit"))

    for {iteration <- 1 to 20} {
      println(s"Iteration $iteration")

      val mapped = herosDataSet.flatMap(iterateNodes)
      println(s"Processing ${mapped.count} nodes")

      if (hitCounter.isDefined) {
        val value = hitCounter.get.value
        if (value > 0) {
          println(s"Hero hit! From $value directions")
          return
        }
      }

      herosDataSet = mapped.reduceByKey(reduceBFSNodes)
    }

    sc.stop()
  }
}