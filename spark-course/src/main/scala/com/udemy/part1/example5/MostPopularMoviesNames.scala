package com.udemy.part1.example5

import java.nio.charset.{CodingErrorAction, StandardCharsets}

import org.apache.spark.broadcast.Broadcast
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

import scala.io.{Codec, Source}
import scala.util.{Failure, Success, Try}

object MostPopularMoviesNames {
  def getMovieMap(): Map[Int, String] = {
    val codec = Codec(StandardCharsets.UTF_8.displayName())
    codec.onMalformedInput(CodingErrorAction.REPLACE)
    codec.onUnmappableCharacter(CodingErrorAction.REPLACE)

    val movieMap = scala.collection.mutable.Map[Int, String]()

    val source = Source.fromFile("data/ml-100k/u.item")(codec)
    val lines = source.getLines()

    for {
      line <- lines
    } {
      val array = line.split("\\|")
      if (array.length > 1)
        movieMap.put(array(0).toInt, array(1))
    }
    source.close()

    movieMap.toMap
  }

  def extractMovie(arg: String): Option[(Int,Int)] = {
    Try {
      val array = arg.split("\t")

      (array(1).toInt, 1)
    } match {
      case Success(value) => Some(value)
      case Failure(exception) => None
    }
  }

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local[*]").setAppName("Most Rated Movies")
    val sc = new SparkContext(conf)
    sc.setLogLevel("ERROR")

    val movieTitles: Broadcast[Map[Int, String]] = sc.broadcast(getMovieMap())

    val moviesRatingRawData = sc.textFile("data/ml-100k/u.data")
    val extractedMovieRatings = moviesRatingRawData.mapPartitions{partition =>
      partition.flatMap{extractMovie}
    }

    val movieRatings: RDD[(Int, String)] = extractedMovieRatings.reduceByKey((x, y) => x+y).mapPartitions{ partition =>
      partition.map{item => (item._2, movieTitles.value(item._1))}
    }

    val finalMoviesRating = movieRatings.sortByKey(ascending = false).collect().foreach(println)

    sc.stop()
  }
}
