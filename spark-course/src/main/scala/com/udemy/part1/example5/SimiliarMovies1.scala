package com.udemy.part1.example5

import java.nio.charset.{CodingErrorAction, StandardCharsets}

import org.apache.spark.{SparkConf, SparkContext}

import scala.io.{Codec, Source}

object SimiliarMovies1 {

  def getMovieNames: Map[Int, String] = {
    val codec: Codec = Codec(StandardCharsets.UTF_8)
    codec.onUnmappableCharacter(CodingErrorAction.REPLACE)
    codec.onMalformedInput(CodingErrorAction.REPLACE)

    val map: scala.collection.mutable.Map[Int, String] = scala.collection.mutable.Map()
    val source: Source = Source.fromFile("../data/ml-100k/u.item")(codec)
    for (line <- source.getLines()) {
      val fields: Array[String] = line.split("\\|")
      map += (fields(0).toInt -> fields(1))
    }

    source.close()
    map.toMap
  }

  type MovieRating = (Int, Double)
  type UserRatings = (Int, (MovieRating, MovieRating))

  def filterUnique(userMovieRating: UserRatings): Boolean = {
    val movie1: MovieRating = userMovieRating._2._1
    val movie2: MovieRating = userMovieRating._2._2

    movie1._1 < movie2._1
  }

  type RatingPair = (Double, Double)
  type MoviePair = ((Int, Int), RatingPair)

  def makePairs(userRatings: UserRatings): MoviePair = {
    val movie1 = userRatings._2._1
    val movie2 = userRatings._2._2

    (movie1._1, movie2._1) -> (movie1._2, movie2._2)
  }

  type MovieRatingsPair = Iterable[RatingPair]

  def calculateSimiliarity(movieRating: MovieRatingsPair): (Double, Int) = {
    var pairs: Int = 0
    var sum_xx: Double = 0.0
    var sum_yy: Double = 0.0
    var sum_xy: Double = 0.0

    for {movie <- movieRating} {
      val pair1: Double = movie._1
      val pair2: Double = movie._2

      sum_xx += pair1 * pair1
      sum_yy += pair2 * pair2
      sum_xy += pair1 * pair2
      pairs += 1
    }

    val numerator: Double = sum_xy
    val denumerator: Double = scala.math.sqrt(sum_xx) * scala.math.sqrt(sum_yy)

    var score: Double = 0.0
    if (denumerator != 0) {
      score = numerator / denumerator
    }

    (score, pairs)
  }

  def main(args: Array[String]): Unit = {
    val config = new SparkConf().setAppName("SimiliarMovies1")
    val sc = new SparkContext(config)
    sc.setLogLevel("ERROR")

    println("Loading movie names....")
    val moviesMap: Map[Int, String] = getMovieNames

    val rawData = sc.textFile("../data/ml-100k/u.data")
    val ratings = rawData.map(_.split("\\s+")).map { item => (item(0).toInt, (item(1).toInt, item(2).toDouble)) }
    val joinedRatings = ratings.join(ratings)
    val filteredJoinedRatings = joinedRatings.filter(filterUnique)
    val pairRatings = filteredJoinedRatings.map(makePairs)
    val groupedRatings = pairRatings.groupByKey()

    val similiarMovies = groupedRatings.mapValues(calculateSimiliarity).cache()

    val scoreThreshold = 0.97
    val coOccurenceThreshold = 50.0

    for {arg <- args} {
      val movieId = arg.toInt

      val filterMovies = similiarMovies.filter { filter =>
        val movies = filter._1
        val similarity = filter._2

        (movies._1 == movieId || movies._2 == movieId) && (similarity._1 > scoreThreshold && similarity._2 > coOccurenceThreshold)
      }

      val proposedMovies = filterMovies.map { item => (item._2, item._1) }.sortByKey(ascending = false).take(10)

      println(s"Top10 similiar movies to ${moviesMap(movieId)}")
      for (movie <- proposedMovies) {
        val rates = movie._1
        val movies = movie._2
        var proposed: Int = movies._1

        if (proposed == movieId) {
          proposed = movies._2
        }

        println(s"${moviesMap(proposed)} - score: ${rates._1}, strength: ${rates._2}")
      }
    }
  }
}
