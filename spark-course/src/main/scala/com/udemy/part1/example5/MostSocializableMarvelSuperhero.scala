package com.udemy.part1.example5

import org.apache.spark.{SparkConf, SparkContext}

import scala.util.{Failure, Success, Try}

object MostSocializableMarvelSuperhero extends App {
  def extractHeroName(input: String): Option[(Int, String)] = {
    Try {
      val array = input.split("\"")
      (array(0).trim.toInt, array(1))
    } match {
      case Success(value) => Some(value)
      case Failure(exception) => None
    }
  }

  def extractHeroGraph(input: String): Option[(Int,Int)] = {
    Try {
       val array = input.split("\\s+")
      (array(0).toInt, array.length - 1)
    } match {
      case Success(value) => Some(value)
      case Failure(exception) => None
    }
  }

  val conf = new SparkConf().setMaster("local[*]").setAppName("Most popular superhero")
  val sc = new SparkContext(conf)
  val heroesNames = sc.textFile("data/SparkScala/Marvel-names.txt").mapPartitions { partition =>
    partition.flatMap(extractHeroName)
  }

  val heroesSocialMapRaw = sc.textFile("data/SparkScala/Marvel-graph.txt")
  val heroesSocialMap = heroesSocialMapRaw.mapPartitions{partition =>
    partition.flatMap(extractHeroGraph)
  }.reduceByKey((x,y)=> x+y).map(item => (item._2, item._1))

  val mostPopularHero = heroesSocialMap.max()
  val mostPopularHeroName = heroesNames.lookup(mostPopularHero._2)

  println(s"Most popular hero is $mostPopularHeroName, and occurred ${mostPopularHero._1}")
  sc.stop()
}
