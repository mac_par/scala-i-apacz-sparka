package com.udemy.part1.example5

import org.apache.spark.sql.SparkSession
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkConf, SparkContext}

import scala.util.{Failure, Success, Try}
object MostRatedMovie {
  def extractData(input:String): Option[(Int,Int)] = {
    Try {
      val array = input.split("\t")
      (array(1).toInt,1)
    } match {
      case Success(value) => Some(value)
      case Failure(exception) => None
    }
  }

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local[*]").setAppName("MostRatedMovie")
    val sc = new SparkContext(conf)
    sc.setLogLevel("ERROR")

    val rawData = sc.textFile("data/ml-100k/u.data")
    val ratedMovies = rawData.mapPartitions{partition =>
      partition.flatMap{extractData}
    }.persist(StorageLevel.MEMORY_ONLY)

    val topRatedMovies1 = ratedMovies.groupByKey().mapValues(_.size).map(item => (item._2,item._1))
    println("topRatedMovies1")
    topRatedMovies1.sortByKey(ascending = false, numPartitions = 1).foreach(println)

    val topRatedMovies2 = ratedMovies.reduceByKey((x,y) => x+y).map(item => (item._2,item._1)).groupByKey
    println("topRatedMovies2")
    topRatedMovies2.sortByKey(ascending = false, numPartitions = 1).foreach(println)

    //sql approach
    val session = SparkSession.builder().config(conf).getOrCreate()
    import session.implicits._
    val sqlMovies = ratedMovies.toDF("movieId","rated")
    sqlMovies.createOrReplaceTempView("movieRatings")
    val topRatedMovies3 = session.sql("select distinct movieId, sum(rated) as times_rated from movieRatings" +
      " group by movieId order by times_rated desc")

    println("topRatedMovies3")
    topRatedMovies3.show(topRatedMovies1.count().toInt)
    sc.stop()
  }
}
