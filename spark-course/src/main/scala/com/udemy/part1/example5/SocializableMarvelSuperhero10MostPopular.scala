package com.udemy.part1.example5

import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkConf, SparkContext}

import scala.util.{Failure, Success, Try}

object SocializableMarvelSuperhero10MostPopular extends App {
  def extractHeroName(input: String): Option[(Int, String)] = {
    Try {
      val array = input.split("\"")
      (array(0).trim.toInt, array(1))
    } match {
      case Success(value) => Some(value)
      case Failure(exception) => None
    }
  }

  def extractHeroGraph(input: String): Option[(Int,Int)] = {
    Try {
       val array = input.split("\\s+")
      (array(0).toInt, array.length - 1)
    } match {
      case Success(value) => Some(value)
      case Failure(exception) => None
    }
  }

  val conf = new SparkConf().setMaster("local[*]").setAppName("Most popular superhero")
  val sc = new SparkContext(conf)
  val heroesNames = sc.textFile("data/SparkScala/Marvel-names.txt").mapPartitions { partition =>
    partition.flatMap(extractHeroName)
  }

  val heroesSocialMapRaw = sc.textFile("data/SparkScala/Marvel-graph.txt")
  val heroesSocialMap = heroesSocialMapRaw.mapPartitions{partition =>
    partition.flatMap(extractHeroGraph)
  }.reduceByKey((x,y)=> x+y).map(item => (item._2, item._1)).persist(StorageLevel.MEMORY_ONLY)

  val mostPopularHeroes = heroesSocialMap.sortByKey(ascending = false).take(10)
  val top10 = mostPopularHeroes.take(10).map{record => (heroesNames.lookup(record._2), record._1)}

  val last10 = heroesSocialMap.sortByKey().take(10).map{record => (heroesNames.lookup(record._2), record._1)}

  println("Top10:")
  top10.foreach(println)

  println("\nDown10")
  last10.foreach(println)
  sc.stop()
}
