package com.udemy.part1.jakis.kurs.utils.tasks.t01

import org.apache.spark.{SparkConf, SparkContext}

object CountWordsSC extends App {
  val sc = new SparkContext(new SparkConf().setMaster("local[*]").setAppName("wordCount1"))
  sc.setLogLevel("WARN")
  val inputRdd = sc.textFile("bin/20_newsgroups/alt.atheism", minPartitions = 3)
  val processedRdd = inputRdd.mapPartitions{partition =>
    partition.flatMap(item => item.split("\\s+")).filterNot(_.isBlank).map(str => (str.toLowerCase,1))
  }

  val collectedRdd = processedRdd.reduceByKey(_+_).sortBy(item=> item._2, ascending = false, numPartitions = 3).cache()

  collectedRdd.foreach(println)
//  collectedRdd.mapPartitionsWithIndex{(idx, iter)=>iter.toList.map(item => f"${item} at part: ${idx}").iterator }
//    .foreach(println)

  sc.stop()
}
