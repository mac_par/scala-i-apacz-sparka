package com.udemy.part1.jakis.kurs.utils.tasks.t02

case class Bank(age: Int, job: String, martial: String, education: String)

object Bank {
  def build(string: String): Bank = {
    val list = string.replaceAll("\"", "").split(";")
    Bank(list(0).toInt, list(1), list(2), list(3))
  }
}