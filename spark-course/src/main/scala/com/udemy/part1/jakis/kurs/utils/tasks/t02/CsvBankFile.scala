package com.udemy.part1.jakis.kurs.utils.tasks.t02

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object CsvBankFile extends App {
  val sc = new SparkContext(new SparkConf().setMaster("local[*]").setAppName("CvsFile"))
  sc.setLogLevel("ERROR")
  val inputRdd = sc.textFile("bin/bank-additional/bank-additional-full.csv").repartition(3).cache()
  val header = inputRdd.first()
  val headerBc = sc.broadcast(header)
  val listedRdd = inputRdd.filter(item => item != headerBc.value).map(item => Bank.build(item))


  listedRdd.saveAsTextFile("bin/tmp/trash/bank.txt")
  sc.stop()
}
