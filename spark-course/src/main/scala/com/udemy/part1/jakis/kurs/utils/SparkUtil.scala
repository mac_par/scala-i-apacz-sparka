package com.udemy.part1.jakis.kurs.utils

import org.apache.spark.{SparkConf, SparkContext}

object SparkUtil {
  def sparkContext(name: String): SparkContext = {
    val sc = new SparkContext(configure(name))
    sc.setLogLevel("WARN")
    sc
  }

  def configure(name: String): SparkConf = {
    new SparkConf().setAppName(name).setMaster("local[*]")
      .set("spark.io.compression.codec", "lzf")
      .set("spak.speculation", "true")
  }

  val BIN_PATH = "bin"

}
