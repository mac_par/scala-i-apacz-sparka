package com.udemy.part1.jakis.kurs.utils.tasks.t02

import org.apache.spark.sql.{DataFrame, SparkSession}

object CsvBankSession extends App {
  val session = SparkSession.builder().master("local[*]").appName("BankCsvApp").getOrCreate()

  import session.implicits._
  val inputDF = session.read
    .format("csv")
    .option("delimiter",";").option("inferSchema", "true")
    .option("header", value = true)
    .load("bin/bank-additional/bank-additional-full.csv")


  inputDF.createTempView("bankTable")

  val result = session.sqlContext.sql("select age, education, job from bankTable")
  result.write.json("bin/tmp/dupa.txt")
  result.show()
}
