package com.udemy.part1

import scala.util.{Success, Try, Failure}
import org.apache.spark.{SparkContext, SparkConf}

object AverageFriendsByFirstName {
  def friendsPerName(arg: String): Option[(String, Int)] = {
    Try {
      val array = arg.split(",")
      val firstName = array(1)
      val friendsCount = array(3).toInt
      firstName -> friendsCount
    } match {
      case Success(value) => Some(value)
      case Failure(exception) => None
    }
  }

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local[*]").setAppName("Average friends amount by first name")
    val sc = new SparkContext(conf)
    sc.setLogLevel("ERROR")

    val input = sc.textFile("data/SparkScala/fakefriends.csv")

    val convertedRdd = input.mapPartitions{ partition =>
      partition.flatMap{friendsPerName}
    }

    val initialMap = convertedRdd.mapValues{value => (value, 1)}.reduceByKey{(x,y)=> (x._1 + y._1, x._2 + y._2)}

    val result = initialMap.mapValues{value => value._1 / value._2.toDouble}

    result.sortByKey().foreach{println}
    sc.stop()
  }
}
