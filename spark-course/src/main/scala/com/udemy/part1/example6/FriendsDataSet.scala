package com.udemy.part1.example6

import org.apache.spark.sql.SparkSession

object FriendsDataSet {
  case class Person(ID: Int, name:String, age:Int, numFriends: Int)

  def extractFriends(line: String): Person = {
    val fields = line.split(",")

    Person(fields(0).toInt, fields(1), fields(2).toInt, fields(3).toInt)
  }
  def main(args:Array[String]): Unit = {
    val session = SparkSession.builder()
      .master("local[*]")
      .appName("Friends Data Set")
      .getOrCreate()
    session.sparkContext.setLogLevel("INFO")

    val persons = session.sparkContext.textFile("data/SparkScala/fakefriends.csv").map(extractFriends)

    import session.implicits._
    val personsSchema = persons.toDS()
    personsSchema.printSchema()

    personsSchema.createOrReplaceTempView("people")

    val teenagers = session.sql("select * from people where age >= 13 and age <=19 order by name")

    val results = teenagers.collect()

    results.foreach(println)

    session.stop()
  }
}
