package com.udemy.part1.example6

import java.nio.charset.{CodingErrorAction, StandardCharsets}
import org.apache.spark.sql.functions.desc
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.SparkSession

import scala.io.{Codec, Source}

object MostPopularMovies {
  def getMovieTitles: Map[Int,String] = {
    val codec: Codec = new Codec(StandardCharsets.UTF_8)
    codec.onMalformedInput(CodingErrorAction.REPLACE)
    codec.onUnmappableCharacter(CodingErrorAction.REPLACE)

    val map = scala.collection.mutable.Map[Int,String]()
    val source:Source = Source.fromFile("data/ml-100k/u.item")(codec)

    for {line <- source.getLines()} {
      val fields = line.split("\\|")
      map += (fields(0).toInt -> fields(1))
    }

    source.close()
    map.toMap
  }
  case class Movie(id:Int,rating:Int)

  def main(args: Array[String]): Unit = {
    val movieNames = getMovieTitles
    val config = new SparkConf().setMaster("local[*]").setAppName("Most watched")
    val session = SparkSession.builder().config(config).getOrCreate()
    session.sparkContext.setLogLevel("ERROR")
    import session.implicits._
    val movies = session.sparkContext.textFile("data/ml-100k/u.data").map(_.split("\\s+")).map(item=> Movie(item(1).toInt, item(2).toInt)).toDS()
    movies.createOrReplaceTempView("movies")

    val moviesSet = movies.groupBy("id").count().orderBy(desc("count")).cache()

    moviesSet.show()

    val top10 = moviesSet.take(10)

    for {movie <- top10} {
      println(s"${movieNames(movie(0).asInstanceOf[Int])} - ${movie(1)}")
    }
    session.stop()
  }
}
