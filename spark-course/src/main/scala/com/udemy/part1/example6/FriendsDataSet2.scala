package com.udemy.part1.example6

import org.apache.spark.sql.SparkSession

object FriendsDataSet2 {
  case class Person(ID:Int, name:String, age:Int, numFriends: Int)

  def getPerson(str:String): Person = {
    val fields = str.split(",")
    Person(fields(0).toInt, fields(1), fields(2).toInt, fields(3).toInt)
  }

  def main(args: Array[String]): Unit = {
    val session = SparkSession.builder()
      .master("local[*]")
      .appName("FriendsDataSet2")
      .getOrCreate()

    session.sparkContext.setLogLevel("ERROR")
    val persons = session.sparkContext.textFile("data/SparkScala/fakefriends.csv")
    import session.implicits._

    val personsSchema = persons.map(getPerson).toDS().cache()

    println("print persons schema")
    personsSchema.createOrReplaceTempView("persons")
    personsSchema.printSchema()

    //display all names
    personsSchema.select("name").show()

    //display rows with age < 21
    println("People younger than 21")
    personsSchema.filter(personsSchema("age") < 21).show()

    //count all ages
    println("Group by age")
    personsSchema.groupBy("age").count().show()

    //display result: name and age + 10
    println("display custom result")
    personsSchema.select(personsSchema("name"), personsSchema("age").alias("real_age"),personsSchema("age") + 10).show

    session.stop
  }
}
