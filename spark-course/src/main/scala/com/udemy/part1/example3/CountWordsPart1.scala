package com.udemy.part1.example3

import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}

object CountWordsPart1 {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local[*]").setAppName("CountWords1")

    val sc = new SparkContext(conf)
    sc.setLogLevel("Error")
    Logger.getLogger("org").setLevel(Level.ERROR)

    val bookLines = sc.textFile("data/SparkScala/book.txt")

    val bookWords = bookLines.mapPartitions{partition =>
      partition.flatMap{line => line.split("\\W+")}
        .map{word => (word.toLowerCase(),1)}
    }

    val bookGroupedWords = bookWords.reduceByKey((x,y) => x+y).sortByKey()

    bookGroupedWords.foreach{println}

    sc.stop()
  }
}
