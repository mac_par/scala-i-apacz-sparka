package com.udemy.part1.example3

import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}

object CountWordsPart4 {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local[*]").setAppName("CountWords1")

    val sc = new SparkContext(conf)
    sc.setLogLevel("Error")
    Logger.getLogger("org").setLevel(Level.ERROR)

    val bookLines = sc.textFile("data/SparkScala/book.txt")
    val discardWords = sc.textFile("bin/english_words.txt")
    val filteredBookWords = bookLines.mapPartitions{partition =>
      partition.flatMap{line => line.split("\\W+")}
        .map{word => word.toLowerCase()}

    }.subtract(discardWords)
    val bookWords = filteredBookWords.mapPartitions{partition =>
      partition.map{item => (item, 1)}
    }

    val bookGroupedWords = bookWords.reduceByKey((x,y) => x+y).mapPartitions{partition =>
      partition.map(record => (record._2, record._1))
    }.groupByKey(3).mapValues(values => values.toList.sorted)

    bookGroupedWords.sortByKey(numPartitions = 3, ascending = true).foreach{println}

    sc.stop()
  }
}
