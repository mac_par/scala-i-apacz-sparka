package com.udemy.part1

import scala.annotation.tailrec
import scala.util.matching.Regex

object ScalaTest extends App {
  val string: String = "To life, the universe and everything is 42."
  val regex: Regex = """.* ([\d]+).*""".r

  val regex(answer) = string
  val number: Int = answer.toInt
  println(number)

  val str1 = "Picard"
  val str2 = "Picard"
  println(str1 == str2)

  val pi = 3.141592653589793238462643383279502884197169399375105820974944592307816406286

  val dblPi = pi * 2
  println(f"2 x pi = $dblPi%.3f")

  for {
    x <- 0 until 10
  } {
    println(fibonacci(x))
  }

  def fibonacci(number: Int): Int = {

    @tailrec
    def fibonacciCount(number: Int, prev: Int, current: Int): Int = {
      if (number <= 0) {
        return current
      }
      fibonacciCount(number-1, prev = prev + current, current = prev)
    }

    fibonacciCount(number, 1, 0)
  }
}
