package com.data.analysis.chpt01

import com.data.analysis.source.CsvDataSrc
import org.apache.spark.sql.SparkSession

object ReadCsv03 extends App {
  val spark = SparkSession.builder().appName("CsvReader03").master("local[*]").getOrCreate()
  spark.sparkContext.setLogLevel("ERROR")
  import spark.implicits._

  val csvInput = spark.read.format("csv").option("delimiter", ",").option("inferSchema", true).option("header", true)
    .load(CsvDataSrc.getSource).repartition(3)

  val price = 200000
  val filteredCSV = csvInput.filter($"SalePrice" > price)

  val result = filteredCSV.select($"Id", $"SalePrice").cache()

  result.printSchema()

  result.show(truncate = false)
}
