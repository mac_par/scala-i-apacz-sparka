package com.data.analysis.chpt01

import com.data.analysis.source.CsvDataSrc
import org.apache.spark.sql.SparkSession

object ReadCsv01 {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().appName("read-csv1").master("local[*]").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    val csvInput = spark.read.csv(CsvDataSrc.getSource)

    csvInput.printSchema()
    csvInput.show(truncate = false)
  }
}
