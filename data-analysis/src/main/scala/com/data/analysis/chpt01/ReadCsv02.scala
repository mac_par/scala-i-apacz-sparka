package com.data.analysis.chpt01

import com.data.analysis.source.CsvDataSrc
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

object ReadCsv02 {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().master("local[*]").appName("csvReader2").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    val csvInput: DataFrame = spark.read.format("csv").option("delimiter", ",").option("header", true).option("inferSchema", true)
      .load(CsvDataSrc.getSource).repartition(4)

    csvInput.printSchema()
    csvInput.show(truncate = false)

    csvInput.write.format("json").mode(SaveMode.Append).save("bin/tmp/csvproby02.json")
  }
}
