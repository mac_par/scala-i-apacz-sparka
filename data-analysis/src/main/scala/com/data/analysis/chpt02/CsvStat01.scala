package com.data.analysis.chpt02

import com.data.analysis.source.CsvDataSrc
import org.apache.spark.sql.functions.avg
import org.apache.spark.sql.SparkSession

object CsvStat01 {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().master("local[*]").appName("Stat01").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    import spark.implicits._

    val srcData = spark.read.format("csv")
      .option("header", true)
      .option("inferSchema", true)
      .option("delimiter", ",")
      .load(CsvDataSrc.getSource)

    val result = srcData.groupBy($"YearBuilt").agg(avg($"SalePrice")).withColumnRenamed("YearBuilt", "Year").sort($"Year".desc)

    result.show(40, truncate = false)
  }
}
