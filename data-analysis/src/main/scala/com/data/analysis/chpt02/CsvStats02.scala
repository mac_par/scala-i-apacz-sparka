package com.data.analysis.chpt02

import com.data.analysis.source.CsvDataSrc
import org.apache.spark.sql.functions.sum
import org.apache.spark.sql.SparkSession

object CsvStats02 {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().master("local[*]").appName("Most promising neighbourhoods").getOrCreate()
    import spark.implicits._
    spark.sparkContext.setLogLevel("ERROR")

    val srcData = spark.read.format("csv").option("header", true).option("inferSchema", true).option("delimiter", ",").load(CsvDataSrc.getSource)

    val groupByNeighbourhood = srcData.groupBy($"YearBuilt".as("Year"), $"Neighborhood").agg(sum($"SalePrice").as("Total_Price"))
//      .withColumnRenamed("YearBuilt", "Year")
      .sort($"Year".desc)

    groupByNeighbourhood.show(40, truncate = false)
  }
}
