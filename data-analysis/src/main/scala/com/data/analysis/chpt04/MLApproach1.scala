package com.data.analysis.chpt04

import com.data.analysis.source.CsvDataSrc
import org.apache.spark.ml.linalg.Vectors
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{IntegerType, StructField}

object MLApproach1 {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().appName("ML1").master("local[*]").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    import spark.implicits._

    val csv = spark.read.format("csv")
      .option("header", value = true)
      .option("delimiter", ",")
      .option("inferSchema", value = true)
      .load(CsvDataSrc.getSource).cache()

    val idField = "Id"
    val fieldName = "SalePrice"
    val fields = csv.schema.fields.collect{
      case StructField(name, IntegerType, _, _) if name != idField && name != fieldName => name
    }

    //prepare scalar data - features(all values)/ labels - to be predicted
    val scalarData = csv.map{ row =>
      //features
      (
        Vectors.dense(fields.map{field => row.getAs[Int](field).toDouble}),
        row.getInt(row.fieldIndex(fieldName))
        )
    }.toDF("features", "labels")

    scalarData.show(truncate = false)
  }
}
