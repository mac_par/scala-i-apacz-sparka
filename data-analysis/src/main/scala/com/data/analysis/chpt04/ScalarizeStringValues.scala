package com.data.analysis.chpt04

import com.data.analysis.source.CsvDataSrc
import org.apache.spark.ml.feature.StringIndexer
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{StringType, StructField, StructType}

object ScalarizeStringValues {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().master("local[*]").appName("ScalarString").getOrCreate()
    import spark.implicits._
    spark.sparkContext.setLogLevel("ERROR")
    val data = spark.read.format("csv")
      .option("header", value = true)
      .option("delimiter", ",")
      .option("inferSchema", value = true)
      .load(CsvDataSrc.getSource).cache()

    val stringFields = data.schema.fields.collect{
      case StructField(name, StringType, _, _) => name
    }

    val scalled = stringFields.foldLeft(data){ (prev, dane) =>
      val indexer = new StringIndexer()
        .setInputCol(dane)
        .setOutputCol(s"${dane}_id")
      indexer.fit(prev).transform(prev)
    }

    scalled.show(truncate = false)
  }
}
