package com.data.analysis.chpt04

import com.data.analysis.source.CsvDataSrc
import org.apache.spark.ml.feature.{StandardScaler, StringIndexer}
import org.apache.spark.ml.linalg.{Vectors, DenseVector}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{IntegerType, StringType, StructField}

object AnalyseWithScalarsAndCategories {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().master("local[*]").appName("ML data").getOrCreate()
    import spark.implicits._
    spark.sparkContext.setLogLevel("ERROR")
    val csvData = spark.read.format("csv")
      .option("header", value = true)
      .option("delimiter", ",")
      .option("inferSchema", value = true)
      .load(CsvDataSrc.getSource).cache()

    //extract scalar values
    val fieldName = "SalePrice"
    val idField = "Id"
    val scalarFields = csvData.schema.fields.collect{
      case StructField(name, IntegerType, _, _) if name != fieldName && name != idField => name
    }

    val stringFields = csvData.schema.fields.collect{
      case StructField(name, StringType, _,_) => name
    }

    //convert string values into scalar
    val categoricalDF = stringFields.foldLeft(csvData){(prevDf, value)=>
      val indexer = new StringIndexer()
        .setInputCol(value)
        .setOutputCol(s"${value}_col")
      indexer.fit(prevDf).transform(prevDf)
    }

    val data = categoricalDF.map{row=>
      (
        Vectors.dense(scalarFields.map(field => row.getAs[Int](field).toDouble)),
        Vectors.dense(stringFields.map(field => row.getAs[Double](s"${field}_col"))),
        row.getAs[Int](fieldName)
      )
    }.toDF("features_scalar", "features_categorical", "label")

    //standardising scalar features
    val scalarStandard = new StandardScaler()
      .setInputCol("features_scalar")
      .setOutputCol("features_scalar_scaled")
      .setWithStd(true)
      .setWithMean(true);

    val scaled = scalarStandard.fit(data).transform(data)

    //combine
    val resul = scaled.map{row =>
      val featuresVector = Vectors.dense(row.getAs[DenseVector]("features_scalar_scaled").values ++
          row.getAs[DenseVector]("features_categorical").values
        )
      (featuresVector, row.getAs[Int]("label"))
    }.toDF("features", "label")

    resul.show()
  }
}
