package com.data.analysis.chpt04

import com.data.analysis.source.CsvDataSrc
import org.apache.spark.ml.feature.VectorSlicer
import org.apache.spark.ml.linalg.Vectors
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{IntegerType, StructField}

object SlicingThem extends App {
  val spark = SparkSession.builder().master("local[*]").appName("cos tam").getOrCreate()
  spark.sparkContext.setLogLevel("ERROR")
  import spark.implicits._
  val initDF = spark.read.format("csv").option("header", value = true)
    .option("inferSchema", value = true)
    .option("delimiter", ",")
    .load(CsvDataSrc.getSource)

  val saleField = "SalePrice"
  val scalarFields = initDF.schema.fields.collect{case StructField(name, IntegerType, _,_) if saleField != name => name}

  val scaledDF = initDF.map{row =>
    (
      Vectors.dense(scalarFields.map{field => row.getAs[Int](field).toDouble}),
      row.getAs[Int](saleField)
    )
  }.toDF("scalar_features", "labels").cache()

  val vectorSlicer = new VectorSlicer()
    .setInputCol("scalar_features")
    .setOutputCol("features")
    .setIndices((1 until scalarFields.size).toArray)

  val extractedDF = vectorSlicer.transform(scaledDF).drop($"scalar_features")

  extractedDF.show(truncate = false)
}
