package com.data.analysis.source

object CsvDataSrc {
  lazy val getSource: String = "bin/house_prices/train.csv"
}
