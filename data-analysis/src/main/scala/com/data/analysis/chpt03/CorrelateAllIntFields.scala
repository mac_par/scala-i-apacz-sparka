package com.data.analysis.chpt03

import com.data.analysis.source.CsvDataSrc
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.corr
import org.apache.spark.sql.types.{IntegerType, StructField}

object CorrelateAllIntFields {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().master("local[*]").appName("Int Corellation").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")

    val input = spark.read.format("csv")
      .option("header", value = true)
      .option("inferSchema", value = true)
      .option("delimiter", ",")
      .load(CsvDataSrc.getSource).cache()

    val fields = input.schema.fields.collect { case StructField(fieldName, IntegerType, _, _) => fieldName }.filter(_ != "SalePrice")

    val correlations = input.select(fields.map(corr(_, "SalePrice")): _*)
    correlations.show(truncate = false)
  }
}
