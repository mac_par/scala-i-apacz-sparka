package com.data.analysis.chpt03

import com.data.analysis.source.CsvDataSrc
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.sum

object PricesForTheGermans {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().master("local[*]").appName("Converter").getOrCreate()
    import spark.implicits._
    import org.apache.spark.sql.functions.udf

    spark.sparkContext.setLogLevel("ERROR")
    val initialDF = spark.read.format("csv").option("header", value = true).option("delimiter", ",").option("inferSchema", value = true)
      .load(CsvDataSrc.getSource)

    val exchangeRate = 0.92
    val func = (value: Int) => value * exchangeRate
    val convertToEUR = udf(func)

    val result = initialDF.groupBy($"YearBuilt".as("Year"), $"Neighborhood").agg(sum($"SalePrice").as("Price[USD]"))
        .sort($"Year".desc, $"Neighborhood".asc).select($"Year", $"Neighborhood", $"Price[USD]", convertToEUR($"Price[USD]").as("Price[EUR]"))

    result.show(40, truncate = false)
  }
}
