package com.data.analysis.chpt03

import com.data.analysis.source.CsvDataSrc
import org.apache.spark.sql.SparkSession

object CsvUdf02 {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().master("local[*]").appName("Udf - approach 2").getOrCreate()
    import spark.implicits._
    import org.apache.spark.sql.functions._

    val csvInput = spark.read.format("csv").option("header", value = true).option("inferSchema", value = true).option("delimiter", ",")
      .load(CsvDataSrc.getSource)

    val exchangeRate = .92
    val func = (value: Int) => value * exchangeRate
    val converter = udf(func)

    //randomize
    val result = csvInput.groupBy($"YearBuilt".as("Year"), $"Neighborhood").agg(sum($"SalePrice") as "Price")
      .sort($"Year" desc, $"Neighborhood" asc)
      .withColumn("random", rand(42) > 0.5)
      .filter("random")
      .select($"Year", $"Neighborhood", $"Price" as "Price[USD]", converter($"Price").as("Price[EUR]"))

    result.show(20, truncate = false)
  }
}
