package com.data.analysis.chpt03


import com.data.analysis.source.CsvDataSrc
import org.apache.spark.sql.SparkSession

object VariousFunc {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().appName("various funcs").master("local[*]").getOrCreate()
    import spark.implicits._
    spark.sparkContext.setLogLevel("ERROR")

    val input = spark.read.format("csv").option("header", value = true).option("inferSchema", value = true).option("delimiter", ",")
      .load(CsvDataSrc.getSource).cache()
    import org.apache.spark.sql.functions.{mean, stddev, variance, corr => kora}

    val result = input.select(variance($"SalePrice"), mean($"SalePrice"), stddev($"SalePrice"))

    result.show(truncate = false)
    input.describe("SalePrice", "LotArea").show()

    input.select(kora("OverallQual", "SalePrice")).show(truncate = false)
  }
}
