package com.data.analysis.chpt05

import com.data.analysis.source.CsvDataSrc
import org.apache.spark.ml.Transformer
import org.apache.spark.ml.evaluation.RegressionEvaluator
import org.apache.spark.ml.feature.{StandardScaler, StringIndexer}
import org.apache.spark.ml.linalg.{DenseVector, Vectors}
import org.apache.spark.ml.regression.DecisionTreeRegressor
import org.apache.spark.sql.types.{IntegerType, StringType, StructField}
import org.apache.spark.sql.{DataFrame, SparkSession}

object MLTaste2 {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().master("local[*]").appName("MLTaste2").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    import spark.implicits._

    val initDataDF = spark.read.format("csv")
      .option("header", value = true)
      .option("delimiter", ",")
      .option("inferSchema", value = true)
      .load(CsvDataSrc.getSource).cache()

    val idField = "Id"
    val saleField = "SalePrice"
    val scalarFields = initDataDF.schema.fields.collect {
      case StructField(name, IntegerType, _, _) if name != idField && name != saleField => name
    }

    val stringFields = initDataDF.schema.fields.collect {
      case StructField(name, StringType, _, _) => name
    }

    val dataWithCategoricalFeaturesDF = stringFields.foldLeft(initDataDF) { (prevDF, field) =>
      val stringIndexer = new StringIndexer()
        .setInputCol(field)
        .setOutputCol(s"${field}_col")

      stringIndexer.fit(prevDF).transform(prevDF)
    }.drop(stringFields.toArray: _*)

    val gatheredFeaturesDF = dataWithCategoricalFeaturesDF.map { row =>
      (
        Vectors.dense(scalarFields.map(field => row.getAs[Int](field).toDouble)),
        Vectors.dense(stringFields.map(field => row.getAs[Double](s"${field}_col"))),
        row.getAs[Int](saleField)
      )
    }.toDF("scalar_features", "categorical_features", "label")

    //normalize scalars
    val standardScaler = new StandardScaler()
      .setInputCol("scalar_features")
      .setOutputCol("scalar_features_scaled")
      .setWithMean(true)
      .setWithStd(true)

    val standardizedDF = standardScaler.fit(gatheredFeaturesDF).transform(gatheredFeaturesDF)
      .drop($"scalar_features")

    val combinedDataDF = standardizedDF.map { row =>
      (
        Vectors.dense(
          row.getAs[DenseVector]("scalar_features_scaled").values
            ++
            row.getAs[DenseVector]("categorical_features").values
        ),
        row.getAs[Int]("label")
      )
    }.toDF("features", "label")

    val Array(trainData, testData) = combinedDataDF.randomSplit(Array(0.7, 0.3))
    val decisionTree = new DecisionTreeRegressor()
      .setLabelCol("label")
      .setFeaturesCol("features")
      .setMaxBins(1000)
      .setMaxDepth(10)

    val model = decisionTree.fit(trainData)
    val (trainResultDF, trainEval) = eval(trainData, model)
    val (testResultDF, testEval) = eval(testData, model)

    println(s"train evaluation: $trainEval")
    println(s"test evaluation: $testEval")

    testResultDF.show(10)
  }

  def eval(df: DataFrame, model: Transformer): (DataFrame, Double) = {
    val evaluator = new RegressionEvaluator()
      .setLabelCol("label")
      .setPredictionCol("prediction")
      .setMetricName("r2")

    val prediction = model.transform(df)
    val evaluation = evaluator.evaluate(prediction)

    (prediction, evaluation)
  }
}
