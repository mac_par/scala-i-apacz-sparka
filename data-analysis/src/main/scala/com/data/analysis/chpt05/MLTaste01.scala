package com.data.analysis.chpt05

import com.data.analysis.source.CsvDataSrc
import org.apache.spark.ml.Transformer
import org.apache.spark.ml.evaluation.RegressionEvaluator
import org.apache.spark.ml.feature.{StandardScaler, StringIndexer}
import org.apache.spark.ml.linalg.{DenseVector, Vectors}
import org.apache.spark.ml.regression.LinearRegression
import org.apache.spark.sql.types.{IntegerType, StringType, StructField}
import org.apache.spark.sql.{DataFrame, SparkSession}

object MLTaste01 {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().master("local[*]").appName("MLTaste1").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    import spark.implicits._
    val dataDF = spark.read.format("csv")
      .option("header", value = true)
      .option("inferSchema", value = true)
      .option("delimiter", ",")
      .load(CsvDataSrc.getSource).cache()

    val idField = "Id"
    val saleField = "SalePrice"
    val scalarfields = dataDF.schema.fields.collect{
      case StructField(name, IntegerType, _, _) if name != idField && name != saleField => name
    }

    val stringFields = dataDF.schema.fields.collect {
      case StructField(name, StringType, _,_) => name
    }

    val dataDFWithCategories = stringFields.foldLeft(dataDF){(prevDF, field) =>
      val stringIndexer = new StringIndexer()
        .setInputCol(field)
        .setOutputCol(s"${field}_col")

      stringIndexer.fit(prevDF).transform(prevDF)
    }

    //scalar_features, categorical_features, label
    val gatheredDF = dataDFWithCategories.map{row => (
        Vectors.dense(scalarfields.map{field => row.getAs[Int](field).toDouble}),
        Vectors.dense(stringFields.map(field => row.getAs[Double](s"${field}_col"))),
          row.getAs[Int](saleField)
      )
    }.toDF("features_scalar","features_categorical", "label")

    //normalize
    val standardizer = new StandardScaler()
      .setInputCol("features_scalar")
      .setOutputCol("features_scalar_scaled")
      .setWithMean(true)
      .setWithStd(true)

    val scaledDF = standardizer.fit(gatheredDF).transform(gatheredDF).drop($"features_scalar")

    val combinedData = scaledDF.map{row => (
      Vectors.dense(row.getAs[DenseVector]("features_scalar_scaled").values
          ++
        row.getAs[DenseVector]("features_categorical").values),
        row.getAs[Int]("label")
    )}.toDF("features", "label")

    val Array(trainData, testData) = combinedData.randomSplit(Array(0.7,.3))

    val lrInstance = new LinearRegression()
      .setMaxIter(1000)
      .setRegParam(0.3)
    val model = lrInstance.fit(trainData)

    val (trainPreditionDF, trainR2Eval) = predict(trainData, model)
    val (testPreditionDF, testR2Eval) = predict(testData, model)

    println(s"Result - train: $trainR2Eval")
    println(s"Result - test: $testR2Eval")
    testPreditionDF.select($"prediction", $"label").show(10)
  }

  def predict(df: DataFrame, model: Transformer): (DataFrame, Double) = {
      val regressionEvaluator = new RegressionEvaluator()
        .setLabelCol("label")
        .setPredictionCol("prediction")
        .setMetricName("r2")
    val predictions = model.transform(df)
    val evaluation = regressionEvaluator.evaluate(predictions)
    (predictions, evaluation)
  }
}
