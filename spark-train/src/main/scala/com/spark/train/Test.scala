package com.spark.train

import org.apache.spark.sql.{Column, DataFrame, SparkSession}
import org.apache.spark.sql.functions.{col, explode}
import org.apache.spark.sql.types.{ArrayType, DataType, ObjectType, StructField, StructType}

object Test extends App {

  def flattenDataType(dataType: DataType, prefix: String = null): Array[Column] = {
    dataType match {
      case ArrayType(elementType, containsNull) => flattenDataType(elementType, prefix);
      case StructType(fields) => flattenStruct(fields, prefix)
      case _ => Array(explode(col(prefix)))
    }
  }

  def flattenStruct(schema: Array[StructField], prefix: String = null): Array[Column] = {
    schema.flatMap { field =>
      val columnName = if (prefix == null) field.name else s"${prefix}.${field.name}"
      field.dataType match {
        case StructType(fields) => flattenStruct(fields,columnName)
        case ArrayType(elementType, containsNull) => flattenDataType(elementType, columnName)
        /* elementType match {
          case element: StructType =>
            flattenStruct(element.fields, prefix)
        }*/

//          Array(col(s"${columnName}.*").as(columnName))
//                  Array(explode(col(columnName)).as(columnName))
//        case structField => Array(s"${columnName}.${field.name}")
//        case _ => Array(col(columnName).as(columnName))
        case _ => Array.empty[Column]//Array(col(columnName))//(col(columnName))
      }
    }
    /*  schema.fields.flatMap(field => {
        val columnHeader = if (prefix == null) field.name else s"${prefix}.${field.name}"
        val fieldType: DataType = field.dataType
        field.dataType match {
          case StructType(fields) => {
            fields.foreach(println)
            Array.empty[Column]}
  //        case ArrayType(elementType, containsNull) =>Array.empty
          case _ => Array(col(columnHeader))
        }
      })*/
  }

  def flattenDF(dataFrame: DataFrame): DataFrame = {
      val columns = flattenStruct(dataFrame.schema.fields).distinct
    columns.foreach(println)
      flattenDF(dataFrame.select(columns:_*))
  }

  /*def flattenArrayStruct(dataType: DataType, prefix: String): Array[String] = {
    dataType.
  }*/

  val session = SparkSession.builder().appName("test1").master("local[*]").getOrCreate()

  import session.implicits._

  val inputDf = session.read.json("bin/test.json").toDF()
  inputDf.printSchema()
  inputDf.show()
  val result: DataFrame = flattenDF(inputDf)
  result.show(false)
  result.printSchema()
//
//  resultDf.write.format("csv").option("delimiter", ";").save("bin/cos.csv")

}
