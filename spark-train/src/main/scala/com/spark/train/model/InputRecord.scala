package com.spark.train.model

case class InputRecord(uuid: String = java.util.UUID.randomUUID().toString, userId: String)
