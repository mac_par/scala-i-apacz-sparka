package com.spark.train.model

case class UserTransaction(userId: String, amount: Int)
