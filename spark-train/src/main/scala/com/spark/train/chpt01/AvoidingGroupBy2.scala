package com.spark.train.chpt01

import com.spark.train.model.UserTransaction
import org.apache.spark.SparkContext

object AvoidingGroupBy2 {
  val sc = new SparkContext("local[*]", "cos tam2")
  sc.setLogLevel("ERROR")

  val input = sc.parallelize(Seq(UserTransaction("A", 125), UserTransaction("A", 25), UserTransaction("A", 12), UserTransaction("B", 10)))

  val groupedRdd = input.mapPartitions{part =>
    part.map(item => (item.userId, item))
  }

//  val reducedRdd = groupedRdd.red

  groupedRdd.foreach(println)

  sc.stop()
}
