package com.spark.train.chpt01

import com.spark.train.model.InputRecord
import org.apache.spark.SparkContext

object DeferencedCalculation {
  def main(args: Array[String]): Unit = {
    val sc = new SparkContext("local[*]", "cos tam")

    val input = sc.parallelize(Seq(InputRecord(userId = "A"), InputRecord(userId = "B"), InputRecord(userId = "C")))

    val filtered = input.filter(_.userId =="A").keyBy(_.userId).map(_._2.userId.toLowerCase)

    filtered.collect().foreach(println)
    sc.stop()
  }
}
