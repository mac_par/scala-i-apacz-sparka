package com.spark.train.chpt01

import com.spark.train.model.UserTransaction
import org.apache.spark.SparkContext

object AvoidingGroupBy {
  def main(args: Array[String]): Unit = {
    val sc = new SparkContext("local[*]", "cos tam2")
    sc.setLogLevel("ERROR")

    val input = sc.parallelize(Seq(UserTransaction("A", 125), UserTransaction("A", 25), UserTransaction("A", 12), UserTransaction("B", 10)))

    val groupedRdd = input.groupBy(_.userId).map{item => (item._1, item._2.toList)}.collect().toList

    groupedRdd.foreach(println)

    sc.stop()
  }
}
