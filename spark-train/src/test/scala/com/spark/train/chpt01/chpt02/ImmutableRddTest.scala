package com.spark.train.chpt01.chpt02

import com.spark.train.model.UserData
import org.apache.spark.sql.SparkSession
import org.scalatest.GivenWhenThen
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.must.Matchers

class ImmutableRddTest extends AnyFunSuite with GivenWhenThen with Matchers {
  test("immutability of Data Frames") {
    Given("spark session and rdd")
    val spark = SparkSession.builder().master("local[*]").appName("cos").getOrCreate()
    import spark.implicits._

    val testRdd = spark.sparkContext.parallelize(List(UserData("a", "cos tam"), UserData("b", "cos tam2"),
      UserData("c", "cos tam3"), UserData("d", "cos tam4"))).toDF()

    val filtered = testRdd.filter($"userId".isin("a")).cache()

    assert(filtered.collect().size == 1)
    assert(testRdd.collect().size == 4)
    spark.stop()
  }
}
