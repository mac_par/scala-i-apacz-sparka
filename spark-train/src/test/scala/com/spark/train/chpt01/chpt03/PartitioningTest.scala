package com.spark.train.chpt01.chpt03

import com.spark.train.model.{UserData, UserTransaction}
import org.apache.spark.sql.SparkSession
import org.scalatest.GivenWhenThen
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.must.Matchers

class PartitioningTest extends AnyFunSuite with GivenWhenThen with Matchers {
  test("partitioning Data Frames to avoid shuffle") {
    Given("spark session with two dfs")
    val spark = SparkSession.builder().master("local[*]").appName("partie").getOrCreate()
    import spark.implicits._

    val df1 = spark.sparkContext.parallelize(List(UserData("A", "dupa"), UserData("B", "makabra"), UserData("C", "dziupla"), UserData("D", "ipsum"))).toDF()

    val repartDF1 = df1.repartition($"userId")

    val df2 = spark.sparkContext.parallelize(List(UserTransaction("B", 125), UserTransaction("E", 1024), UserTransaction("C", 68),UserTransaction("B", 3), UserTransaction("A", 5))).toDF()

    val repartDf2 = df2.repartition($"userId")

    val joinedDF = repartDF1.join(repartDf2, "userId").cache()

    assert(joinedDF.count() == 4)

    joinedDF.show(truncate = false)
    joinedDF.explain(true)
    spark.close()
  }
}
