package com.spark.train.chpt01.chpt05

import com.spark.train.chpt01.{Person, Transaction}
import org.apache.spark.sql.SparkSession
import org.scalatest.GivenWhenThen
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.must.Matchers
import org.slf4j.event.Level

class DFJoinsTest extends AnyFunSuite with GivenWhenThen with Matchers {
  test("inner join on two dfs") {
    Given("create session and initial dfs")
    val spark = SparkSession.builder().master("local[*]").appName("DFAlwaysDFs").getOrCreate()
    spark.sparkContext.setLogLevel("INFO")
    import spark.implicits._
    val transactions = spark.sparkContext.parallelize(List((1, "car"), (2, "gun"), (4, "bag"), (5, "fertilizer"))).toDF("userId", "item")
    val persons = spark.sparkContext.parallelize(List((1, "Tom"), (2, "Michael"), (3, "Johnny"))).toDF("id", "user")

    When("join is performed")
    val result = transactions.join(persons, $"userId" === $"id", "inner").drop("id").cache()

    Then("compare result with expectation")
    result.show(truncate = false)

    assert(result.count() == 2)
  }

  test("left join with two DFs") {
    Given("two dfs")
    val spark = SparkSession.builder().master("local[*]").appName("DFAlwaysDFs").getOrCreate()
    spark.sparkContext.setLogLevel("INFO")
    import spark.implicits._
    val transactions = spark.sparkContext.parallelize(List((1, "car"), (2, "gun"), (4, "bag"), (5, "fertilizer"))).toDF("userId", "item")
    val persons = spark.sparkContext.parallelize(List((1, "Tom"), (2, "Michael"), (3, "Johnny"))).toDF("id", "user")

    When("left join is performed")

    val result = transactions.join(persons, $"userId" === $"id", "left_outer").drop($"id").na.fill("").cache()

    Then("result should match the expctation")
    result.show(truncate = false)

    assert(result.count() == 4)
  }

  test("outer join with two DFs") {
    Given("two dfs")
    val spark = SparkSession.builder().master("local[*]").appName("DFAlwaysDFs").getOrCreate()
    spark.sparkContext.setLogLevel("INFO")
    import spark.implicits._
    val transactions = spark.sparkContext.parallelize(List((1, "car"), (2, "gun"), (4, "bag"), (5, "fertilizer"))).toDF("userId", "item")
    val persons = spark.sparkContext.parallelize(List((1, "Tom"), (2, "Michael"), (3, "Johnny"))).toDF("id", "user")

    When("full outer join is performed")

    val result = transactions.join(persons, $"userId" === $"id", "full_outer").drop($"id").na.fill(-1, Array("userId")).cache()

    Then("result should match the expctation")
    result.show(truncate = false)

    assert(result.count() == 5)
  }

  test("perform inner join on DS") {
    Given("two DSes")
    val spark = SparkSession.builder().master("local[*]").appName("DFAlwaysDFs").getOrCreate()
    spark.sparkContext.setLogLevel("INFO")
    import spark.implicits._
    val transactions = spark.sparkContext.parallelize(List(Transaction(1, "car"), Transaction(2, "gun"), Transaction(4, "bag"), Transaction(5, "fertilizer"))).toDS()
    val persons = spark.sparkContext.parallelize(List(Person(1, "Tom"), Person(2, "Michael"), Person(3, "Johnny"))).toDS()

    When("full outer join is performed")

    val result = transactions.joinWith(persons, $"userId" === $"id", "inner").cache()

    Then("result should match the expctation")
    result.show(truncate = false)

    assert(result.count() == 2)
  }

  test("perform right join on DS") {
    Given("two DSes")
    val spark = SparkSession.builder().master("local[*]").appName("DFAlwaysDFs").getOrCreate()
    spark.sparkContext.setLogLevel("INFO")
    import spark.implicits._
    val transactions = spark.sparkContext.parallelize(List(Transaction(1, "car"), Transaction(2, "gun"), Transaction(4, "bag"), Transaction(5, "fertilizer"))).toDS()
    val persons = spark.sparkContext.parallelize(List(Person(1, "Tom"), Person(2, "Michael"), Person(3, "Johnny"))).toDS()

    When("full outer join is performed")

    val result = transactions.joinWith(persons, $"userId" === $"id", "right_outer").cache()

    Then("result should match the expctation")
    result.show(truncate = false)

    assert(result.count() == 3)
  }

  test("perform left join on DS") {
    Given("two DSes")
    val spark = SparkSession.builder().master("local[*]").appName("DFAlwaysDFs").getOrCreate()
    spark.sparkContext.setLogLevel("INFO")
    import spark.implicits._
    val transactions = spark.sparkContext.parallelize(List(Transaction(1, "car"), Transaction(2, "gun"), Transaction(4, "bag"), Transaction(5, "fertilizer"))).toDS()
    val persons = spark.sparkContext.parallelize(List(Person(1, "Tom"), Person(2, "Michael"), Person(3, "Johnny"))).toDS()

    When("full outer join is performed")

    val result = transactions.joinWith(persons, $"userId" === $"id", "left_outer").cache()

    Then("result should match the expctation")
    result.show(truncate = false)

    assert(result.count() == 4)
  }
}
