package com.spark.train.chpt01.chpt03

import com.spark.train.model.UserTransaction
import org.apache.spark.{Partitioner, SparkContext}
import org.scalatest.GivenWhenThen
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.must.Matchers

class PartitionByParttitions extends AnyFunSuite with GivenWhenThen with Matchers {
  test("Partitions by partitions") {
    Given("sesja i rdd")
    val spark = new SparkContext("local[*]", "app")
    val executors = 2
    val rdd = spark.parallelize(List(UserTransaction("A", 125), UserTransaction("B", 5), UserTransaction("C", 666), UserTransaction("C", 1024)))
        .keyBy(_.userId)
        .partitionBy(new Partitioner {
          override def numPartitions: Int = executors

          override def getPartition(key: Any): Int = key.hashCode() % executors
        })

    println(rdd.partitions.length)

    val result = rdd.mapPartitions{part =>
      part.map(_._2.amount)
    }.collect().toList
    spark.stop()

    result must contain theSameElementsAs List(125,5, 666, 1024)
  }
}
