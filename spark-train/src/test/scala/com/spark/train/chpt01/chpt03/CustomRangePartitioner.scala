package com.spark.train.chpt01.chpt03

import org.apache.spark.Partitioner

class CustomRangePartitioner(range: List[(Int, Int)]) extends Partitioner {
  override def numPartitions: Int = range.size

  override def getPartition(key: Any): Int = {
    if (key == null || !key.isInstanceOf[Int]) {
      throw new IllegalArgumentException("not today")
    }

    val asKey = key.asInstanceOf[Int]
    val index = range.lastIndexWhere(i => i._1 <= asKey && asKey < i._2)
    println(s"for key $key index was $index")
    index
  }
}
