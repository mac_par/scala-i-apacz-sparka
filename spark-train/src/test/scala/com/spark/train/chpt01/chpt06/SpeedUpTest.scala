package com.spark.train.chpt01.chpt06

import org.apache.spark.rdd.RDD
import org.apache.spark.{HashPartitioner, SparkContext}
import org.scalatest.GivenWhenThen
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.must.Matchers

import scala.collection.mutable.ArrayBuffer

class SpeedUpTest extends AnyFunSuite with Matchers with GivenWhenThen {
  type Mine = (Int, Long)

  test("aggregateBykey example") {
    Given("SparkContext and a RDD")
    val sc = new SparkContext("local[*]", "costam")
    sc.setLogLevel("INFO")
    val input = sc.parallelize(Array((1, 25L),(3, 45L), (2, 20L), (3, 15L), (1, 20L), (5,11L), (6, 5L)))
    val keyed = input.keyBy(_._1).cache()
    val fnt: ((Int, Long), (Int, Long)) => (Int, Long) = (uno:Mine, due:Mine) => (uno._1, uno._2 + due._2)

    When("aggregated")
    val result = keyed.aggregateByKey(ArrayBuffer.empty[Long])((x,y)=> {x+=y._2; x;}, (x,y)=> x++= y)


    val result2: RDD[(Int, (Int, Long))] = keyed.reduceByKey(new HashPartitioner(4), fnt)
    Then("compare results")

    result.collect().toList must contain theSameElementsAs List(
      (1, ArrayBuffer(25L, 20L)),
      (2,ArrayBuffer(20L)),
      (3, ArrayBuffer(45L,15L)),
      (5,ArrayBuffer(11L)),
      (6,ArrayBuffer(5L))
    )

    result2.collect().toList must contain theSameElementsAs List(
      (1, (1,45L)),
      (2, (2,20L)),
      (3,(3,60L)),
      (5, (5,11L)),
      (6, (6,5L))
    )
    sc.stop()
  }


  test("proba die reducten") {
    Given("SparkContext and a RDD")
    val sc = new SparkContext("local[*]", "costam")
    sc.setLogLevel("INFO")
    val input = sc.parallelize(Array((1, 25L),(3, 45L), (2, 20L), (3, 15L), (1, 20L), (5,11L), (6, 5L)))

    When("aggregated")
    val keyed = input.reduceByKey(_+_)
    Then("compare results")

    keyed.collect().toList must contain theSameElementsAs List(
      (1, 45L),
      (2, 20L),
      (3,60L),
      (5, 11L),
      (6, 5L)
    )
    sc.stop()
  }
}
