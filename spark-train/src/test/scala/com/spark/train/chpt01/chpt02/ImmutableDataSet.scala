package com.spark.train.chpt01.chpt02

import com.spark.train.model.UserData
import org.apache.spark.sql.SparkSession
import org.scalatest.GivenWhenThen
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.must.Matchers

class ImmutableDataSet extends AnyFunSuite with GivenWhenThen with Matchers {
  test("jakis tam test") {
    Given("Spark session i lista userów")
    val spark = SparkSession.builder().master("local[*]").appName("dupa").getOrCreate()
    import spark.implicits._

    val dataSet = spark.sparkContext.parallelize(List(UserData("A", "fdfsdf"), UserData("C", "fgdfgd"), UserData("E", "hmhmhm")))
        .toDS()

    val filteredDs = dataSet.filter($"userId".isin("E")).collect().toList

    assert(filteredDs.nonEmpty)
    filteredDs must contain theSameElementsAs List(UserData("E", "hmhmhm"))
    spark.close()
  }
}
