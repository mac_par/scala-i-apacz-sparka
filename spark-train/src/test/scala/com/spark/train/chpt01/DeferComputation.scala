package com.spark.train.chpt01

import com.spark.train.model.InputRecord
import org.apache.spark.sql.SparkSession
import org.scalatest.GivenWhenThen
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.must.Matchers

class DeferComputation extends AnyFunSpec with Matchers with GivenWhenThen {
  val spark = SparkSession.builder().appName("Defer it").master("local[*]").getOrCreate()
  describe("defered computation") {
    it("It should be defered") {
      Given("prepare input RDD")
      val input = spark.sparkContext.makeRDD(Seq(InputRecord(userId = "A"), InputRecord(userId = "B"), InputRecord(userId = "C")))

      When("filter on A and on userId key")
      val rdd = input.filter(_.userId.contains("A"))
        .keyBy(_.userId)
        .map(_._2.userId.toLowerCase())

      Then("should be defered")
      rdd.collect().toList
    }

  }
}
