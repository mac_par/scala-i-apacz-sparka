package com.spark.train.chpt01.chpt4

import org.apache.spark.sql.SparkSession
import org.scalatest.GivenWhenThen
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.must.Matchers
import org.apache.spark.sql.functions.udf

class Udfunctions extends AnyFunSuite with GivenWhenThen with Matchers {
  val spark = SparkSession.builder().master("local[*]").appName("cos z udf").getOrCreate()

  test("adding udf") {
    Given("df i udf function")
    spark.sparkContext.setLogLevel("INFO")
    import spark.implicits._

    val df = spark.sparkContext.parallelize(Seq((1,"manko"), (2,"siamko"), (3,"tamko"))).toDF("id", "slowo")

    val upper = (word: String) => word.toUpperCase()

    val tmpUdf = udf(upper)

    When("performing action")
    val resultDF = df.select($"id", tmpUdf($"slowo"))

    Then("Slowo column is in upper case")
    resultDF.show(truncate = false)
  }

  test("another try with udp") {
    Given("create df and register udf")
    spark.sparkContext.setLogLevel("INFO")
    import spark.implicits._
    val inputDf = spark.sparkContext.parallelize(Seq((1,"manko"), (2,"siamko"), (3,"tamko"))).toDF("id","slowo")
    inputDf.createOrReplaceTempView("cosTam")

    //register function
    spark.udf.register("uppEm", (word:String) => word.toUpperCase)

    When("performing action")

    val result = spark.sql("select id, uppEm(slowo) as slowa from cosTam").cache()

    Then("display result")
    result.show(truncate = false)

    result.explain()
  }
}
