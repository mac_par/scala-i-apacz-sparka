package com.spark.train.chpt01.chpt03

import com.spark.train.model.{UserData, UserTransaction}
import org.apache.spark.sql.SparkSession
import org.scalatest.GivenWhenThen
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.must.Matchers

class JoinTwoDFs extends AnyFunSuite with GivenWhenThen with Matchers {
  test("testing joins on DFs") {
    Given("spark sessio i 2 dfy")
    val spark = SparkSession.builder().master("local[ca*]").appName("dupa2").getOrCreate()
    import spark.implicits._

    val df1 = spark.sparkContext.parallelize(List(UserData("user1", "dupa"), UserData("user2", "apud"), UserData("user3", "marko"))).toDF()
    val df2 = spark.sparkContext.parallelize(List(UserTransaction("user4", 200), UserTransaction("user1", 100), UserTransaction("user2", 10))).toDF()
    When("joinowanie")
    val innerJoin = df1.join(df2, "userId").cache()

    Then("2 elementy są dostępne")
    assert(innerJoin.count() == 2)
    innerJoin.show(truncate = false)

    val v2 = df1.join(df2, Seq("userId"), "left").cache()
    assert(v2.count() == 3)
    v2.show(truncate = false)

    val v3 = df2.join(df1, Seq("userId"), "right")
      .cache()
    v3.show(truncate = false)
    spark.close()
  }
}
