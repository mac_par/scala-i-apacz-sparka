package com.spark.train.chpt01.chpt02

import org.apache.spark.SparkContext
import org.scalatest.GivenWhenThen
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.must.Matchers

class ImmutabilityTest extends AnyFunSpec with Matchers with GivenWhenThen {
  describe("Immutability in Spark") {
    it("seq of elements") {
      Given("SparkContext and Sequence")
      val sc = new SparkContext("local[*]", "test1")

      val data = sc.parallelize(0 to 5)
      val manipulated = data.map(_ *2)

      manipulated.collect.toList must contain theSameElementsAs List(0,2,4,6,8,10)

      data.collect.toList must contain theSameElementsAs List(0,1,2,3,4,5)

      sc.stop()
    }
  }

}
