package com.spark.train.chpt01.chpt03

import com.spark.train.model.UserTransaction
import org.apache.spark.{HashPartitioner, RangePartitioner, SparkContext}
import org.scalatest.{BeforeAndAfter, GivenWhenThen}
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.must.Matchers

import scala.collection.mutable.ArrayBuffer

class GroupBy1 extends AnyFunSuite with Matchers with GivenWhenThen{

  test("grupowanie") {
    Given("Spark Session i rdd")
    val spark = new SparkContext("local[*]", "cos tam")
    spark.setLogLevel("INFO")

    val list = Seq(UserTransaction("A", 125), UserTransaction("B", 25), UserTransaction("C", 50), UserTransaction("A", 5), UserTransaction("B", 45), UserTransaction("A", 45))
    val rdd = spark.parallelize(list)

    val keyedRdd = rdd.keyBy(_.userId)

    When("aggregating ")
    val aggregated = keyedRdd.aggregateByKey(scala.collection.mutable.ArrayBuffer.empty[Long])((buff, user)=> buff += user.amount, (buff1, buff2)=> buff1 ++= buff2)


    Then("Check")
    aggregated.collect().toList must contain theSameElementsAs List(
      "A" -> ArrayBuffer(125,5, 45),
      "B" -> ArrayBuffer(25, 45),
      "C" -> ArrayBuffer(50)
    )
    spark.stop()
  }

  test("grupowanie2") {
    Given("Spark Session i rdd")
    val spark = new SparkContext("local[*]", "cos tam")
    spark.setLogLevel("INFO")

    val list = Seq(UserTransaction("A", 125), UserTransaction("B", 25), UserTransaction("C", 50), UserTransaction("A", 5), UserTransaction("B", 45), UserTransaction("A", 45))
    val rdd = spark.parallelize(list)

    val keyedRdd = rdd.keyBy(_.userId)

    When("aggregating ")
    val reduced = keyedRdd.reduceByKey((u1, u2)=> UserTransaction(u1.userId, u1.amount + u2.amount))


    Then("Check")
    reduced.collect().toList must contain theSameElementsAs List(
      "A" -> UserTransaction("A", 175),
      "B" -> UserTransaction("B", 70),
      "C" -> UserTransaction("C", 50)
    )
    spark.stop()
  }

  test("zmienianie opcji partycjonowania") {
    Given("Spark Session i rdd")
    val spark = new SparkContext("local[*]", "cos tam")
    spark.setLogLevel("INFO")

    val list = Seq(UserTransaction("A", 125), UserTransaction("B", 25), UserTransaction("C", 50), UserTransaction("A", 5), UserTransaction("B", 45), UserTransaction("A", 45))
    val rdd = spark.parallelize(list)

    val keyedRdd = rdd.keyBy(_.userId)

    When("partycjonujemy")
    val part1 = keyedRdd.partitioner
    println(part1)
    assert(!part1.isDefined)

    Then("set partitioner")
    val hashedPartioner = keyedRdd.partitionBy(new HashPartitioner(5))
    val hashed = hashedPartioner.partitioner
    println(hashed)
    assert(hashed.isDefined)

    val byRange = keyedRdd.partitionBy(new RangePartitioner(5, keyedRdd))
    val part3 = byRange.partitioner
    println(part3)
    assert(part3.isDefined)

    spark.stop()
  }

  test("własny partitioner") {
    Given("Spark Session i rdd")
    val spark = new SparkContext("local[*]", "cos tam")
    spark.setLogLevel("INFO")

    val list = Seq(UserTransaction("A", 125), UserTransaction("B", 25), UserTransaction("C", 50), UserTransaction("A", 5), UserTransaction("B", 45), UserTransaction("A", 45))
    val rdd = spark.parallelize(list)

    val keyedRdd = rdd.keyBy(_.amount)

    When("partycjonujemy")
    val part = keyedRdd.partitionBy(new CustomRangePartitioner(List((0,15),(15,30),(30,70), (70, 4555555))))

    part.collect()
    spark.stop()
  }
}
