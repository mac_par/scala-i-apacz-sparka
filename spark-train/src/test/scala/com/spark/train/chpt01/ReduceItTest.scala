package com.spark.train.chpt01

import com.spark.train.model.UserTransaction
import org.apache.spark.SparkContext
import org.scalatest.GivenWhenThen
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

class ReduceItTest extends AnyFunSpec with GivenWhenThen with Matchers {

  describe("Reduce testing") {
    it("Showing reduce") {
      Given("SparkContext and initial rdd")
      val sc = new SparkContext("local[*]", "cos tam")
      val rdd = sc.parallelize(List(UserTransaction("A", 15), UserTransaction("A", 25), UserTransaction("A", 5), UserTransaction("A", 1)))

      When("values are reduced to the biggest number")
      val value = rdd.map(_.amount).reduce { (a, b) => if (a > b) a else b }

      Then("values should be 25")
      value should be
      25
      sc.stop()
    }
  }

  describe("cos tam") {
    it("Showing reduce by ReduceByKey Api") {
      Given("SparkContext and initial rdd")
      val sc = new SparkContext("local[*]", "cos tam")
      val rdd = sc.parallelize(List(UserTransaction("A", 15), UserTransaction("A", 25), UserTransaction("A", 5), UserTransaction("A", 1),
        UserTransaction("B", 14)))

      When("values are reduced to user's highest amount")
      val reduced = rdd
        //      .map{item => (item.userId, item)}
        .keyBy(_.userId)
        .reduceByKey { (a, b) => if (a.amount > b.amount) a else b }.collect().toList

      Then("values should be 25")
      reduced should contain theSameElementsAs List(("A",UserTransaction("A", 25)), ("B", UserTransaction("B", 14)))
      sc.stop()
    }
  }
}
