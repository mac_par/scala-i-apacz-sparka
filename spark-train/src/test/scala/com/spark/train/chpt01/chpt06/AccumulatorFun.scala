package com.spark.train.chpt01.chpt06

import org.apache.spark.SparkContext
import org.scalatest.GivenWhenThen
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.must.Matchers

class AccumulatorFun extends AnyFunSuite with Matchers with GivenWhenThen {
  test("Uzycie akumulatorów") {
    Given("spark context, kolekcja oraz akumulator")

    val sc = new SparkContext("local[*]", "cos tam")
    sc.setLogLevel("ERROR")
    val accum = sc.longAccumulator("jakis acumulator")
    val rdd = sc.parallelize(1 until 50)

    rdd.mapPartitions{ part =>
      part.map{i =>
        accum.add(1)
        i *5
      }
    }.collect()

    assert(accum.value == 49L)
    sc.stop()
  }
}
