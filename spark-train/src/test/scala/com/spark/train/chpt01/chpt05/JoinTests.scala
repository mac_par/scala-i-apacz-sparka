package com.spark.train.chpt01.chpt05

import org.apache.spark.{HashPartitioner, SparkContext}
import org.apache.spark.rdd.RDD
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.must.Matchers
import org.scalatest.{BeforeAndAfter, GivenWhenThen}

class JoinTests extends AnyFunSuite with GivenWhenThen with Matchers with BeforeAndAfter {
  val sc: SparkContext = new SparkContext("local[*]", "JoinTest")

  test("Simple inner join") {
    Given("initial two rdds")
    val transactions = sc.parallelize(List((1, "car"), (2, "gun"), (4, "bag"), (5, "fertilizer")))
    val persons = sc.parallelize(List((1, "Tom"), (2, "Michael"), (3, "Johnny")))

    When("join")
    val join: RDD[(Int, (String, String))] = transactions.join(persons)

    Then("check result")
    val result = join.collect().toList

    result must contain theSameElementsAs List((1, ("car", "Tom")), (2, ("gun", "Michael")))
  }

  test("right outer join") {
    Given("two rdds")
    val transactions = sc.parallelize(List((1, "car"), (2, "gun"), (4, "bag"), (5, "fertilizer")))
    val persons = sc.parallelize(List((1, "Tom"), (2, "Michael"), (3, "Johnny")))

    When("right join transations with persons")
    val rightJoin = transactions.rightOuterJoin(persons)

    Then("result should match expectation")
    rightJoin.collect().toList must contain theSameElementsAs List(
      (1, (Some("car"), "Tom")),
      (2, (Some("gun"), "Michael")),
      (3, (None, "Johnny"))
    )
  }

  test("left outer join test") {
    Given("two rdds")
    val transactions = sc.parallelize(List((1, "car"), (2, "gun"), (4, "bag"), (5, "fertilizer")))
    val persons = sc.parallelize(List((1, "Tom"), (2, "Michael"), (3, "Johnny")))

    When("left outer join on transactions with persons is done")
    val result = transactions.leftOuterJoin(persons)

    Then("compare result with expectations")
    result.collect().toList must contain theSameElementsAs List(
      (1, ("car", Some("Tom"))),
      (2, ("gun", Some("Michael"))),
      (4, ("bag", None)),
      (5, ("fertilizer", None))
    )
  }

  test("full outer join") {
    Given("two rdds")
    val transactions = sc.parallelize(List((1, "car"), (2, "gun"), (4, "bag"), (5, "fertilizer")))
    val persons = sc.parallelize(List((1, "Tom"), (2, "Michael"), (3, "Johnny")))

    When("performing full outer join")
    val result = transactions.fullOuterJoin(persons)

    Then("result must meet expectation")
    result.collect.toList must contain theSameElementsAs List(
      (1, (Some("car"),Some("Tom"))),
      (2, (Some("gun"), Some("Michael"))),
      (3, (None, Some("Johnny"))),
      (4, (Some("bag"), None)),
      (5, (Some("fertilizer"), None))
    )
  }

  test("perform inner join with custom partitioner") {
    Given("two rdds")
    val transactions = sc.parallelize(List((1, "car"), (2, "gun"), (4, "bag"), (5, "fertilizer")))
    val persons = sc.parallelize(List((1, "Tom"), (2, "Michael"), (3, "Johnny")))

    When("prepare partitioner and the join them using it")
    val partitioner = transactions.partitioner match {
      case Some(part) => part
      case None => new HashPartitioner(5)
    }

    Then("compare result with expected result")
    val result = transactions.join(persons, partitioner)

    result.collect.toList must contain theSameElementsAs List(
      (1, ("car", "Tom")),
      (2, ("gun","Michael"))
    )
  }

  test("broadcasting a small dataset to all executors test") {
    Given("two rdds")
    val transactions = sc.parallelize(List((1, "car"), (2, "gun"), (4, "bag"), (5, "fertilizer")))
    val persons = sc.parallelize(List((1, "Tom"), (2, "Michael"), (3, "Johnny")))

    When("preparing broadcast and then broadcasting")
    val broad = sc.broadcast(persons.collect.toMap)

   val result = transactions.mapPartitions{ partitions=>
     partitions.flatMap{
       case (p,r) => broad.value.get(p) match {
         case Some(z) => Seq((p,(r,z)))
         case None => Seq.empty
       }
     }
   }

    Then("result should match expectations")
    result.collect.toList must contain theSameElementsAs List(
      (1, ("car", "Tom")),
      (2,("gun","Michael"))
    )
  }
}
