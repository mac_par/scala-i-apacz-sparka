package com.ml.course.chpt01

import org.apache.spark.ml.classification.DecisionTreeClassifier
import org.apache.spark.ml.evaluation.BinaryClassificationEvaluator
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.sql.SparkSession

object Cryotherapy01_treat extends App {
  val session = SparkSession.builder()
    .appName("Cryonics01")
    .master("local[*]")
    .config("spark.sql.warehouse.dir", "/tmp").getOrCreate()
  session.sparkContext.setLogLevel("WARN")
  private val featuresLabel = "features"
  private val resultTreatementLabel = "Result_of_Treatment"
  private val labelLabel = "label"

  val inputData = session.read
    .option("header", value = true).option("inferSchema", value = true)
    .format("com.databricks.spark.csv")
    .load("bin/cryotherapy/Cryotherapy.csv")
    .cache()

  //preparing fields
  val cryotherapyDF = inputData.withColumnRenamed(resultTreatementLabel, labelLabel)
  val featuresNamesArray = cryotherapyDF.schema.fields.filter(field => field.name != labelLabel).map(_.name)
  val vectorAssembler = new VectorAssembler().setInputCols(featuresNamesArray).setOutputCol(featuresLabel)
  val numericDF = vectorAssembler.transform(cryotherapyDF).select(labelLabel, featuresLabel)

  //preparing model
  val parts = numericDF.randomSplit(Array(0.8, 0.2))
  val trainDF = parts(0)
  val testDF = parts(1)
  val dt = new DecisionTreeClassifier()
    .setImpurity("gini")
    .setMaxBins(10)
    .setMaxDepth(30)
    .setFeaturesCol(featuresLabel)
    .setLabelCol(labelLabel)

  val dtModel = dt.fit(trainDF)

  val evaluator = new BinaryClassificationEvaluator().setLabelCol(labelLabel)
  val predictionDF = dtModel.transform(testDF)
  val accurary = evaluator.evaluate(predictionDF)
  println(accurary)
}
