package com.ml.course.chpt01

import org.apache.spark.sql.SparkSession

object Cryotherapy01 extends App {
    val session = SparkSession.builder()
      .appName("Cryonics01")
      .master("local[*]")
      .config("spark.sql.warehouse.dir", "/tmp").getOrCreate()
  session.sparkContext.setLogLevel("WARN")

  val inputData = session.read
    .option("header", value = true).option("inferSchema", value = true)
    .format("com.databricks.spark.csv")
    .load("bin/cryotherapy/Cryotherapy.csv")
    .cache()

  inputData.printSchema()
  inputData.show()
  inputData.describe().show()
  inputData.createTempView("cryotherapy_data")
  session.sql("select * from cryotherapy_data").show()
}
