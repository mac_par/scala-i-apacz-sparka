package com.ml.course.chpt02

import org.apache.spark.ml.evaluation.RegressionEvaluator
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.ml.regression.{GeneralizedLinearRegression, LinearRegression}
import org.apache.spark.ml.tuning.{CrossValidator, ParamGridBuilder}
import org.apache.spark.mllib.evaluation.RegressionMetrics
import org.apache.spark.sql.{Row, SparkSession}

object UrbanTraffic02 extends App {
  val labelColumn = "Slowness in traffic (%)"
  val modelLabel = "label"
  val modelFeatures = "features"
  val modelPrediction = "prediction"
  val session = SparkSession.builder().master("local[*]").appName("UrbanTraffic02").getOrCreate()
  session.sparkContext.setLogLevel("WARN")

  import session.implicits._

  val inputDF = session.read
    .option("inferSchema", value = true)
    .option("header", value = true)
    .option("delimiter", ";")
    .format("csv").load("bin/urban-traffic/urban-traffic.csv").cache()

  val featuresNamesArray = inputDF.columns.filter(col => col != labelColumn)
  val vectorAssembler = new VectorAssembler().setInputCols(featuresNamesArray).setOutputCol(modelFeatures)
  val dataDF = vectorAssembler.transform(inputDF)
    .withColumnRenamed(labelColumn, modelLabel)
    .select(modelLabel, modelFeatures)

  val splitData = dataDF.randomSplit(Array(.6, .4))
  val trainData = splitData(0)
  val testData = splitData(1)

  val glr = new GeneralizedLinearRegression().setLabelCol(modelLabel).setFeaturesCol(modelFeatures)

  val glrModel = glr.fit(trainData)

  val trainPredictionsAndLabel = glrModel.transform(testData).select(modelLabel, modelPrediction)
    .map { case Row(label: Double, prediction: Double) => (label, prediction) }.rdd

  val testRegressionMetrics = new RegressionMetrics(trainPredictionsAndLabel)
  val results = "\n=====================================================================\n" +
    s"TrainingData count: ${trainData.count}\n" +
    s"TestData count: ${testData.count}\n" +
    "=====================================================================\n" +
    s"TestData MSE = ${testRegressionMetrics.meanSquaredError}\n" +
    s"TestData RMSE = ${testRegressionMetrics.rootMeanSquaredError}\n" +
    s"TestData R-squared = ${testRegressionMetrics.r2}\n" +
    s"TestData MAE = ${testRegressionMetrics.meanAbsoluteError}\n" +
    s"TestData explained variance = ${testRegressionMetrics.explainedVariance}\n" +
    "=====================================================================\n"
  println(results)

  println("Preparing K-fold Cross Validation and Grid Search")
  val paramGrid = new ParamGridBuilder()
    .addGrid(glr.maxIter, Array(10, 20, 30, 50, 100, 500, 1000))
    .addGrid(glr.regParam, Array(0.001, 0.01, 0.1))
    .addGrid(glr.tol, Array(0.01, 0.1))
    .build()

  val numFolds = 10
  val crossValidator = new CrossValidator()
    .setEstimator(glr)
    .setEvaluator(new RegressionEvaluator())
    .setEstimatorParamMaps(paramGrid)
    .setNumFolds(numFolds)

  println("Training model with the Linear Regression algorithm")
  val cvModel = crossValidator.fit(trainData)
  cvModel.write.overwrite().save("bin/tmp/LR_model")

//  val sameCvModel = LinearRegression.load("bin/tmp/LR_model")

  println("Evaluating the cross validate model on the test set and calculating the regression metrics")
  val trainPredictionsAndLabelCv = cvModel.transform(testData).select(modelLabel, modelPrediction)
    .map{case Row(label: Double, prediction: Double) => (label, prediction)}.rdd
  val testRegressionMetricsCv = new RegressionMetrics(trainPredictionsAndLabelCv)

  val resultsCv = "\n=====================================================================\n" +
    s"TrainingData count: ${trainData.count}\n" +
    s"TestData count: ${testData.count}\n" +
    "=====================================================================\n" +
    s"TestData MSE = ${testRegressionMetricsCv.meanSquaredError}\n" +
    s"TestData RMSE = ${testRegressionMetricsCv.rootMeanSquaredError}\n" +
    s"TestData R-squared = ${testRegressionMetricsCv.r2}\n" +
    s"TestData MAE = ${testRegressionMetricsCv.meanAbsoluteError}\n" +
    s"TestData explained variance = ${testRegressionMetricsCv.explainedVariance}\n" +
    "=====================================================================\n"
  println(resultsCv)
}
