package com.ml.course.chpt02

import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.ml.regression.LinearRegression
import org.apache.spark.mllib.evaluation.RegressionMetrics
import org.apache.spark.sql.types.DoubleType
import org.apache.spark.sql.{DataFrame, Row, SparkSession}

object UrbanTraffic01 extends App {
  val labelColumn = "Slowness in traffic (%)"
  val modelLabel = "label"
  val modelFeatures = "features"
  val modelPrediction = "prediction"

  val session = SparkSession.builder().master("local[*]").appName("UrbanTraffic").getOrCreate()
  session.sparkContext.setLogLevel("OFF")
  import session.implicits._
  val inputDF = session.read.option("inferSchema", value = true).option("header", value = true).option("delimiter", ";")
    .format("csv").load("bin/urban-traffic/urban-traffic.csv").cache()

  val featuresColumns = inputDF.columns.filter(_ != labelColumn)

  val featuresVector = new VectorAssembler()
    .setOutputCol(modelFeatures)
    .setInputCols(featuresColumns.toArray)

  val modelDF = featuresVector.transform(inputDF).withColumnRenamed(labelColumn, modelLabel).select(modelFeatures, modelLabel)

  val parts = modelDF.randomSplit(Array(0.6, 0.4))
  val (trainDF, testDF) = (parts(0), parts(1))
  trainDF.cache()
  testDF.cache()
  val lr = new LinearRegression().setLabelCol(modelLabel).setFeaturesCol(modelFeatures)
  val lrModel = lr.fit(trainDF)
  println("Metrics")
  val trainPredictionsAndModel = lrModel.transform(testDF).select(modelLabel, modelPrediction)
    .map{
      case Row(label: Double, prediction: Double) => (label, prediction)
    }.rdd

  val regressionMetric = new RegressionMetrics(trainPredictionsAndModel)
  val results =
    s"""=====================================================================\n"
    TrainingData count: ${trainDF.count}
    "TestData count: ${testDF.count}
    "=====================================================================
    "TestData MSE = ${regressionMetric.meanSquaredError}
    "TestData RMSE = ${regressionMetric.rootMeanSquaredError}
    "TestData R-squared = ${regressionMetric.r2}
    "TestData MAE = ${regressionMetric.meanAbsoluteError}
    "TestData explained variance =  ${regressionMetric.explainedVariance}
    "=====================================================================\n"""
  println(results)
}
