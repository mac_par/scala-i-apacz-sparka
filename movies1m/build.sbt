import Dependencies._

scalaVersion     := "2.12.10"
version          := "1.0-SNAPSHOT"
organization     := "com.movies"
organizationName := "movies"
exportJars := true

mainClass in (Compile, packageBin) := Some("com.movies.SimiliarMovies")

lazy val root = (project in file("."))
  .settings(
    name := "movies1M",
    libraryDependencies ++= Seq(
      "org.apache.spark" % "spark-core_2.12" % "2.4.4" % "provided"
    )
  )