package com.movies

import java.io.File
import java.nio.charset.{CodingErrorAction, StandardCharsets}

import org.apache.spark.{HashPartitioner, SparkConf, SparkContext}

import scala.io.{BufferedSource, Codec, Source}
import scala.math.sqrt

object SimiliarMovies {
  val SEPARATOR: String = "::"

  //Title, Genre
  type MovieDetail = (String, String)

  def getMoviesDetails: Map[Int, MovieDetail] = {
    implicit val codec: Codec = Codec(StandardCharsets.UTF_8)
    codec.onUnmappableCharacter(CodingErrorAction.REPLACE)
    codec.onMalformedInput(CodingErrorAction.REPLACE)

    val map: scala.collection.mutable.Map[Int, MovieDetail] = scala.collection.mutable.Map[Int, MovieDetail]()
    val source: BufferedSource = Source.fromFile("movies.dat")
    for {line <- source.getLines()} {

      val fields = line.split(SEPARATOR)
      map += (fields(0).toInt -> (fields(1), fields(2)))
    }
    source.close()

    map.toMap
  }

  type UserRating = (Int, Double)
  type UserRatingEntry = (Int, UserRating)

  def mapRatings(line: String): UserRatingEntry = {
    val fields = line.split(SEPARATOR)
    fields(0).toInt -> (fields(1).toInt, fields(2).toDouble)
  }

  type UserRatings = (Int, (UserRating, UserRating))

  def byAscendingMovieIds(userRating: UserRatings): Boolean = {
    val rating1: UserRating = userRating._2._1
    val rating2: UserRating = userRating._2._2

    rating1._1 < rating2._1
  }

  type RatingPair = (Double, Double)
  type MoviePair = ((Int, Int), RatingPair)

  def moviePairing(userRating: UserRatings): MoviePair = {
    val movie1: UserRating = userRating._2._1
    val movie2: UserRating = userRating._2._2

    (movie1._1, movie2._1) -> (movie1._2, movie2._2)
  }

  def calculateSimiliarity(ratings: Iterable[RatingPair]): (Double, Int) = {
    var pairs: Int = 0
    var sum_xx: Double = 0.0
    var sum_yy: Double = 0.0
    var sum_xy: Double = 0.0

    for {rating <- ratings} {
      val rating1: Double = rating._1
      val rating2: Double = rating._2
      sum_xx += rating1 * rating1
      sum_yy += rating2 * rating2
      sum_xy += rating1 * rating2

      pairs += 1
    }

    val numerator: Double = sum_xy
    val denumerator = sqrt(sum_xx) * sqrt(sum_yy)
    var score: Double = 0.0
    if (denumerator != 0) {
      score = numerator / denumerator
    }

    (score, pairs)
  }

  def main(args: Array[String]): Unit = {
    val config = new SparkConf().setAppName("SimiliarMovies1M")
    val sc = new SparkContext(config)
    sc.setLogLevel("ERROR")

    println("Loading movies...")
    val moviesDetails: Map[Int, MovieDetail] = getMoviesDetails

    val ratings = sc.textFile("ratings.dat").map(mapRatings)

    val joinedRatings = ratings.join(ratings).partitionBy(new HashPartitioner(4))
    val reducedRatingsSet = joinedRatings.filter(byAscendingMovieIds)

    val moviePairs = reducedRatingsSet.map(moviePairing).partitionBy(new HashPartitioner(4))
    val groupedMoviePairs = moviePairs.groupByKey

    val similiarMovies = groupedMoviePairs.mapValues(calculateSimiliarity).cache()

    val scoreThreshold: Double = 0.97
    val audienceThreshold: Int = 50

    for {arg <- args} {
      val movieId = arg.toInt

      val proposedMovies = similiarMovies.filter { proposition =>
        val movies = proposition._1
        val scale = proposition._2

        (movies._1 == movieId || movies._2 == movieId) && (scale._1 > scoreThreshold && scale._2 > audienceThreshold)
      }.sortByKey(ascending = false).take(10)

      val movieDetail: MovieDetail = moviesDetails(movieId)
      println(s"Similiar movies for ${movieDetail._1} - ${movieDetail._2}")
      for {proposition <- proposedMovies} {
        val moviePair = proposition._1
        val movieSimiliarity = proposition._2

        var proposed: Int = moviePair._1
        if (proposed == movieId) {
          proposed = moviePair._2
        }

        val tmpData = moviesDetails(proposed)
        println(s"${tmpData._1} - Genre: ${tmpData._2}, score: ${movieSimiliarity._1}, strength: ${movieSimiliarity._1}")
      }
    }
    sc.stop()
  }
}
