package com.irfan

import org.apache.spark.sql.SparkSession

object ReadingCsv {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().appName("apka").master("local[*]").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    import spark.implicits._

    val df = spark.read.format("csv").option("inferSchema", value = true).option("header", value = true).load("bin/retail").toDF
    df.printSchema()

    df.show()

    val df2 = spark.read.format("json").load("bin/rsvpdata").toDF()

    df2.printSchema()

    df2.show()
  }
}
