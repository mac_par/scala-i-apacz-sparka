package com.irfan.part2

import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions.udf

object CsvMitDF {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().master("local[*]").appName("costam").getOrCreate()
    spark.sparkContext.setLogLevel("WARN")
    import spark.implicits._

    val mapper: String => String = (x: String) =>  if (x.equals("United Kingdom")) "UK" else x
    val function1 = udf(mapper)
    spark.udf.register("mappCountry", function1)
    val initDf: DataFrame = spark.read.format("csv").option("inferSchema", value = true).option("delimiter", ",").option("header", value = true)
      .load("bin/irfan/retail.csv")
    val result = initDf.withColumn("Country_TMP", function1($"Country")).drop("Country").withColumnRenamed("Country_TMP", "Country")

    result.show(60)
  }
}
