package com.irfan.part2

import org.apache.spark.SparkContext
import org.apache.spark.util.LongAccumulator

object Csv1 {
  def main(args: Array[String]): Unit = {
    val sc = new SparkContext("local[*]", "gowno1")
    sc.setLogLevel("WARN")
    val accum = sc.longAccumulator("counter")
    val country = sc.broadcast("United Kingdom");
    val rdd = sc.textFile("bin/irfan/retail.csv", 4)
    val lines = rdd.zipWithIndex.filter(x => x._2 != 0).map(_._1)
    val results = lines.mapPartitions{ partition =>
      partition map{item => accum.add(1); (accum.value, item.contains(country.value))}
    }

    results.foreach(println)
    sc.stop()
  }
}
