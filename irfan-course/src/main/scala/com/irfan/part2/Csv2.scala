package com.irfan.part2

import org.apache.spark.SparkContext

object Csv2 {
  def main(args: Array[String]): Unit = {
    val sc = new SparkContext("local[*]", "gowno2")

    val map = Map[String,String]("United Kingdom" -> "UK")
    val broadcast = sc.broadcast(map)
    val accum = sc.longAccumulator("counter")

    val rdd = sc.textFile("bin/irfan/retail.csv")
    val lines = rdd.zipWithIndex().filter(_._2 != 0).mapPartitions{partition =>
      partition.map{ item => item._1.split(",")
      }.map{ array =>
        val country = array.length-1
        val countryName = array(country)
        array(country) = broadcast.value.getOrElse(countryName, countryName)
        array
      }.map(_.mkString(","))
    }

    lines.foreach(println)
    sc.stop()
  }
}
