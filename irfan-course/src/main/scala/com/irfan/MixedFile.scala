package com.irfan

import org.apache.spark.{SparkConf, SparkContext}

object MixedFile {
  def main(args:Array[String]): Unit = {
    val config = new SparkConf().setMaster("local").setAppName("role_model")
    val sc = SparkContext.getOrCreate(config)
    sc.setLogLevel("ERROR")
    sc.hadoopConfiguration.set("textinputformat.record.delimiter", "+")
    val file = sc.textFile("bin/irfan.txt")

    /*file.mapPartitions{partition =>
      partition.flatMap{line=>
        line.split("\\+")
      }.flatMap{line=> line.split(",")}
    }.foreach(println)*/

    file.mapPartitions{partition =>
      partition.map{line =>
        line.split(",").mkString(":")
      }
    }.foreach(println)

    sc.stop()
  }
}
