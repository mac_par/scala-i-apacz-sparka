package com.irfan.part03

import org.apache.spark.SparkContext

object CountCountries extends App {
  val sc = new SparkContext("local[*]", "countryCounter")
  sc.setLogLevel("ERROR")
  val rdd = sc.textFile("bin/irfan/retail.csv").cache()
  val header = rdd.take(1)
  val countryMap = rdd.mapPartitions{partition =>
    partition.filter(line => line != header).map(line => line.split(",")).map(array => array(7) -> 1)
  }

  val sumUp = countryMap.reduceByKey(_+_)
  sumUp.foreach(println)
  sc.stop()
}
