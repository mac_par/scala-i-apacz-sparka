package com.irfan.part03.task01

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.avg

object TotalNightMinutes2 extends App {
  val session = SparkSession.builder().appName("cos").master("local[*]").getOrCreate()

  import session.implicits._
  session.sparkContext.setLogLevel("ERROR")
  val inputDF = session.read.format("csv").option("inferSchema", value = true).option("header", value = true).option("delimiter", ",")
    .load("bin/irfan/customer_churn.csv").toDF()

  val nightMinutesPerState = inputDF.groupBy($"state").agg(avg($"total night minutes").as("avg minutes at night")).orderBy($"state".asc)
  nightMinutesPerState.show()
}
