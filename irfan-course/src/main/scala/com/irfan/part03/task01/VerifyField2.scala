package com.irfan.part03.task01

import org.apache.spark.sql.functions.udf
import org.apache.spark.sql.SparkSession

object VerifyField2 extends App {
  val session = SparkSession.builder().master("local[*]").appName("cos").getOrCreate()
  import session.implicits._
  session.sparkContext.setLogLevel("ERROR")

  val inputDF = session.read.format("csv").option("inferSchema", value = true).option("header", value = true).option("delimiter", ",")
    .load("bin/irfan/customer_churn.csv").toDF()
  val function: String => Boolean = x => x == "yes" || x == "no"
  val pred = udf(function)
  val rows = inputDF.filter(!pred($"international plan"))

  rows.show()
}
