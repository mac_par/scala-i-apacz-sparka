package com.irfan.part03.task01

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{sum, udf}

object CountVmails2 extends App {
  val session = SparkSession.builder().master("local[*]").appName("costam").getOrCreate()
  val states = Set("OH", "OK","RI")
  session.sparkContext.setLogLevel("ERROR")
  import session.implicits._
  val df = session.read.format("csv").option("inferSchema", value = true).option("delimiter", ",").option("header", value = true)
    .load("bin/irfan/customer_churn.csv").toDF()
  val statesPerMessage = df.groupBy($"state").agg(sum($"number vmail messages").as("vmails")).orderBy($"state".asc).cache()
  val total = statesPerMessage.agg(sum($"vmails").as("sum"))
  total.show()

  val compare: String => Boolean = states.contains(_)
  val filterStates = udf(compare)
  statesPerMessage.filter(filterStates($"state")).show()
  statesPerMessage.show()
}
