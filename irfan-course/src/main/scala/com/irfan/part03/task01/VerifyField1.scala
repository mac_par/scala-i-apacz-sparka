package com.irfan.part03.task01

import org.apache.spark.SparkContext

object VerifyField1 extends App {
  val sc = new SparkContext("local[*]", "cos")
  sc.setLogLevel("ERROR")
  val inputRDD = sc.textFile("bin/irfan/customer_churn.csv")
  val header = sc.broadcast(inputRDD.first())
  val results = inputRDD.mapPartitions { partition =>
    partition.filter(line => line != header.value).map { line =>
      val fieldValue = line.split(",")(4)
      "[yes|no]".r.findAllIn(fieldValue).groupCount == 0
    }.filter(!_)
  }

  val result = results.count()
  println(result)
  sc.stop()
}
