package com.irfan.part03.task01

import org.apache.spark.SparkContext
import org.apache.spark.broadcast.Broadcast

object CountVMails1 extends App {
  val sc = new SparkContext("local[*]", "costam")
  sc.setLogLevel("ERROR")
  val initialData = sc.textFile("bin/irfan/customer_churn.csv")
  val header = initialData.first()
  val rows = initialData.mapPartitions { partition =>
    partition.filter(row => row != header).map(row => {
      val arr = row.split(",")
      arr(0)-> arr(6).toInt
    })
  }.cache()

  val totalVMails = rows.values.reduce(_+_)
  val set = Set("OH", "OK", "RI")
  val states: Broadcast[Set[String]] = sc.broadcast(set)
  val selectedStats = rows.mapPartitions{ partition =>
    partition.filter(row => states.value.contains(row._1)).map(_._2)
  }.reduce(_+_)


  println(totalVMails)
  println(selectedStats)

  val max = rows.map(_._2).max()
  println(max)
  sc.stop()
}
