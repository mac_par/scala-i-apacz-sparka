package com.irfan.part03.task01

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{sum, udf}

object DayCallsForChurned2a extends App {
  val session = SparkSession.builder().master("local[*]").appName("cos").getOrCreate()

  import session.implicits._
  session.sparkContext.setLogLevel("ERROR")
  val inputDF = session.read.format("csv").option("inferSchema", value = true).option("header", value = true).option("delimiter",",")
    .load("bin/irfan/customer_churn.csv").toDF()
  val allowedStates = Set("OH", "NY")
  val statePred: String => Boolean = allowedStates.contains(_)
  val stateFilter = udf(statePred)
  val resultRows = inputDF.filter(stateFilter($"state")).filter($"churn".equalTo("FALSE")).select(sum($"total day calls").as("total_per_day"))
  resultRows.show()
}
