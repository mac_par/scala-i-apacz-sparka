package com.irfan.part03.task01

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

object NumberOfUniqueStates extends App {
  val sc = new SparkContext("local[*]", "costam")
  sc.setLogLevel("ERROR")
  val rows = sc.textFile("bin/irfan/customer_churn.csv")
  val header = rows.first()
  val result = rows.mapPartitions{partition =>
    partition.filter(line => line != header).map(line => line.split(",")(0))
  }.distinct().cache()

  result.foreach(println)
  println(result.count())
  val twoChars = result.filter(state => state.length != 2)
  println(twoChars.count() == 0)
  sc.stop()
}
