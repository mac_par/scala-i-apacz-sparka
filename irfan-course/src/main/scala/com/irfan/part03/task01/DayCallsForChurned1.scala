package com.irfan.part03.task01

import org.apache.spark.SparkContext
import org.apache.spark.broadcast.Broadcast

object DayCallsForChurned1 extends App {
  val sc = new SparkContext("local[*]", "cos")
  sc.setLogLevel("ERROR")
  val input = sc.textFile("bin/irfan/customer_churn.csv")
  val header = input.first()
  val value: Broadcast[String] = sc.broadcast(header)
  val set = sc.broadcast(Set("OH", "NY"))
  val rows = input.mapPartitions{partition =>
    partition.filter(line => line!= value.value).map{line =>
      val arr = line.split(",")

      //state -> (mins -> churned)
      arr(0)-> (arr(8).toInt->arr(20).toBoolean)
    }.filter(item => item._2._2).filter(item => set.value.contains(item._1)).map(item => item._1 -> item._2._1)
  }.cache()

  val calls = rows.values.reduce(_+_)
  println(calls)
  sc.stop()
}
