package com.irfan.part03.task01

import org.apache.spark.SparkContext

object HowMayCust1 extends App {
  val sc = new SparkContext("local[*]", "cos")
  sc.setLogLevel("ERROR")
  val inputRDD = sc.textFile("bin/irfan/customer_churn.csv")
  val header = sc.broadcast(inputRDD.first())
  val yes = sc.broadcast("yes")
  val results = inputRDD.mapPartitions { partition =>
    partition.filter(line => line != header.value).map { line =>
      val arr = line.split(",")
      arr(4)-> (arr(5) -> arr(20).toBoolean)
    }.filter(row => row._1 == yes.value && row._2._1 == yes.value).filter(item => item._2._2)
  }

  val result = results.count()
  println(result)
  sc.stop()
}
