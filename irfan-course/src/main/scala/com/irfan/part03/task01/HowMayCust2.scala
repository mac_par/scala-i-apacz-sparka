package com.irfan.part03.task01

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.udf

object HowMayCust2 extends App {
  val session = SparkSession.builder().master("local[*]").appName("cos").getOrCreate()

  import session.implicits._

  session.sparkContext.setLogLevel("ERROR")

  val inputDF = session.read.format("csv").option("inferSchema", value = true).option("header", value = true).option("delimiter", ",")
    .load("bin/irfan/customer_churn.csv").toDF()
  val function: String => Boolean = x => x == "yes"
  val pred = udf(function)
  val rows = inputDF.filter(pred($"international plan")).filter(pred($"voice mail plan")).cache()

  println(rows.count())
  rows.show()
}
