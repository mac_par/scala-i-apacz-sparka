package com.irfan.part03.task01

import org.apache.spark.SparkContext

object TotalNightMinutes1 extends App {
  val sc = new SparkContext("local[*]", "costam")
  sc.setLogLevel("ERROR")
  val initialData = sc.textFile("bin/irfan/customer_churn.csv")
  val header = initialData.first()
  val rows = initialData.mapPartitions { partition =>
    partition.filter(row => row != header).map(row => {
      val arr = row.split(",")
      arr(0) -> arr(13).toDouble
    })
  }.cache()

  val avgMinutes = rows.aggregateByKey(List.empty[Double])((list, mins) => list :+ mins, (l1, l2) => l1 ++ l2).mapValues(list => list.sum / list.size)

  avgMinutes.foreach(println)

  sc.stop()
}
