package com.irfan.part03

import org.apache.spark.sql.{Dataset, SparkSession}

object WebLogsSession extends App {
  val session = SparkSession.builder().appName("jakasApka").master("local[*]").getOrCreate()
  session.sparkContext.setLogLevel("ERROR")
  val inputFile: Dataset[String] = session.read.textFile("bin/irfan/access_log_Jul95_empty_lines.txt")
  val filteredLines = inputFile.na.drop(how = "all")
  filteredLines.write.format("text").save("bin/irfan/results/result.txt")
}
