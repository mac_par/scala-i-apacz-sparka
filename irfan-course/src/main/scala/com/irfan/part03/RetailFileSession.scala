package com.irfan.part03

import org.apache.spark.sql.SparkSession

object RetailFileSession extends App {
  val spark = SparkSession.builder().master("local[*]").appName("costam").getOrCreate()
  spark.sparkContext.setLogLevel("ERROR")
  val inputFile = spark.read.format("csv").option("header", value = true).option("delimiter", ",").option("inferSchema", value = true)
    .load("bin/irfan/retail.csv")
  inputFile.write.format("json").save("bin/irfan/results/result.json")
}
