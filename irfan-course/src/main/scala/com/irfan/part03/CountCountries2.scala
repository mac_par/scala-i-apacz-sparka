package com.irfan.part03

import org.apache.spark.sql.SparkSession

object CountCountries2 extends App {
  val session = SparkSession.builder().appName("costam").master("local[*]")
    .getOrCreate()
  session.sparkContext.setLogLevel("ERROR")
  import session.implicits._
  val initial = session.read.format("csv").option("delimiter", ",").option("header", value = true)
    .option("inferSchema", value = true).load("bin/irfan/retail.csv")
  val result = initial.select($"Country").distinct().orderBy($"Country".asc).cache()
  println(result.count())
  result.show(truncate = false)

}
