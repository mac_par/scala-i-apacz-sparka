package com.irfan.part03

import org.apache.spark.sql.SparkSession

object OnlySpain2 extends App {
  val spark = SparkSession.builder().master("local[*]").appName("cos").getOrCreate()
  spark.sparkContext.setLogLevel("ERROR")
  import spark.implicits._
  val df = spark.read.format("csv").option("inferSchema", value = true).option("header", value = true).load("bin/irfan/retail.csv")

  val result = df.filter($"Country".equalTo("Spain"))

  result.write.format("csv").option("header",value = true).option("delimiter",":").save("bin/irfan/results/dupa.csv")
}
