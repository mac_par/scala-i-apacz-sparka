package com.irfan.part03

import org.apache.spark.SparkContext

object OnlySpain1 extends App {
  val sc = new SparkContext("local[*]", "costam")
  sc.setLogLevel("ERROR")
  val inputFile = sc.textFile("bin/irfan/retail.csv")
  val value = sc.broadcast("Spain")

  val filteredResults = inputFile.mapPartitions{partition =>
    partition.filter(line => line.contains(value.value))
  }

  filteredResults.saveAsTextFile("bin/irfan/results/test.txt")
  sc.stop()
}
