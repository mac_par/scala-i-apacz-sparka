package com.irfan.part03

import org.apache.spark.SparkContext

object RetailWithoutHeader1 extends App {
  val sc = new SparkContext("local[*]", "jakasap")
  sc.setLogLevel("ERROR")
  sc.hadoopConfiguration.set("textinputformat.record.separator", ",")
  val inputFile = sc.textFile("bin/irfan/retail.csv")
  val header = inputFile.first()
  val lines = inputFile.mapPartitions{partition =>
    partition.filter(line => line != header)
  }

  lines.saveAsTextFile("bin/irfan/results/result-cvs.txt")
  sc.stop()
}
