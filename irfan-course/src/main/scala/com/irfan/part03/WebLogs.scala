package com.irfan.part03

import org.apache.spark.SparkContext

object WebLogs extends App {
  val sc = new SparkContext("local[*]", "jakiesapp")
  sc.setLogLevel("ERROR")
  val inputFile = sc.textFile("bin/irfan/access_log_Jul95_empty_lines.txt")

  val emptyLines = inputFile.filter(line => !line.isEmpty)
  emptyLines.saveAsTextFile("bin/irfan/results/logs.txt")
  sc.stop()
}
