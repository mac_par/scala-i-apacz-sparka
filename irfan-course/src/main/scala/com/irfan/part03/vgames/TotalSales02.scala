package com.irfan.part03.vgames

import org.apache.spark.sql.SparkSession

object TotalSales02 extends App {
  val session = SparkSession.builder().master("local[*]").appName("app").getOrCreate()
  import session.implicits._
  session.sparkContext.setLogLevel("ERROR")
  val inputDF = session.read.format("csv").option("inferSchema", value = true).option("header", value = true).option("delimiter",",")
    .load("bin/irfan/vgsales_cleaned.csv").toDF()
  val totalSalesDF = inputDF.groupBy($"Platform").sum("NA_Sales","EU_Sales","JP_Sales","Other_Sales","Global_Sales")
      .orderBy($"Platform")

  totalSalesDF.show()
}
