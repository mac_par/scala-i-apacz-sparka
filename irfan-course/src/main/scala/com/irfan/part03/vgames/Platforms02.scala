package com.irfan.part03.vgames

import org.apache.spark.sql.SparkSession

object Platforms02 extends App {
  val session = SparkSession.builder().master("local[*]").appName("cos").getOrCreate()

  session.sparkContext.setLogLevel("ERROR")
  val inputDF = session.read.format("csv").option("delimiter", ",").option("inferSchema", value = true).option("header", value = true)
    .load("bin/irfan/vgsales_cleaned.csv").toDF()
  inputDF.createTempView("Games")
  val gamesPerPlatforms = session.sql("select Platform, Year, Name, Genre, Publisher from Games")

  gamesPerPlatforms.show()
}
