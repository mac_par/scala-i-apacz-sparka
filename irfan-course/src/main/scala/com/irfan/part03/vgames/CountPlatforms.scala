package com.irfan.part03.vgames

import org.apache.spark.SparkContext

object CountPlatforms extends App {
  val sc = new SparkContext("local[*]", "cos")
  sc.setLogLevel("ERROR")
  val inputRdd = sc.textFile("bin/irfan/vgsales_cleaned.csv").cache()
  val header = inputRdd.first()
  val bc = sc.broadcast(header)

  val countMap = inputRdd.mapPartitions{partition =>
    partition.filter(line => line != bc.value).map{line =>
      var arr = line.split(",")
      arr(2) -> 1
    }
  }

  val platforms = countMap.reduceByKey(_+_).sortByKey()
  platforms.foreach(println)
  sc.stop()
}
