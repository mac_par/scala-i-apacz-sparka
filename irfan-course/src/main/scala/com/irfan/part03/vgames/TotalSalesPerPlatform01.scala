package com.irfan.part03.vgames

import org.apache.spark.SparkContext
import scala.math.max

object TotalSalesPerPlatform01 extends App {
  val sc = new SparkContext("local[*]", "cos")
  sc.setLogLevel("ERROR")
  val inputRDD = sc.textFile("bin/irfan/vgsales_cleaned.csv")
  val header = inputRDD.first()
  val bc = sc.broadcast(header)
  val mapped = inputRDD.mapPartitions{partition =>
    partition.filter(_ != bc.value).map{line =>
      val arr = line.split(",")
      arr(2) -> (arr(6).toDouble, arr(7).toDouble,arr(8).toDouble, arr(9).toDouble, arr(10).toDouble)
    }
  }.cache()

  //total sales per platforms
  val salesPlatforms = mapped.reduceByKey{(a,b) =>(a._1 + b._1, a._2 + b._2, a._3 + b._3, a._4 + b._4, a._5 + b._5)}.sortByKey()
  val maxSalesPerPlatform = mapped.reduceByKey{(a,b) =>(max(a._1, b._1), max(a._2, b._2), max(a._3, b._3), max(a._4, b._4), max(a._5, b._5))}.sortByKey()

  salesPlatforms.foreach(println)
  maxSalesPerPlatform.foreach(println)
  sc.stop()}
