package com.irfan.part03.vgames

import org.apache.spark.SparkContext

object TotalSales01 extends App {
  val sc = new SparkContext("local[*]", "local")
  sc.setLogLevel("OFF")
  val inputRdd = sc.textFile("bin/irfan/vgsales_cleaned.csv")
  val header = inputRdd.first()
  val bc = sc.broadcast(header)
  val mappedRdd = inputRdd.mapPartitions{partition =>
    partition.filter(line => line != bc.value).map{line =>
      val arr = line.split(",")
      arr(2) -> (arr(6).toDouble, arr(7).toDouble, arr(8).toDouble, arr(9).toDouble, arr(10).toDouble)
    }
  }

  val salesPerPlatform = mappedRdd.reduceByKey((x,y) => (x._1 + y._1, x._2 + y._2, x._3 + y._3, x._4 + y._4, x._5 + y._5))

  salesPerPlatform.foreach(println)
  sc.stop()
}
