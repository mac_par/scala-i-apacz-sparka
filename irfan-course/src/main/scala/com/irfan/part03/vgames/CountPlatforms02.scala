package com.irfan.part03.vgames

import org.apache.spark.sql.SparkSession

object CountPlatforms02 extends App {
  val session = SparkSession.builder().master("local[*]").appName("cos").getOrCreate()
  import session.implicits._
  session.sparkContext.setLogLevel("ERROR")
  val inputDF = session.read.format("csv").option("delimiter", ",").option("inferSchema", value = true).option("header", value = true)
    .load("bin/irfan/vgsales_cleaned.csv").toDF()

  val resultDF = inputDF.groupBy($"Platform").count().orderBy($"Platform")

  resultDF.show()
}
