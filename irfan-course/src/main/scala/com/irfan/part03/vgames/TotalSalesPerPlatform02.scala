package com.irfan.part03.vgames

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{sum, max}


object TotalSalesPerPlatform02 extends App {
  val session = SparkSession.builder().master("local[*]").appName("cos").getOrCreate()
  session.sparkContext.setLogLevel("ERROR")
  import session.implicits._
  val inputDF = session.read.format("csv").option("delimiter", ",").option("inferSchema", value = true).option("header", value = true)
    .load("bin/irfan/vgsales_cleaned.csv").toDF().cache()

  val totalSalesPerPlatformDF = inputDF.groupBy($"Platform").agg(sum($"NA_Sales"), sum($"EU_Sales"), sum($"JP_Sales"),sum($"Other_Sales"),sum($"Global_Sales"))
    .sort($"Platform".asc)

  val maxSalePerPlatform = inputDF.groupBy($"Platform").agg(max($"NA_Sales"), max($"EU_Sales"), max($"JP_Sales"),max($"Other_Sales"),max($"Global_Sales").as("Global_Sales"))
    .sort($"Global_Sales".desc)

  totalSalesPerPlatformDF.show()

  maxSalePerPlatform.show()
}
