package com.irfan.part03.vgames

import org.apache.spark.SparkContext

object Platforms01 extends App {
  val sc = new SparkContext("local[*]", "cos")
  sc.setLogLevel("ERROR")
  val inputRdd = sc.textFile("bin/irfan/vgsales_cleaned.csv").cache()
  val header = inputRdd.first()
  val bc = sc.broadcast(header)
  val partialResult = inputRdd.mapPartitions{partition =>
    partition.filter(line => line != bc.value).map{line =>
      val arr = line.split(",")
      arr(2) -> (arr(0), arr(1), arr(3), arr(4), arr(5))
    }
  }

  val gamesPerPlatform = partialResult.groupByKey().sortByKey()
  gamesPerPlatform.foreach(println)
  sc.stop()
}
