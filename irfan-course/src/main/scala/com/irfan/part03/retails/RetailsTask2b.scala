package com.irfan.part03.retails

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.count

object RetailsTask2b extends App {
  val session = SparkSession.builder().appName("shop2").master("local[*]").getOrCreate()
  import session.implicits._
  session.sparkContext.setLogLevel("ERROR")

  val ordersDF = session.read.format("csv").option("delimiter", ",").option("inferSchema", value = true).option("header", value = false)
    .load("bin/irfan/retail_db/orders/part-00000").toDF("order_id", "date", "order_customer_id", "status")

  val customersDF = session.read.format("csv").option("delimiter", ",").option("inferSchema", value = true).option("header", value = false)
    .load("bin/irfan/retail_db/customers/part-00000").toDF("customer_id", "name", "surname", "cos1", "cos2", "address", "city", "state", "post")

  val ordersByCustomersDF = ordersDF.join(customersDF, $"order_customer_id" === $"customer_id").filter($"status" === "CANCELED")
    .filter($"state" === "CA")
  val resultDF = ordersByCustomersDF.groupBy($"city").agg(count($"city").as("count")).sort($"count".desc).limit(5)

  resultDF.show()
}
