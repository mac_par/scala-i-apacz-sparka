package com.irfan.part03.retails

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

object RetailsTask2a extends App {
  val sc = new SparkContext("local[*]", "shop")
  sc.setLogLevel("ERROR")
  val ordersRDD = sc.textFile("bin/irfan/retail_db/orders/part-00000")
  val bcStatus = sc.broadcast("CANCELED")
  val ordersExtractRdd = ordersRDD.mapPartitions { partition =>
    partition.map { line =>
      val array = line.split(",")
      //customerId -> order status
      array(2).toInt -> array(3)
    }.filter { item =>
      item._2 == bcStatus.value
    }
  }

  val customersRDD = sc.textFile("bin/irfan/retail_db/customers/part-00000")
  val filteredCustomersRDD = customersRDD.mapPartitions{partition =>
    partition.map{line =>
      val array = line.split(",")
      //id -> city,state
      array(0).toInt -> (array(6), array(7))
    }
  }

  val ordersPerCustomerRdd: RDD[(String, Int)] = ordersExtractRdd.join(filteredCustomersRDD).mapPartitions{ partition =>
    partition.filter(line => line._2._2._2 == "CA")
      .map{line => line._2._2._1 -> 1
    }
  }.reduceByKey(_+_)

  val top5StatesRdd = ordersPerCustomerRdd.sortBy(_._2, ascending = false).take(5)

  top5StatesRdd.foreach(println)
  sc.stop()
}
