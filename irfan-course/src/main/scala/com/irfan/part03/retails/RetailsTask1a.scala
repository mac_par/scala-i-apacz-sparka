package com.irfan.part03.retails

import org.apache.spark.SparkContext

object RetailsTask1a extends App {
  val sc = new SparkContext("local[*]", "shop")
  sc.setLogLevel("ERROR")
  val ordersRDD = sc.textFile("bin/irfan/retail_db/orders/part-00000")
  val bcStatus = sc.broadcast("CANCELED")
  val ordersExtractRdd = ordersRDD.mapPartitions { partition =>
    partition.map { line =>
      val array = line.split(",")
      //customerId -> order status
      array(2).toInt -> array(3)
    }.filter { item =>
      item._2 == bcStatus.value
    }
  }

  val customersRDD = sc.textFile("bin/irfan/retail_db/customers/part-00000")
  val filteredCustomersRDD = customersRDD.mapPartitions{partition =>
    partition.map{line =>
      val array = line.split(",")
      //id -> state
      array(0).toInt -> array(7)
    }
  }

  val ordersPerCustomerRdd = ordersExtractRdd.join(filteredCustomersRDD).mapPartitions{partition =>
    partition.map{line =>
      line._2._2 -> 1
    }
  }.reduceByKey(_+_)

  val top5StatesRdd = ordersPerCustomerRdd.sortBy(_._2, ascending = false).take(5)

  top5StatesRdd.foreach(println)
  sc.stop()
}
