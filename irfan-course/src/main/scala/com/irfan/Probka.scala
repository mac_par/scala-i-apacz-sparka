package com.irfan

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Probka {
  def main(args: Array[String]): Unit = {
    val sparkConfig = new SparkConf().setAppName("moja app").setMaster("local[*]")
    val sc: SparkContext = new SparkContext(sparkConfig)
    sc.setLogLevel("ERROR")
    val value: RDD[(String, String)] = sc.wholeTextFiles("bin/rsvpdata").cache()
    value.foreach(println)
    value.mapPartitions{partition =>
      partition.map{_._2}
    }.foreach(println)

    sc.textFile("bin/rsvpdata/rsvp*.json", 5).foreach(println)
    sc.stop()
  }
}
