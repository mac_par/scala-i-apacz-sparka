#!/usr/bin/env sh
rm ./logdata
touch ./logdata
tail -f ./logdata | nc -l 7779 &
TAIL_NC_PID=$!
cat fake_logs/log1.log >> logdata
sleep 5
cat fake_logs/log2.log >> logdata
sleep 1
cat fake_logs/log1.log >> logdata
sleep 2
cat fake_logs/log1.log >> logdata
sleep 3
sleep 20
kill $TAIL_NC_PID
