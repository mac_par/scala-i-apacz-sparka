package com.udemy.part4

case class LogEntry(ip:String, client: String, user: String, dateTime: String, request: String, status: String, bytes: String, referer: String, agent: String)
