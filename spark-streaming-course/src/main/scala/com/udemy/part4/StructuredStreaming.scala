package com.udemy.part4

import java.text.SimpleDateFormat
import java.util.Locale
import java.util.regex.Pattern

import org.apache.spark.sql.functions.window
import org.apache.spark.sql.streaming.OutputMode
import org.apache.spark.sql.{Row, SparkSession}

object StructuredStreaming {
  def apacheLogPattern: Pattern = {
    val ddd = "\\d{1,3}"
    val ip = s"($ddd\\.$ddd\\.$ddd\\.$ddd)?"
    val client = "(\\S+)"
    val user = "(\\S+)"
    val dateTime = "(\\[.+?\\])"
    val request = "\"(.*?)\""
    val status = "(\\d{3})"
    val bytes = "(\\S+)"
    val referer = "\"(.*?)\""
    val agent = "\"(.*?)\""
    val regex = s"$ip $client $user $dateTime $request $status $bytes $referer $agent"
    Pattern.compile(regex)
  }

  lazy val logPattern = apacheLogPattern
  val datePattern = Pattern.compile("\\[(.*?) .+]")

  def parseDateField(field: String): Option[String] = {
    val dateMatcher = datePattern.matcher(field)
    if (dateMatcher.find()) {
      val dateString = dateMatcher.group(1)
      val dateFormat = new SimpleDateFormat("dd/MMM/yyyy:HH:mm:ss", Locale.ENGLISH)
      val date = dateFormat.parse(dateString)
      val timestamp = java.sql.Timestamp.from(date.toInstant)
      Some(timestamp.toString)
    } else {
      None
    }
  }

  def parseLog(row: Row): Option[LogEntry] = {
    val content = logPattern.matcher(row.getString(0))
    if (content.matches()) {
      Some(LogEntry(content.group(1),
        content.group(2),
        content.group(3),
        parseDateField(content.group(4)) getOrElse "",
        content.group(5),
        content.group(6),
        content.group(7),
        content.group(8),
        content.group(9)
      ))
    } else {
      None
    }
  }

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .master("local[*]")
      .appName("StructuredStreaming")
      .getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")

    import spark.implicits._
    val rawData = spark.readStream.text("data/logs")

    val dataSet = rawData.flatMap(parseLog).select("status", "dateTime")
    val windowed = dataSet.groupBy($"status", window($"dateTime", "1 hour").as("czas")).count().orderBy("czas")

    val query  = windowed.writeStream.outputMode(OutputMode.Complete()).format("console").start()

    query.awaitTermination
    spark.stop()
  }
}
