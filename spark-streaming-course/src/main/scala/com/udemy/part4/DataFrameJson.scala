package com.udemy.part4

import java.util.regex.Pattern

import org.apache.spark.sql.{SaveMode, SparkSession}
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.{Seconds, StreamingContext}

object DataFrameJson {
  def apacheLogPattern:Pattern = {
    val ddd = "\\d{1,3}"
    val ip = s"($ddd\\.$ddd\\.$ddd\\.$ddd)?"
    val client = "(\\S+)"
    val user = "(\\S+)"
    val dateTime = "(\\[.+?\\])"
    val request = "\"(.*?)\""
    val status = "(\\d{3})"
    val bytes = "(\\S+)"
    val referer = "\"(.*?)\""
    val agent = "\"(.*?)\""
    val regex = s"$ip $client $user $dateTime $request $status $bytes $referer $agent"
    Pattern.compile(regex)
  }

  def main(args: Array[String]): Unit = {
    val scc = new StreamingContext("local[*]","DataFrameTry",Seconds(1))
    scc.sparkContext.setLogLevel("ERROR")

    val pattern = apacheLogPattern

    val logs = scc.socketTextStream("127.0.0.1",9999, StorageLevel.MEMORY_AND_DISK_SER)

    val requests = logs.flatMap{log =>
      val matcher: java.util.regex.Matcher = pattern.matcher(log.toString)

      if (matcher.matches()) {
        val requestFields = matcher.group(5).split("\\s+")
        val url = util.Try(requestFields(1)) getOrElse "error"
        Some((url, matcher.group(6).toInt, matcher.group(9)))
      } else {
        None
      }
    }

    requests.foreachRDD{(rdd,time) =>
      val session = SparkSession.builder().config(rdd.sparkContext.getConf).getOrCreate()

      import session.implicits._

      val dataFrame = rdd.repartition(1).map{entry =>
        Record(entry._1, entry._2, entry._3)
      }.toDF()

      dataFrame.createOrReplaceTempView("requests")

      val result = session.sql("select agent, count(*) as total from requests group by agent")

      //save to results.json
      result.write.mode(SaveMode.Append).json("bin/results")
    }


//    scc.checkpoint("bin/tmp")
    scc.start()
    scc.awaitTermination()
  }
}

