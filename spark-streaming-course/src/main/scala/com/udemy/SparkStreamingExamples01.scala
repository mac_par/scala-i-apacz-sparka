package com.udemy

import java.nio.charset.{CodingErrorAction, StandardCharsets}

import org.apache.spark.SparkConf
import org.apache.spark.streaming.dstream.ReceiverInputDStream
import org.apache.spark.streaming.twitter.TwitterUtils
import org.apache.spark.streaming.{Seconds, StreamingContext}
import twitter4j.Status

import scala.io.{Codec, Source}

object SparkStreamingExamples01 {
  def setTwitterProperties = {
    val codec = Codec(StandardCharsets.UTF_8)
    codec.onMalformedInput(CodingErrorAction.REPLACE)
    codec.onUnmappableCharacter(CodingErrorAction.REPLACE)

    val source: Source = Source.fromFile("spark-streaming-course/dane/twitter.txt")(codec)
    for {line <- source.getLines()} {
      val fields = line.split("\\s+")
      System.setProperty(s"twitter4j.oauth.${fields(0)}", fields(1))
    }

    source.close()
  }

  def main(args: Array[String]): Unit = {
    setTwitterProperties
    val config = new SparkConf().setMaster("local[*]").setAppName("Tweets printer")
    val scc = new StreamingContext(config, Seconds(1))

    scc.sparkContext.setLogLevel("ERROR")

    val tweets: ReceiverInputDStream[Status] = TwitterUtils.createStream(scc, None)

    val statuses = tweets.map{tweet => tweet.getText}


    statuses.print()

    scc.start()
    scc.awaitTermination()
  }
}
