package com.udemy.part2

import java.nio.charset.{CodingErrorAction, StandardCharsets}

import com.mongodb.spark.MongoSpark
import com.mongodb.spark.config.WriteConfig
import org.apache.spark.sql.{SaveMode, SparkSession}
import org.apache.spark.streaming.twitter.TwitterUtils
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.iq80.leveldb.WriteOptions

import scala.io.{Codec, Source}

object SaveTweetsToMongo {
  def setTwitterAccount: Unit = {
    val codec: Codec = Codec(StandardCharsets.UTF_8)
    codec.onUnmappableCharacter(CodingErrorAction.REPLACE)
    codec.onMalformedInput(CodingErrorAction.REPLACE)

    val source:Source = Source.fromFile("/develop/Scala_Programming/spark-practice/spark-streaming-course/dane/twitter.txt")

    for {line <- source.getLines()} {
      val fields = line.split("\\s+")
      System.setProperty(s"twitter4j.oauth.${fields(0)}",fields(1))
    }
    source.close()
  }

  def main(args: Array[String]): Unit = {
    setTwitterAccount

    val scc = new StreamingContext("local[*]","saveTweets", Seconds(1))
    scc.sparkContext.setLogLevel("ERROR")

    val tweets = TwitterUtils.createStream(scc, None)

    val content = tweets.map(_.getText)

    var received: Long = 0

    val writeOverrides: scala.collection.mutable.Map[String, String] = scala.collection.mutable.Map[String, String]()
    writeOverrides.put("collection", "spark")
    writeOverrides.put("writeConcern.w", "majority")
    writeOverrides.put("spark.mongodb.input.uri", "mongodb://127.0.0.1:27017/tweets.taghreedaat")
    writeOverrides.put("spark.mongodb.output.uri", "mongodb://127.0.0.1:27017/tweets.taghreedaat")
    val writeConfig: WriteConfig = WriteConfig(writeOverrides)

    content.foreachRDD{(rdd,time)=>
      if (rdd.count() > 0) {
        val repartition = rdd.repartition(1).cache()
          val session = SparkSession.builder().config(scc.sparkContext.getConf)
          .config("spark.mongodb.input.uri", "mongodb://127.0.0.1:27017/tweets.taghreedaat")
          .config("spark.mongodb.output.uri", "mongodb://127.0.0.1:27017/tweets.taghreedaat")
              .getOrCreate()
        import session.implicits._
        val toSave = rdd.toDF("message")

        MongoSpark.save(toSave,writeConfig)
        println(s"Total tweets received: $received")
        if (received > 1000) {
          System.exit(0)
        }
      }
    }

    scc.start()
    scc.awaitTermination()
  }
}
