package com.udemy.part2

import java.nio.charset.{CodingErrorAction, StandardCharsets}
import java.util.concurrent.atomic.AtomicLong

import org.apache.spark.SparkContext
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.catalyst.expressions.Minute
import org.apache.spark.streaming.twitter.TwitterUtils
import org.apache.spark.streaming.{Minutes, Seconds, StreamingContext}

import scala.io.{Codec, Source}

object AvgTweetLength1 {
  def configLogger(sparkCtn: SparkContext): Unit = sparkCtn.setLogLevel("ERROR")

  def setTwitter: Unit = {
    val codec: Codec = Codec(StandardCharsets.UTF_8)
    codec.onMalformedInput(CodingErrorAction.REPLACE)
    codec.onUnmappableCharacter(CodingErrorAction.REPLACE)

    val source: Source = Source.fromFile("spark-streaming-course/dane/twitter.txt")

    for {line <- source.getLines()} {
      val fields = line.split("\\s+")
      System.setProperty(s"twitter4j.oauth.${fields(0)}", fields(1))
    }
    source.close()
  }

  def main(args: Array[String]): Unit = {
    setTwitter

    val scc = new StreamingContext("local[*]","AvgTweetLength1",Seconds(1))
    configLogger(scc.sparkContext)

    val tweetStream = TwitterUtils.createStream(scc, None)

    val windowed = tweetStream.window(Seconds(30),Seconds(10)).map(item => item.getText.length)


    windowed.foreachRDD{(rdd,time)=>
      val count = rdd.count()
      if (count > 0) {
        val total = rdd.reduce((x,y)=> x+y).intValue()

        println(s"Characters: $total, Tweets: $count. Avg: ${total/count}")
      }

    }
    scc.start()
    scc.awaitTermination()
  }
}
