package com.udemy.part2

import java.nio.charset.{CodingErrorAction, StandardCharsets}

import org.apache.spark.SparkContext
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.streaming.twitter.TwitterUtils
import org.apache.spark.streaming.{Seconds, StreamingContext}

import scala.io.{Codec, Source}

object AvgTweetLength2 {
  def configLogger(sparkCtn: SparkContext): Unit = sparkCtn.setLogLevel("ERROR")

  def setTwitter: Unit = {
    val codec: Codec = Codec(StandardCharsets.UTF_8)
    codec.onMalformedInput(CodingErrorAction.REPLACE)
    codec.onUnmappableCharacter(CodingErrorAction.REPLACE)

    val source: Source = Source.fromFile("spark-streaming-course/dane/twitter.txt")

    for {line <- source.getLines()} {
      val fields = line.split("\\s+")
      System.setProperty(s"twitter4j.oauth.${fields(0)}", fields(1))
    }
    source.close()
  }

  def main(args: Array[String]): Unit = {
    setTwitter

    val scc = new StreamingContext("local[*]","AvgTweetLength1",Seconds(1))
    configLogger(scc.sparkContext)

    val tweetStream = TwitterUtils.createStream(scc, None)

    val windowed = tweetStream.window(Seconds(30),Seconds(10)).map(item => item.getText.length)


    windowed.foreachRDD{(rdd,time)=>
      val session = SparkSession.builder().config(scc.sparkContext.getConf).getOrCreate()
      import session.implicits._

      val dataset = rdd.toDS
      dataset.groupBy().max().show()

    }
    scc.start()
    scc.awaitTermination()
  }
}
