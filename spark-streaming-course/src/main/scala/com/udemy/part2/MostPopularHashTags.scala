package com.udemy.part2

import java.nio.charset.{CodingErrorAction, StandardCharsets}

import org.apache.spark.SparkContext
import org.apache.spark.streaming.twitter.TwitterUtils
import org.apache.spark.streaming.{Seconds, StreamingContext}

import scala.io.{Codec, Source}

object MostPopularHashTags {
  def configLogger(sparkCtn: SparkContext): Unit = sparkCtn.setLogLevel("ERROR")

  def setTwitter: Unit = {
    val codec: Codec = Codec(StandardCharsets.UTF_8)
    codec.onMalformedInput(CodingErrorAction.REPLACE)
    codec.onUnmappableCharacter(CodingErrorAction.REPLACE)

    val source: Source = Source.fromFile("spark-streaming-course/dane/twitter.txt")

    for {line <- source.getLines()} {
      val fields = line.split("\\s+")
      System.setProperty(s"twitter4j.oauth.${fields(0)}", fields(1))
    }
    source.close()
  }

  def main(args: Array[String]): Unit = {
    setTwitter
    val scc = new StreamingContext("local[*]","PopularHashtags",Seconds(1))
    configLogger(scc.sparkContext)

    val tweetStream = TwitterUtils.createStream(scc, None)

    val tweets = tweetStream.map(_.getText).flatMap(_.split("\\s+")).filter(_.startsWith("#"))
    val hashTags = tweets.map(hashTag => (hashTag,1))

    val groupedHashTags = hashTags.reduceByKeyAndWindow(_+_,_-_,Seconds(10),Seconds(4))
    val popularHashTags = groupedHashTags.transform(rdd => rdd.sortBy(_._2,ascending = false))
    popularHashTags.print()

    scc.checkpoint("/tmp/checkpoint")
    scc.start()
    scc.awaitTermination()
  }
}
