package com.udemy.part2

import java.nio.charset.{CodingErrorAction, StandardCharsets}

import org.apache.spark.SparkContext
import org.apache.spark.streaming.twitter.TwitterUtils
import org.apache.spark.streaming.{Seconds, StreamingContext}

import scala.io.{Codec, Source}

object SaveTweetsToHDD {
  def setTwitterAccount: Unit = {
    val codec: Codec = Codec(StandardCharsets.UTF_8)
    codec.onUnmappableCharacter(CodingErrorAction.REPLACE)
    codec.onMalformedInput(CodingErrorAction.REPLACE)

    val source:Source = Source.fromFile("/develop/Scala_Programming/spark-practice/spark-streaming-course/dane/twitter.txt")

    for {line <- source.getLines()} {
      val fields = line.split("\\s+")
      System.setProperty(s"twitter4j.oauth.${fields(0)}",fields(1))
    }
    source.close()
  }

  def main(args: Array[String]): Unit = {
    setTwitterAccount

    val scc = new StreamingContext("local[*]","saveTweets", Seconds(1))
    scc.sparkContext.setLogLevel("ERROR")

    val tweets = TwitterUtils.createStream(scc, None)

    val content = tweets.map(_.getText)

    var received: Long = 0

    content.foreachRDD{(rdd,time)=>
      if (rdd.count() > 0) {
        val repartition = rdd.repartition(1).cache()
        repartition.saveAsTextFile(s"output/tweets_${time.milliseconds.toString}")
        received += repartition.count

        println(s"Total tweets received: $received")
        if (received > 1000) {
          System.exit(0)
        }
      }
    }

    scc.start()
    scc.awaitTermination()
  }
}
