package com.udemy.part3

import java.util.regex.Pattern

import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.{Seconds, StreamingContext}
import java.util.regex.Matcher

import org.apache.spark.streaming.dstream.DStream

object ApacheCatcher {
  def apacheLogPattern():Pattern = {
    val ddd = "\\d{1,3}"
    val ip = s"($ddd\\.$ddd\\.$ddd\\.$ddd)?"
    val client = "(\\S+)"
    val user = "(\\S+)"
    val dateTime = "(\\[.+?\\])"
    val request = "\"(.*?)\""
    val status = "(\\d{3})"
    val bytes = "(\\S+)"
    val referer = "\"(.*?)\""
    val agent = "\"(.*?)\""
    val regex = s"$ip $client $user $dateTime $request $status $bytes $referer $agent"
    Pattern.compile(regex)
  }

  def main (args:Array[String]): Unit = {
    val scc = new StreamingContext("local[*]","Apache logger",Seconds(1))
    scc.sparkContext.setLogLevel("ERROR")
    val pattern = apacheLogPattern()

    val listener = scc.socketTextStream("127.0.0.1",9999, StorageLevel.MEMORY_AND_DISK_SER)

    val requests = listener.map { line =>
        val matcher:Matcher = pattern.matcher(line)
        if (matcher.matches()) matcher.group(5)
    }

    val urls = requests.map{ request =>
      val values = request.toString.split("\\s+")
      if (values.length == 3) {
        values(1)
      } else {
        "error"
      }
    }


    val urlCounts: DStream[(String, Int)] = urls.filter{ url => !url.equals("error")}.map{ item => (item,1)}.reduceByKeyAndWindow(_+_,_-_,Seconds(300), Seconds(1))

    val sortedUrls = urlCounts.transform{rdd => rdd.sortBy(_._2,ascending = false)}

    sortedUrls.print()

    scc.checkpoint("bin/tmp")
    scc.start()
    scc.awaitTermination()
  }
}
