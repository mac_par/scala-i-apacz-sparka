package com.udemy.part3

import java.util.regex.Pattern
import java.util.regex.Matcher

import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.{Seconds, StreamingContext}

object ApacheCatcher3 {
  def apacheLogPattern():Pattern = {
    val ddd = "\\d{1,3}"
    val ip = s"($ddd\\.$ddd\\.$ddd\\.$ddd)?"
    val client = "(\\S+)"
    val user = "(\\S+)"
    val dateTime = "(\\[.+?\\])"
    val request = "\"(.*?)\""
    val status = "(\\d{3})"
    val bytes = "(\\S+)"
    val referer = "\"(.*?)\""
    val agent = "\"(.*?)\""
    val regex = s"$ip $client $user $dateTime $request $status $bytes $referer $agent"
    Pattern.compile(regex)
  }

  def main(args: Array[String]): Unit = {
    val scc = new StreamingContext("local[*]","ApacheCatcher3",Seconds(1))

    val logs = scc.socketTextStream("127.0.0.1",9999, StorageLevel.MEMORY_AND_DISK_SER)
    val pattern = apacheLogPattern()

    val agents = logs.mapPartitions{partition =>
      partition.map{log =>
        val matcher: Matcher = pattern.matcher(log.toString)

        if (matcher.matches()) matcher.group(9)
      }
    }

    val reducedAgents = agents.map{item => (item,1)}.reduceByKeyAndWindow(_+_,_-_, Seconds(300), Seconds(1))
    val result = reducedAgents.transform{rdd =>
      rdd.sortBy(_._2, ascending = false)
    }.print()

    scc.checkpoint("bin/tmp")
    scc.start()
    scc.awaitTermination()
  }
}
