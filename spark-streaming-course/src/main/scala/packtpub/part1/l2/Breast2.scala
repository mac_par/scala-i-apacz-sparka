package packtpub.part1.l2

import org.apache.spark.sql.SparkSession

object Breast2 {
  def binarize(i: Int): Int = i match {
    case 2 => 0
    case 4 => 1
    case 0 => 2
    case 1 => 3
    case _  => -1
  }

  case class CancerClass(sample: Long, cThick: Int, uCSize: Int, uCShape: Int, mAdhes: Int, sECSize:Int, bNuc:Int, bChrom:Int, nNuc: Int, mitosis: Int, clas:Int)
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().appName("Brast2").master("local[*]").getOrCreate()

    spark.sparkContext.setLogLevel("ERROR")
    import spark.implicits._
    val table = spark.sparkContext.textFile("bin/breast/breast-cancer-wisconsin.data").map{line =>
      val array = line.split(",")
      CancerClass(array(0).toLong, array(1).toInt, array(2).toInt, array(3).toInt, array(4).toInt, array(5).toInt, array(6).toInt, array(7).toInt, array(8).toInt,array(9).toInt,
        array(10).toInt)
    }.toDF()

    table.createOrReplaceTempView("cancerTable")

    spark.udf.register("binarizeIt", (arg: Int) => binarize(arg))
    val sqlUdf = spark.sql("select *, binarizeIt(clas) as ord from cancerTable")
    sqlUdf.show()

    println(spark.catalog.isCached("cancerTable"))
    spark.catalog.cacheTable("cancerTable")
    spark.catalog.listDatabases().show()
    spark.catalog.listTables().show()
    println(spark.catalog.isCached("cancerTable"))
    spark.catalog.clearCache()
    println(spark.catalog.isCached("cancerTable"))
    spark.catalog.listDatabases().show()
    spark.catalog.listTables().show()


  }
}
