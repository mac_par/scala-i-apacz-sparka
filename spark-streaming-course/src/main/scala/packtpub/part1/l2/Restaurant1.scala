package packtpub.part1.l2

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.udf

object Restaurant1 {
  case class RestClass(name: String, street: String, city: String, phone: String, cuisine: String)
  def normalizePhone(nbr: String):String = nbr match {
    case nbr if nbr.contains("/") => nbr.replaceAll("/","-").replaceAll("--","-")
    case nbr => nbr
  }

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().master("local[*]").appName("Restaurant1").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")

    import spark.implicits._


    val restaurant1 = spark.read.format("csv").option("header", "true")
      .load("/develop/Scala_Programming/spark-practice/bin/restaurant/fz.csv").as[RestClass]
    spark.udf.register("normalizePh", (item: String) => normalizePhone(item))

    val udfPhone = udf[String,String](normalizePhone)
    val restaurantStd = restaurant1.withColumn("stdPhone", udfPhone(restaurant1.col("phone")))
    restaurantStd.createOrReplaceTempView("restaurants")

    val results = spark.sql("select name, street, city, cuisine, stdPhone from restaurants order by name asc")

    results.show()

    val count = spark.sql("select count(*) as total from restaurants")
    count.show()
  }
}
