package packtpub.part1.l2

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{DataType, StructType}

object Breast1 {
  def main(args: Array[String]): Unit = {
    val schema = new StructType().add("sample", "long").add("cThick", "integer")
      .add("uCSize", "integer").add("uCShape", "integer")
      .add("mAdhes", "integer").add("sECSize", "integer").add("bNuc", "integer")
      .add("bChrom", "integer").add("nNuc", "integer").add("mitosis", "integer")
      .add("clas", "integer")

    val spark = SparkSession.builder().master("local[*]").appName("Breast1").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")

    val df = spark.read.format("csv").option("header", false).schema(schema)
      .load("bin/breast/breast-cancer-wisconsin.data")

    df.show()
    df.createOrReplaceTempView("cancerTable")
    val resultTB = spark.sql("select * from cancerTable")
    resultTB.show()

    val resultTB2 = spark.sql("select sample, bNuc from cancerTable")
    resultTB2.show()
  }
}
