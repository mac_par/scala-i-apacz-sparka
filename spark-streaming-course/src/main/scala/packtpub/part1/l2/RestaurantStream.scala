package packtpub.part1.l2

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.streaming.{OutputMode, Trigger}
import org.apache.spark.sql.types.{StringType, StructType}

object RestaurantStream {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().appName("Restaurant Stream").master("local[*]").getOrCreate()
    import spark.implicits._
    spark.sparkContext.setLogLevel("ERROR")

    val schema = new StructType().add("name", StringType).add("street", StringType).add("city", StringType).add("phone", StringType).add("cuisine", StringType)
    val stream = spark.readStream.format("csv").schema(schema).option("header", value = true).load("bin/restaurant")
    val byCity = stream.groupBy($"city").count()
    val consoleWriter = byCity.writeStream.format("console")
      .trigger(Trigger.ProcessingTime("20 seconds"))
      .queryName("counts")
      .outputMode(OutputMode.Complete)
      .start()

    spark.streams.active.foreach(println)
    spark.streams.active(0).explain()
    consoleWriter.awaitTermination()
    spark.close()
  }
}
