package packtpub.part1.l2

import org.apache.spark.sql.SparkSession

object Restaurant2 {
  def benchmark(name: String)(f: => Unit) {
    val init = System.nanoTime()
    f
    val end = System.nanoTime()
    println(s"Operation $name took ${end - init} ns")
  }

  def main(args: Array[String]): Unit = {
   val spark = SparkSession.builder().appName("Restaurant2").master("local[*]").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    import spark.implicits._

    val rest1 = spark.read.format("csv").option("header","true").load("bin/restaurant/fz.csv")
    val rest2 = spark.read.format("csv").option("header","true").load("bin/restaurant/fz-nophone.csv")


    //spark sql 1.6
    spark.conf.set("spark.sql.codegen.wholeStage","false")
    benchmark("Spark 1.6"){
      rest1.join(rest2,"name").count()
    }

    spark.conf.set("spark.sql.codegen.wholeStage", "true")
    benchmark("Spark 2.2") {
      rest1.join(rest2,"name").count()
    }

    rest1.join(rest2, "name").selectExpr("count(*)").explain(true)
    rest1.join(rest2, "name").selectExpr("count(*)").explain()
  }
}
