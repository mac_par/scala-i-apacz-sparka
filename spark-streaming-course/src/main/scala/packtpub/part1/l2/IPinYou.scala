package packtpub.part1.l2

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{IntegerType, StringType, StructType}

object IPinYou {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().appName("IPinYou").master("local[*]").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    val bidSchema = new StructType().add("bidid", StringType).add("timestamp", StringType).add("ipinyouid", StringType).add("useragent", StringType).add("IP", StringType).add("region", IntegerType).add("city", IntegerType).add("adexchange", StringType).add("domain", StringType).add("url:String", StringType).add("urlid: String", StringType).add("slotid: String", StringType).add("slotwidth", StringType).add("slotheight", StringType).add("slotvisibility", StringType).add("slotformat", StringType).add("slotprice", StringType).add("creative", StringType).add("bidprice", StringType)

    import spark.implicits._
    val streaming = spark.readStream.format("csv").schema(bidSchema)
      .option("header",value = false)
      .option("inferSchema",value = true)
      .option("sep","\t")

  }
}
