package packtpub.part1

import org.apache.spark.sql.SparkSession

object Probka1 {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().appName("cos tam").master("local[*]").getOrCreate()
    import spark.implicits._
    val dana = spark.read.json("bin/proby/colors.json")
    val colors = dana.as[Color]
    colors.show()

    colors.createOrReplaceTempView("kolorki")
    val result = spark.sql("select count(*) as kolors, transparency from kolorki group by transparency")

    result.show()

    spark.stop()
  }

  case class Color(name: String, value: BigInt, transparency: Double)
}
