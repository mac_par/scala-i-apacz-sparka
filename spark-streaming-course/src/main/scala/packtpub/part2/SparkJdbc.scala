package packtpub.part2

import java.util.Properties

import org.apache.spark.sql.{SaveMode, SparkSession}
import org.apache.spark.sql.functions.unix_timestamp

object SparkJdbc {
  def main(args: Array[String]) : Unit =  {
    val jdbcUrl = "jdbc:oracle:thin:@localhost.localdomain:1521/XEPDB1"
    val tableName = "transactions"
    val propeties = new Properties()
    propeties.put("user","maciek")
    propeties.put("password","Zaq12wsxcde3#")

    val spark = SparkSession.builder().master("local[*]").appName("Spark mit Jdbc").getOrCreate()
    import spark.implicits._
    spark.sparkContext.setLogLevel("ERROR")
    val retailDS = spark.read.format("csv").option("header",value = true).option("separator",",")
      .load("/develop/Scala_Programming/spark-practice/bin/retail/Online Retail.csv")

    val ts = unix_timestamp($"InvoiceDate","dd/MM/yy HH:mm").cast("timestamp")
    val retailDSWithTs = retailDS.withColumn("ts", ts)

    val requiredTs = "2011-12-01"

    retailDSWithTs.createOrReplaceTempView("retailTable")
    val before = spark.sql(s"select * from retailTable where ts <'$requiredTs'")
    val after = spark.sql(s"select * from retailTable where ts >='$requiredTs'")

    val writeData = after.select("InvoiceNo","StockCode","Description","Quantity","UnitPrice","CustomerID","Country","ts")
      .withColumnRenamed("ts","InvoiceDate")

    //write data into database
    writeData.write.mode(SaveMode.Append).jdbc(jdbcUrl, tableName, propeties)


    val writeDataJson = before.select("InvoiceNo","StockCode","Description","Quantity","UnitPrice","CustomerID","Country","ts").withColumnRenamed("ts","InvoiceDate")
    writeDataJson.write.format("json").mode(SaveMode.Append).save("bin/tmp")

    spark.close()
  }
}
