package packtpub.part2

import com.mongodb.spark.config.WriteConfig
import org.apache.spark.sql.{SaveMode, SparkSession}

object Electronics {
  def main(args: Array[String]): Unit = {
    val writingConf = WriteConfig(Map("uri"->"mongodb://127.0.0.1:27017/amz.reviews"))

    val spark = SparkSession.builder().master("local[*]").appName("SparkMitMongo").getOrCreate()
    import spark.implicits._
    //pobranie i zapis do MongoDB
    val amzData = spark.read.json("bin/amazon/reviews_Electronics_5.json")

    amzData.createOrReplaceTempView("reviews")

    val selectionDF = spark.sql("select asin, overall, reviewTime, reviewerID, reviewerName, helpful from reviews where overall >= 3")
    selectionDF.show()

    val selectedElementsDF = amzData.select($"asin", $"overall",$"helpful").where($"helpful".getItem(0) < 3).cache()

    selectedElementsDF.write.format("mongo").option("spark.mongodb.output.uri","mongodb://127.0.0.1:27017/amz.reviews").mode(SaveMode.Append).save()
    //Pobranie z MongoDB

    val inputDF = spark.read.format("mongo").option("spark.mongodb.input.uri","mongodb://127.0.0.1:27017/amz.reviews").load()
    inputDF.show()
    spark.close()
  }
}
