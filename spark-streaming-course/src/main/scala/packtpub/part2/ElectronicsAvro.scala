package packtpub.part2

import org.apache.spark.sql.SparkSession

object ElectronicsAvro {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().appName("AvroApp").master("local[*]").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    val reviews = spark.read.json("bin/amazon/reviews_Electronics_5.json")
    //overall <3, przetranszowanie na jedną partycje is zapis do pliku rev.avro
    reviews.filter("overall < 3").coalesce(1).write.format("avro").save("bin/tmp/rev.avro")

    val readit = spark.read.format("avro").load("bin/tmp/rev.avro")
    readit.cache()
    readit.printSchema()
    readit.show()
    spark.close()
  }
}
