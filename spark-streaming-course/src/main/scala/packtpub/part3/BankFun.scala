package packtpub.part3

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{DataTypes, StructField, StructType}
import org.apache.spark.sql.expressions.scalalang.typed.{avg}
import org.apache.spark.sql.functions.round

object BankFun {
  def getSchema: StructType= {
    val age = StructField("age", DataTypes.IntegerType)
    val job  = StructField("job", DataTypes.StringType)
    val marital  = StructField("marital", DataTypes.StringType)
    val edu  = StructField("education", DataTypes.StringType)
    val credit_default  = StructField("credit_default", DataTypes.StringType)
    val housing  = StructField("housing", DataTypes.StringType)
    val loan  = StructField("loan", DataTypes.StringType)
    val contact  = StructField("contact", DataTypes.StringType)
    val month  = StructField("month", DataTypes.StringType)
    val day  = StructField("day", DataTypes.StringType)
    val dur  = StructField("duration", DataTypes.DoubleType)
    val campaign  = StructField("campaign", DataTypes.DoubleType)
    val pdays  = StructField("pdays", DataTypes.DoubleType)
    val prev  = StructField("previous", DataTypes.DoubleType)
    val pout  = StructField("pout", DataTypes.StringType)
    val emp_var_rate  = StructField("emp_var_rate", DataTypes.DoubleType)
    val cons_price_idx  = StructField("cons_price_idx", DataTypes.DoubleType)
    val cons_conf_idx  = StructField("cons_conf_idx", DataTypes.DoubleType)
    val euribor3m  = StructField("euribor3m", DataTypes.DoubleType)
    val nr_employed  = StructField("nr_employed", DataTypes.DoubleType)
    val deposit  = StructField("y", DataTypes.StringType)

    val fields = Array(age, job, marital, edu, credit_default, housing, loan, contact, month, day, dur, campaign, pdays, prev, pout, emp_var_rate, cons_price_idx, cons_conf_idx, euribor3m, nr_employed, deposit)
    StructType(fields)
  }
  case class Call(age: Int, job: String, marital: String, education: String, credit_default: String, housing: String, loan: String, contact: String, month: String, day: String, duration: Double, campaign: Double, pdays: Double, previous: Double, pout: String, emp_var_rate: Double, cons_price_idx: Double, cons_conf_idx: Double, euribor3m: Double, nr_employed: Double, y: String)

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().master("local[*]").appName("Bank data").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    import spark.implicits._
    val bankDetails = spark.read.format("csv")
      .option("header",value = true)
      .option("delimiter",";")
      .load("bin/bank-additional/bank-additional-full.csv").cache()

    bankDetails.printSchema()
    val callStats = bankDetails.select($"age", $"duration", $"campaign", $"previous", $"y")
      .as[Call]
      .cache()
    callStats.show()
    //describes dataset
    callStats.describe().show()

    bankDetails.stat.crosstab("age","marital").orderBy("age_marital").show(10,truncate = false)

    val req = bankDetails.stat.freqItems(Seq("education"), 0.3)

    println(req.collect()(0))

    //nie pojdzie bo string
//    println(s"Quantiles: ${callStats.stat.approxQuantile("age", Array(0.25, 0.5, 0.75), 0.0)}")

    callStats.groupByKey(callStat => callStat.y).agg(avg[Call](_.age).name("A"), avg[Call](_.campaign).name("B"),
      avg[Call](_.duration).name("C"), avg[Call](_.previous).name("D")).withColumnRenamed("value","E")
        .select($"E".name("TDSubscribed?"), $"A".name("Total Customers"), round($"B",2).name("Avg Calls(curr)"),
          round($"C",2).name("Avg Call(duration)"), round($"D",2).name("Avg Call(prev)")).show()

    spark.close()
  }
}
