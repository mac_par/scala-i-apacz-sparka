package packtpub.part3

import org.apache.spark.sql.SparkSession

object Sampling {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().appName("cos tam").master("local[*]").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    val fraction = Map("unknown" -> .1, "single"-> .15, "married"-> 0.5, "divorced"-> 0.25)
    import spark.implicits._
    val dataDF = spark.read.format("csv").option("sep",";").option("header",value = true).load("bin/bank-additional/bank-additional-full_cp.csv")
    dataDF.select($"age", $"duration", $"campaign", $"previous", $"y")
    dataDF.createOrReplaceTempView("calls")
    val stratifiedData = dataDF.stat.sampleBy("marital", fraction, 36L).cache()

    println(s"Count: ${stratifiedData.count()}")
    val sampleWithReplacement = stratifiedData.sample(withReplacement = true, 0.1)
    sampleWithReplacement.groupBy($"marital").count().orderBy("marital").show(truncate = false)

    spark.close()
  }
}
