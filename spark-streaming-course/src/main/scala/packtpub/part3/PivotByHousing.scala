package packtpub.part3

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{count, sum, avg, round}

object PivotByHousing {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().appName("Pivoting by housing").master("local[*]").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    import spark.implicits._
    val callsDF = spark.read.format("csv").option("header", value = true).option("sep", ";").load("bin/bank-additional/bank-additional-full_cp.csv").cache()
    callsDF.groupBy("marital").pivot("housing").agg(count("housing")).sort("marital").show(false)

    val drugiePodejscie = callsDF.groupBy("job").pivot("marital", Seq("unknown", "single", "married", "divorced"))
      .agg(round(sum("campaign"), 2), round(avg("campaign"), 2))
      .sort("job")
      .toDF("job", "un-sum", "un-avg", "s-sum", "s-avg", "m-sum", "m-avg", "d-sum", "d-avg")
    drugiePodejscie.show(false)

    //ile średnio rozmawial rozwodnik lub osoba zamężna wg zatrudnienia
    val kolejne  = callsDF.groupBy("job").pivot("marital", Seq("divorced","married"))
      .agg(round(avg("duration"),2)).sort("job")/*.toDF("Zatrudnienie", "Średnio Rozwodnik", "Średnio Zamężny/a")*/

    kolejne.show(false)

    val kolejne2 = callsDF.groupBy("job", "housing").pivot("marital", Seq("divorced","married"))
      .agg(round(avg("duration"),2)).sort("job")
    kolejne2.show(false)

    //hipoteka per miesiac
    val months = Seq("jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec")

    val hipoteken = callsDF.groupBy("y").pivot("month", months).agg(count("y")).sort("y")
    //pustaki wypelnione zerami
        .na.fill(0)
    hipoteken.show(false)

    spark.close()
  }
}
