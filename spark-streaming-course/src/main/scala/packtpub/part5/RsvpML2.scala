package packtpub.part5

import org.apache.spark.SparkConf
import org.apache.spark.ml.PipelineModel
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.from_json
import org.apache.spark.sql.streaming.Trigger
import org.apache.spark.sql.types.DataTypes.{DoubleType, LongType, StringType}
import org.apache.spark.sql.types.{DataTypes, StructType}

object RsvpML2 {
  val schema = new StructType()
    .add("event",
      new StructType()
        .add("event_id", StringType, true)
        .add("event_name", StringType, true)
        .add("event_url", StringType, true)
        .add("time", LongType, true))
    .add("group",
      new StructType()
        .add("group_city", StringType, true)
        .add("group_country", StringType, true)
        .add("group_id", LongType, true)
        .add("group_lat", DoubleType, true)
        .add("group_lon", DoubleType, true)
        .add("group_name", StringType, true)
        .add("group_state", StringType, true)
        .add("group_topics", DataTypes.createArrayType(
          new StructType()
            .add("topicName", StringType, true)
            .add("urlkey", StringType, true)), true)
        .add("group_urlname", StringType, true))
    .add("guests", LongType, true)
    .add("member",
      new StructType()
        .add("member_id", LongType, true)
        .add("member_name", StringType, true)
        .add("photo", StringType, true))
    .add("mtime", LongType, true)
    .add("response", StringType, true)
    .add("rsvp_id", LongType, true)
    .add("venue",
      new StructType()
        .add("lat", DoubleType, true)
        .add("lon", DoubleType, true)
        .add("venue_id", LongType, true)
        .add("venue_name", StringType, true))
    .add("visibility", StringType, true)

  def main(args: Array[String]): Unit = {
    val config = new SparkConf().setMaster("local[*]").setAppName("Rsvp ML2")
    val spark = SparkSession.builder().config(config).getOrCreate()
    import spark.implicits._

    val pipeline = PipelineModel.load("output/logistic")

    val kafkaDF = spark.readStream.format("kafka")
      .option("kafka.bootstrap.servers", "127.0.0.1:9092")
      .option("subscribe", "meetupTopic")
      .option("auto.offset.reset", "earliest")
      .load()

    val rsvpDF = kafkaDF.select(from_json($"value".cast("string"), schema).alias("rsvp"))
      .filter(row => !row.anyNull)

    val selectedDF = rsvpDF.select($"rsvp.group.group_city", $"rsvp.group.group_lat", $"rsvp.group.group_lon", $"rsvp.response")

    selectedDF.printSchema()

    val transformedDF = pipeline.transform(selectedDF)

    //save json

    val handler = transformedDF.writeStream.format("json")
      .option("path", "output/ml-data")
      .option("truncate", value = false)
      .option("checkpointLocation", "output/checkpoint")
      .trigger(Trigger.ProcessingTime("30 seconds"))
      .start()

    handler.awaitTermination()
  }
}
