package packtpub.part5

import org.apache.spark.SparkConf
import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.classification.LogisticRegression
import org.apache.spark.ml.evaluation.BinaryClassificationEvaluator
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.DataTypes._
import org.apache.spark.sql.types.{DataTypes, StructType}
import org.apache.spark.sql.functions.when

object RsvpML1 {
  val schema = new StructType()
    .add("event",
      new StructType()
        .add("event_id", StringType, true)
        .add("event_name", StringType, true)
        .add("event_url", StringType, true)
        .add("time", LongType, true))
    .add("group",
      new StructType()
        .add("group_city", StringType, true)
        .add("group_country", StringType, true)
        .add("group_id", LongType, true)
        .add("group_lat", DoubleType, true)
        .add("group_lon", DoubleType, true)
        .add("group_name", StringType, true)
        .add("group_state", StringType, true)
        .add("group_topics", DataTypes.createArrayType(
          new StructType()
            .add("topicName", StringType, true)
            .add("urlkey", StringType, true)), true)
        .add("group_urlname", StringType, true))
    .add("guests", LongType, true)
    .add("member",
      new StructType()
        .add("member_id", LongType, true)
        .add("member_name", StringType, true)
        .add("photo", StringType, true))
    .add("mtime", LongType, true)
    .add("response", StringType, true)
    .add("rsvp_id", LongType, true)
    .add("venue",
      new StructType()
        .add("lat", DoubleType, true)
        .add("lon", DoubleType, true)
        .add("venue_id", LongType, true)
        .add("venue_name", StringType, true))
    .add("visibility", StringType, true);

  def main(args: Array[String]): Unit = {
    val config = new SparkConf().setAppName("RsvpML 1").setMaster("local[*]")

    val spark = SparkSession.builder().config(config).getOrCreate()
    import spark.implicits._
    val initialData = spark.read.format("json").schema(schema).load("bin/rsvpdata")

    val locationsDF = initialData.select($"group.group_city", $"group.group_lat", $"group.group_lon", $"response")
    val distinctDF = locationsDF.filter(row => !row.anyNull)

    val binarizedDF = distinctDF.withColumn("response", when($"response" === "yes", "1").otherwise("0"))

    val finalDF = binarizedDF.withColumn("responseNum", $"response".cast("double")).drop("response")
      .withColumnRenamed("responseNum", "label")

    val vectorAssembler = new VectorAssembler().setInputCols(Array("group_lat", "group_lon")).setOutputCol("features")

    //model
    val model = new LogisticRegression().setMaxIter(10)
      .setRegParam(0.00001)
      .setElasticNetParam(0.1)
      .setThreshold(0.1)

    val initialPipeline = new Pipeline().setStages(Array(vectorAssembler, model))

    val splitedDF = finalDF.randomSplit(Array(.8, .2))

    val midPipeline = initialPipeline.fit(splitedDF(0))
    val predictedDF = midPipeline.transform(splitedDF(1))

    predictedDF.show(truncate = false)

    val binarizer = new BinaryClassificationEvaluator()

    val metricName = binarizer.metricName
    val largerBetter = binarizer.isLargerBetter
    val evaluatedValue = binarizer.evaluate(predictedDF)

    println(s"Binary Classification Evaluator: Metric name:$metricName nIs larger better?: $largerBetter Value: + $evaluatedValue")

    midPipeline.save("output/logistic")
  }
}
