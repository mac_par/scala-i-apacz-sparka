package packtpub.part5

import com.google.gson.{JsonObject, JsonParser}
import org.apache.kafka.clients.consumer.{ConsumerConfig, ConsumerRecord}
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.SparkConf
import org.apache.spark.mllib.classification.StreamingLogisticRegressionWithSGD
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.streaming.dstream.InputDStream
import org.apache.spark.streaming.kafka010.{ConsumerStrategies, HasOffsetRanges, KafkaUtils, LocationStrategies, OffsetRange}
import org.apache.spark.streaming.{Seconds, StreamingContext}

object RsvpMLRDD1 {
  def prepareLabeledPoints(str: String): LabeledPoint = {
    val jsonObject: JsonObject = new JsonParser().parse(str).getAsJsonObject
    val response: Double = extractResponse(jsonObject)
    val groupObject = jsonObject.get("group").getAsJsonObject
    val groupLat = extractGroupCoords(groupObject, "group_lat")
    val groupLon = extractGroupCoords(groupObject, "group_lon")

    LabeledPoint.parse(s"($response,[$groupLat,$groupLon])")
  }

  def extractResponse(json: JsonObject): Double = {
    val value = json.get("response").getAsString
    if ("yes".equals(value)) 1.0 else 0.0
  }

  def extractGroupCoords(json:JsonObject, label: String): Double = {
    json.get(label).getAsDouble
  }

  lazy val kafkaParams: Map[String,Object] = {
    val map = scala.collection.mutable.HashMap(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG -> "127.0.0.1:9092"
    , (ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, classOf[StringDeserializer])
    , (ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, classOf[StringDeserializer])
    , (ConsumerConfig.GROUP_ID_CONFIG, "meetup-consumer")
    , (ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest")
    , (ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false: java.lang.Boolean)
    )

    map.toMap
  }

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("Rsvp with ML on RDD").setMaster("local[*]")

    val sc = new StreamingContext(conf, Seconds(5))

    val topics = Seq("meetupTopic")
    val kafkaDStream: InputDStream[ConsumerRecord[String, String]] = KafkaUtils.createDirectStream(sc, LocationStrategies.PreferConsistent,
      ConsumerStrategies.Subscribe[String,String](topics, kafkaParams))

    val trainData = kafkaDStream.mapPartitions{part =>
      part.map(_.value()).map(prepareLabeledPoints)
    }

    trainData.print()
    //Streaming logistic regression instance
    val streamingLogisticRegressionWithSGD = new StreamingLogisticRegressionWithSGD().setInitialWeights(Vectors.zeros(2))

    //train the data
    streamingLogisticRegressionWithSGD.trainOn(trainData)

    val values = trainData.map{item => (item.label, item.features)}

    //predict on passed data
    streamingLogisticRegressionWithSGD.predictOnValues(values)


    sc.awaitTermination()
    sc.stop()
  }
}
