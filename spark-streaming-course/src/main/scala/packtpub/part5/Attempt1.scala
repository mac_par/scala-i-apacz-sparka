package packtpub.part5

import java.util.logging.{Level, Logger}

import org.apache.spark.sql.{DataFrame, Encoders, SparkSession}
import org.apache.spark.sql.functions.{from_json, window}
import org.apache.spark.sql.streaming.OutputMode
import org.apache.spark.sql.types.{DataTypes, DoubleType, LongType, StringType, StructType}

object Attempt1 {
  val RSVP_SCHEMA = new StructType()
    .add("venue",
      new StructType()
        .add("venue_name", StringType, true)
        .add("lon", DoubleType, true)
        .add("lat", DoubleType, true)
        .add("venue_id", LongType, true))
    .add("visibility", StringType, true)
    .add("response", StringType, true)
    .add("guests", LongType, true)
    .add("member",
      new StructType()
        .add("member_id", LongType, true)
        .add("photo", StringType, true)
        .add("member_name", StringType, true))
    .add("rsvp_id", LongType, true)
    .add("mtime", LongType, true)
    .add("event",
      new StructType()
        .add("event_name", StringType, true)
        .add("event_id", StringType, true)
        .add("time", LongType, true)
        .add("event_url", StringType, true))
    .add("group",
      new StructType()
        .add("group_city", StringType, true)
        .add("group_country", StringType, true)
        .add("group_id", LongType, true)
        .add("group_lat", DoubleType, true)
        .add("group_long", DoubleType, true)
        .add("group_name", StringType, true)
        .add("group_state", StringType, true)
        .add("group_topics", DataTypes.createArrayType(
          new StructType()
            .add("topicName", StringType, true)
            .add("urlkey", StringType, true)), true)
        .add("group_urlname", StringType, true))

  def main(args: Array[String]) :Unit = {
    val spark = SparkSession.builder().master("local[*]").appName("Attempt 1").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    import spark.implicits._

    val kafkaDS = spark.readStream.format("kafka")
      .option("kafka.bootstrap.servers","127.0.0.1:9092")
      .option("subscribe", "meetupTopic")
      .option("auto.offset.reset", "earliest")
      .load()

    val valuesDS = kafkaDS.select($"timestamp", from_json($"value".cast("string"), RSVP_SCHEMA).alias("rsvp"))
    val counted = valuesDS.withWatermark("timestamp", "2 seconds")
      .groupBy(window($"timestamp", "5 seconds", "2 seconds"), $"rsvp.guests").count()
    val data = counted.writeStream.format("console")
      .option("truncate", "false")
      .start()
    data.awaitTermination()
    data.stop()
  }
}
