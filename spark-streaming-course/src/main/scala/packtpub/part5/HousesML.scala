package packtpub.part5

import org.apache.spark.SparkConf
import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.evaluation.RegressionEvaluator
import org.apache.spark.ml.feature.{Imputer, VectorAssembler}
import org.apache.spark.ml.regression.LinearRegression
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.apache.spark.sql.types.{DataTypes, StructType}
import org.apache.spark.sql.functions.col


object HousesML {
  val schema = {
    new StructType().add("House", DataTypes.LongType, nullable = true)
      .add("Taxes", DataTypes.LongType, nullable = true)
      .add("Bedrooms", DataTypes.LongType, nullable = true)
      .add("Baths", DataTypes.FloatType, nullable = true)
      .add("Quadrant", DataTypes.StringType, nullable = true)
      .add("NW", DataTypes.LongType, nullable = true)
      .add("Price($)", DataTypes.LongType, nullable = false)
      .add("Size(sqft)", DataTypes.LongType, nullable = false)
      .add("lot", DataTypes.LongType, nullable = true)
  }

  def main(args: Array[String]): Unit = {
    val sparkConf = new SparkConf().setMaster("local[*]").setAppName("ML Try with houses")
      .set("spark.sql.caseSensitive", "false")

    val spark = SparkSession.builder().config(sparkConf).getOrCreate()
    import spark.implicits._

    val housesDF: DataFrame = spark.read.schema(schema).json("bin/mldata/houses.json")
    val gatheredDF = housesDF.select($"Taxes", $"Bedrooms", $"Baths", col("Price($)"), $"Size(sqft)")
    val imputer = new Imputer().setInputCols(Array("Baths")).setOutputCols(Array("~Baths~"))
    val vectorAssembler = new VectorAssembler().setInputCols(Array("Taxes", "Bedrooms", "~Baths~", "Size(sqft)"))
      .setOutputCol("features")

    //Model
    val model = new LinearRegression()
    model.setMaxIter(1000)

    //set pipeline with stages
    val pipeline = new Pipeline().setStages(Array(imputer, vectorAssembler, model))

    //randomly divide dataset
    val dividedDF = gatheredDF.withColumnRenamed("Price($)", "label")
      .randomSplit(Array(.8, .2))
    val trainDF = dividedDF(0)
    val evaluateDF = dividedDF(1)

    //train model
    val pipelineModel = pipeline.fit(trainDF)

    val resultDF = pipelineModel.transform(evaluateDF)

    resultDF.show(truncate = false)
    val evaluationDf = resultDF.select($"prediction", $"label")

    val r2Eval = new RegressionEvaluator().setMetricName("r2")
    val rmseEval = new RegressionEvaluator().setMetricName("rmse")

    val r2 = r2Eval.evaluate(evaluationDf)
    val rmse = rmseEval.evaluate(evaluationDf)

    println(s"R2: $r2, RSME: $rmse")
  }
}
