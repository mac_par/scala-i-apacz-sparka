package packtpub.part5

import org.apache.spark.graphx.{Edge, Graph}
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkConf, SparkContext}

object GraphXExample {
  def main(args: Array[String]): Unit = {
    val config = new SparkConf().setMaster("local[*]").setAppName("GraphX try")

    val sc = new SparkContext(config)
    val vertices = Seq(
      1l -> "James",
      2l -> "Andy",
      3l -> "Ed",
      4l -> "Roger",
      5l -> "Tony"
    )
    val edges = Seq(
      Edge(2,1,"friend"),
      Edge(3,1,"friend"),
      Edge(3,2, "colleague"),
      Edge(3,5, "partner"),
      Edge(4,3, "Boss"),
      Edge(5,2, "Partner")
    )

    val verticesRdd = sc.parallelize(vertices)
    val edgesRdd = sc.parallelize(edges)

    val graph = Graph.apply(verticesRdd, edgesRdd,"", StorageLevel.MEMORY_ONLY, StorageLevel.MEMORY_ONLY)

    graph.vertices.saveAsTextFile("output/graph/vertices")
    graph.edges.saveAsTextFile("output/graph/edges")

    sc.stop()
  }
}
