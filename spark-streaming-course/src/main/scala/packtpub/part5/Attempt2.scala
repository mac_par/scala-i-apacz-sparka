package packtpub.part5

import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.apache.spark.streaming.dstream.{DStream, InputDStream}
import org.apache.spark.streaming.kafka010.{ConsumerStrategies, KafkaUtils, LocationStrategies}
import org.apache.spark.streaming.{Seconds, StreamingContext}

object Attempt2 {
  def kafkaParams: Map[String, Object] = {
    val map: scala.collection.mutable.Map[String, Object] = scala.collection.mutable.HashMap(
      ("bootstrap.servers", "127.0.0.1:9092")
      , ("key.deserializer", classOf[StringDeserializer])
      , ("value.deserializer", classOf[StringDeserializer])
      , ("group.id", "meetup-consumer")
      , ("auto.offset.reset", "earliest")
      , ("enable.auto.commit", false: java.lang.Boolean)
    )

    map.toMap
  }

  def main(args: Array[String]): Unit = {
    val sparkConf = new SparkConf().setAppName("Attempt 2").setMaster("local[*]")
    val sc = new StreamingContext(sparkConf, Seconds(1))

    val topics = Seq("meetupTopic")

    val kafkaStream = KafkaUtils.createDirectStream(
      sc, LocationStrategies.PreferConsistent, ConsumerStrategies.Subscribe[String, String](topics, kafkaParams))
    val kafkaValues = kafkaStream.mapPartitions{part => part.map(_.value())}

    kafkaValues.foreachRDD{(rdd, time) =>
      val spark = SparkSession.builder().config(sparkConf).getOrCreate()
      import spark.implicits._
      val jsonStream = spark.read.json(spark.createDataset(rdd))
      jsonStream.printSchema()
    }

    /*kafkaValues.foreachRDD { (rdd, time) =>
      val spark = SparkSession.builder().appName("cos").master("local[*]").getOrCreate()
      import spark.implicits._

      val ds: DataFrame = spark.read.json(spark.createDataset(rdd))
      ds.show(truncate = false)
    }*/

    sc.awaitTermination()
    sc.stop()
  }


}
