package packtpub.part4

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

object ElectricityDetailed3 {

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().master("local[*]").appName("Electricity consumption").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    import spark.implicits._

    val rawData = spark.read.format("csv")
      .option("header",value = true)
      .option("sep",";").load("bin/household_power_consumption.txt")


    val days = rawData.groupBy($"date").agg(sum($"Global_active_power").name("A"), sum($"Global_reactive_power").name("B"),
      sum($"Voltage").name("C"), sum($"Global_intensity").name("D"), sum($"Sub_metering_1").name("E"),
      sum($"Sub_metering_2").name("F"), sum($"Sub_metering_3").name("G"))
        .select($"date", round($"A",2).name("dgap"), round($"B",2).name("dgrp"),
          round($"C",2).name("dvolt"),round($"D",2).name("dgi"), round($"E",2).name("dsm1"),
          round($"F",2).name("dsm2"), round($"G",2).name("dsm3"))
        .withColumn("dow", from_unixtime(unix_timestamp($"date", "dd/MM/yyyy"),"EEEEE"))
        .withColumn("day",dayofmonth(to_date(unix_timestamp($"date","dd/MM/yyyy").cast("timestamp"))))
        .withColumn("month", month(to_date(unix_timestamp($"date","dd/MM/yyyy").cast("timestamp"))))
        .withColumn("year", year(to_date(unix_timestamp($"date","dd/MM/yyyy").cast("timestamp"))))
        .groupBy($"year", $"month").count().orderBy($"year",$"month")

    days.printSchema()
    days.show(false)
    //Date;Time;Global_active_power;Global_reactive_power;Voltage;Global_intensity;Sub_metering_1;Sub_metering_2;Sub_metering_3
    spark.close()
  }
}
