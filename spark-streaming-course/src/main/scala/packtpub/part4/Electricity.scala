package packtpub.part4

import org.apache.spark.sql.SparkSession

object Electricity {
case class HouseholdEPC(date: String, time: String, gap: Double, grp: Double, voltage: Double, gi: Double, sm_1: Double, sm_2: Double, sm_3: Double)

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().master("local[*]").appName("Electricity consumption").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    import spark.implicits._

    val rawData = spark.read.textFile("bin/household_power_consumption.txt")
    val header = rawData.first()
    println(header)
    val electricityData = rawData.filter(_ != header).filter(item => !item.contains("?")).mapPartitions{partition =>
      partition.map(_.split(";")).map{array =>
        HouseholdEPC(array(0).trim, array(1).trim, array(2).toDouble,
          array(3).toDouble, array(4).toDouble, array(5).toDouble, array(6).toDouble, array(7).toDouble, array(8).toDouble)
      }
    }

    val electricityDF = electricityData.toDF()
    electricityDF.printSchema()
    electricityData.show(truncate = false)

    spark.close()
  }
}
