package packtpub.part4

import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.SparkSession

object WordsWords {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.OFF)
    val config = new SparkConf().setMaster("local[*]").setAppName("WordsWords")
    val sc = new SparkContext(config)

    val text = sc.textFile("bin/stopwords.txt")
    val regex = "[,.:;'\"\\?\\-!\\(\\)]".r

    val spark = SparkSession.builder().getOrCreate()
    import spark.implicits._
    val textDF = text.toDF

    val processed = text.flatMap{line => line.split("[\\s]")}.map{word => regex.replaceAllIn(word.trim.toLowerCase, "")}
        .filter(!_.isEmpty).toDF.cache()
    println(s"count: ${processed.count()}")
    val cleanWords = textDF.except(processed)
    println(s"clean count: ${cleanWords.count()}")
    sc.stop()
  }
}
