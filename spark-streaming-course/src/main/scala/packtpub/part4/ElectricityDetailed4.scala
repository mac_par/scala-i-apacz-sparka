package packtpub.part4

import org.apache.spark.sql.{Dataset, SparkSession}
import org.apache.spark.sql.functions._

object ElectricityDetailed4 {

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().master("local[*]").appName("Electricity consumption").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    import spark.implicits._

    val rawData = spark.read.format("csv")
      .option("header",value = true)
      .option("sep",";").load("bin/household_power_consumption.txt")


    val days: Dataset[HouseholdEPC] = rawData.withColumnRenamed("Global_active_power","gap")
      .withColumnRenamed("Global_reactive_power", "grp")
        .withColumnRenamed("Global_intensity","gi")
        .withColumnRenamed("Sub_metering_1","sm_1")
        .withColumnRenamed("Sub_metering_2","sm_2")
        .withColumnRenamed("Sub_metering_3","sm_3").as[HouseholdEPC]


    days.printSchema()
    days.show(false)
    //date: String, time: String, gap: Double, grp: Double, voltage: Double, gi: Double, sm_1: Double, sm_2: Double, sm_3: Double
    //Date;Time;Global_active_power;Global_reactive_power;Voltage;Global_intensity;Sub_metering_1;Sub_metering_2;Sub_metering_3
    spark.close()
  }
}
