package packtpub.part4

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{sum,round,dayofmonth,unix_timestamp,to_date, month,year,from_unixtime}
object ElectricityDetailed2 {

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().master("local[*]").appName("Electricity consumption").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    import spark.implicits._

    val rawData = spark.read.format("csv")
      .option("header",value = true)
      .option("sep",";").load("bin/household_power_consumption.txt")


    val days = rawData.groupBy($"date").agg(sum($"Global_active_power").name("A"),
      sum($"Global_reactive_power").name("B"), sum($"Voltage").name("C"),
      sum($"Global_intensity").name("D"),
      sum("Sub_metering_1").name("F"),
      sum("Sub_metering_2").name("G"),
      sum("Sub_metering_3").name("H"))
      .select($"date",round($"A",2).name("dgap"),round($"B",2).name("dgrp"), round($"C",2).name("dvolatage"),
        round($"D",2).name("dgi"),round($"F",2).name("dsm_1"), round($"G",2).name("dsm_2"), round($"H",2).name("dsm_3"))
        .withColumn("dow", from_unixtime(unix_timestamp($"date", "dd/MM/yyyy"),"EEEEE"))
        .withColumn("day", dayofmonth(to_date(unix_timestamp($"date","dd/MM/yyyy").cast("timestamp"))))
        .withColumn("month", month(to_date(unix_timestamp($"date", "dd/MM/yyyy").cast("timestamp"))))
        .withColumn("year",year(to_date(unix_timestamp($"date", "dd/MM/yyyy").cast("timestamp"))))

    days.printSchema()
    days.show(false)
    //Date;Time;Global_active_power;Global_reactive_power;Voltage;Global_intensity;Sub_metering_1;Sub_metering_2;Sub_metering_3
    spark.close()
  }
}
