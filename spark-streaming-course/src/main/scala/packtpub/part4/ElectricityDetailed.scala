package packtpub.part4

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{from_unixtime, to_date, unix_timestamp,dayofmonth, month,year}


object ElectricityDetailed {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().appName("Electricity Detailed").master("local[*]").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    import spark.implicits._
    val households = spark.read.format("csv").option("header", value = true).option("sep", ";").load("bin/household_power_consumption.txt")

    val collected = households.withColumn("dow", from_unixtime(unix_timestamp($"date", "dd/MM/yyyy"), "EEEEE"))
        .withColumn("day",dayofmonth(to_date(unix_timestamp($"date", "dd/MM/yyyy").cast("timestamp"))))
        .withColumn("month", month(to_date(unix_timestamp($"date", "dd/MM/yyyy").cast("timestamp"))))
        .withColumn("year", year(to_date(unix_timestamp($"date","dd/MM/yyyy").cast("timestamp"))))

    collected.printSchema()
    collected.show(false)
    spark.close()
  }
}
