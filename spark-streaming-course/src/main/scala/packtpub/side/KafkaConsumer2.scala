package packtpub.side

import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{to_timestamp,explode,dayofmonth,unix_timestamp,to_date}
import org.apache.spark.streaming.kafka010.{ConsumerStrategies, KafkaUtils, LocationStrategies}
import org.apache.spark.streaming.{Seconds, StreamingContext}

object KafkaConsumer2 {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.OFF)
    val conf = new SparkConf().setMaster("local[*]").setAppName("cos")
    val sc = new StreamingContext(conf, Seconds(2))
    sc.sparkContext.setLogLevel("ERROR")

    val topics = Set("meetupTopic")
    val stream = KafkaUtils.createDirectStream(sc, LocationStrategies.PreferConsistent,
      ConsumerStrategies.Subscribe[String, String](topics, kafkaConfig))

    val valueStream = stream.mapPartitions { partition =>
      partition.map(_.value())
    }

    valueStream.foreachRDD(rdd => procesRdd(rdd))
    sc.start()
    sc.awaitTermination()
  }

  def procesRdd(rdd: RDD[String]): Unit = {
    val spark = SparkSession.builder().master("local[*]").appName("sessionen").getOrCreate()
    import spark.implicits._

    val dataframe = spark.read.json(spark.createDataset(rdd))
    val processed = dataframe.filter($"guests".gt(0)).filter($"group.group_country".isInCollection(Set("pl","us","gb")))
      .filter($"guests".isNotNull)
      .filter($"guests".gt(0))
        .select($"event.event_name".as("wydarzenie"), unix_timestamp($"event.time".cast("timestamp")).as("czas"),
        $"group.group_city".as("Miasto"), $"group.group_name".as("groupa_wsparcia"), explode($"group.group_topics").as("tematy"),
          $"guests".as("goscie"),
        $"member.member_name".as("gosc"), to_timestamp($"mtime").as("utworzono"))

    processed.printSchema()
    processed.show(truncate = false)
  }

  def kafkaConfig: Map[String, Object] = {
    Map("bootstrap.servers"->"127.0.0.1:9092", "key.deserializer" -> classOf[StringDeserializer],
      "value.deserializer" -> classOf[StringDeserializer], "group.id" -> "jakis_konsument",
      "auto.offset.reset" -> "earliest", "enable.auto.commit" -> (false: java.lang.Boolean))
  }
}
